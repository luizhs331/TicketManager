#ifndef TELACONFIGGERAL_H
#define TELACONFIGGERAL_H

#include <QObject>

class ConfigGeralController : public QObject {
    Q_OBJECT
    Q_PROPERTY( QStringList comboBox READ comboBoxPrintar NOTIFY comboBoxChanged )
    Q_PROPERTY( bool selectAnimationFailOrSucess READ selectAnimationFailOrSucess WRITE setSelectAnimationFailOrSucess NOTIFY selectAnimationFailOrSucessChanged )
public:
    explicit ConfigGeralController( QObject* parent = nullptr );

    void setSelectAnimationFailOrSucess( bool selectAnimationFailOrSucess );

    bool selectAnimationFailOrSucess() const;

    QStringList comboBoxPrintar() const;

signals:
    void comboBoxChanged();
    void selectAnimationFailOrSucessChanged();

public slots:

    void insertNomeTurma( const QString& nomeTurma );
    void inserEnviarDadosIntegracao( const int& valor );
    void deletarTurma( const QString& nomeTurma );
    void salvarNomeEscola ( QString nomeEscola, QString maximoCaraLinha );

    int convertOptionForIndex( QStringList options, QString optionSelected );
    int retornoMaximoCaracterLinha();

    bool selectDadosIntegracao();

    QString retornoNomeEscola();
    QString salvarDadosConfigGeral( const QString& nomeEscola, const QString& maximoCaracterLinha );

private:
    bool _selectAnimationFailOrSucess = false;
};

#endif // TELACONFIGGERAL_H
