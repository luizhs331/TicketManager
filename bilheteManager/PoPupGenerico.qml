import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.12

Popup {
    id: popup
    width: parent.width
    height: parent.height
    anchors.centerIn: parent
    modal: true
    visible: popupRectangle.visible
    focus: true
    z: 0

    property string descricaoPoPup
    property string colorTop
    property string colorTextTop: "#fff"
    property string imgLink: ""
    property bool hasImage: imgLink.trim().length > 0

    property int sizePointSize: 13
    property int heightRectangleTop: 80

    closePolicy: function () {
        return Popup.CloseOnEscape || Popup.CloseOnPressOutsideParent
    }

    background: Rectangle {

        radius: 10

        Rectangle {
            id: roundRect
            radius: 3
            z: 1
            color: colorTop
            visible: popupRectangle.visible
            width: popup.width
            height: heightRectangleTop
            Rectangle {
                id: rectangleUp
                width: parent.width
                height: roundRect.height / 1.24
                color: "transparent"

                Column {
                    height: rectangleUp.height
                    width: rectangleUp.width
                    spacing: 10

                    Label {
                        text: descricaoPoPup
                        font.family: "Helvetica"
                        font.pointSize: sizePointSize
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        color: colorTextTop
                        visible: !hasImage
                    }
                    Image {
                        id: imgV
                        opacity: 1
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        width: 40
                        height: 40
                        visible: hasImage
                        source: imgLink
                    }
                }
            }
        }

        Rectangle {
            id: squareRect
            z: 2
            width: popup.width
            visible: popupRectangle.visible
            color: "#fff"
            height: 20
            //            y:63
            //            x:0
            anchors.bottom: roundRect.bottom
        }
    }
}
