#ifndef PROCESSOCONTROLLER_H
#define PROCESSOCONTROLLER_H

#include <QObject>

class ProcessoController : public QObject
{
    Q_OBJECT
public:
    explicit ProcessoController(QObject *parent = nullptr);    
signals:
    void liberarProcesso();
public slots:
    virtual bool validarSenhaProcesso( const QString& senha );

};

#endif // PROCESSOCONTROLLER_H
