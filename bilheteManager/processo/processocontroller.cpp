#include "processocontroller.h"

#include <QEventLoop>
#include <QDebug>

#include "processo/processoservice.h"
#include "bilheteentradaousaida/bilheteentradaousaidacontroller.h"

ProcessoController::ProcessoController( QObject* parent ) :
    QObject( parent ) {}

bool ProcessoController::validarSenhaProcesso( const QString& senha ) {
    bool success;
    qInfo() << "ProcessoController::validarSenhaProcesso";
    success = ProcessoService().validarSenhaProcesso( senha );
    qInfo() << "ProcessoController::validarSenhaProcesso [RETURN]" << success;
    return success;
}
