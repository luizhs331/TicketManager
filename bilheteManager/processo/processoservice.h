#ifndef PROCESSOSERVICE_H
#define PROCESSOSERVICE_H

#include <QObject>

class ProcessoService : public QObject
{
    Q_OBJECT
public:
    explicit ProcessoService(QObject *parent = nullptr);

    bool validarSenhaProcesso( const QString& senha );

signals:

};

#endif // PROCESSOSERVICE_H
