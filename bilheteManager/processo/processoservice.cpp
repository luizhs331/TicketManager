#include "processoservice.h"

#include <login/loginservice.h>

#include <usuario/usuarioservice.h>

ProcessoService::ProcessoService( QObject* parent ) :
    QObject( parent ) {}

bool ProcessoService::validarSenhaProcesso( const QString& senha ) {
    return LoginService().recuperarUsuarioLogado()->senhaUsuario() == UsuarioService().criptografiaSenha( senha );
}
