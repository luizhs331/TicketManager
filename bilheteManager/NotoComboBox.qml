import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Private 1.0

Rectangle {
    id: mainRetangulo
    visible: true

    property var modelTxt
    property var textEditComboBox
    property var tamanhoQuadradoDropBox: 0
    property var textError: "*"

    property bool isTextConcat: false
    property bool isRequiredfield: false

    property int fontSizeEdit: 15
    property int currentIndexBox: currentIndex

    signal emitirText(var text)

    function textConcat() {

        if (isTextConcat) {
            return textEditComboBox + box.currentText
        } else {
            if (box.currentText == '')
                return textEditComboBox
        }
        return box.currentText
    }

    Column {
        width: mainRetangulo.width
        height: mainRetangulo.width

        spacing: 10

        Label {
            id: mandatoryFild
            text: textError
            font.family: "Helvetica"
            font.pointSize: 10
            color: "#ff0000"
            visible: isRequiredfield
            opacity: 1
        }

        ComboBox {
            id: box
            currentIndex: currentIndexBox
            //        displayText: currentIndex === -1 ? "Please Choose..." : currentText
            activeFocusOnPress: true
            style: ComboBoxStyle {
                id: comboBox
                background: Rectangle {
                    id: rectCategory
                    color: enabled ? "#fff" : "#000"
                    border.width: box.focus ? 2 : 0.5
                    border.color: box.focus ? "#11cdef" : "#d3d3d3"
                    opacity: enabled ? 1 : 0.2
                    radius: 3
                }
                label: Text {
                    id: textComboBox
                    verticalAlignment: Text.AlignVCenter
                    font.pointSize: fontSizeEdit
                    font.family: "Helvetica"
                    //            font.capitalization: Font.SmallCaps
                    color: "#000"
                    opacity: 0.6
                    text: textConcat()
                }

                property Component __dropDownStyle: MenuStyle {
                    __maxPopupHeight: 600
                    __menuItemType: "comboboxitem"

                    frame: Rectangle {
                        color: "#fff"
                        border.width: 1
                        border.color: "gray"
                        opacity: 0.5
                    }

                    itemDelegate.label: // an item text
                                        Text {
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 15
                        font.family: "Courier"
                        font.capitalization: Font.SmallCaps
                        color: styleData.selected ? "#000" : "#d3d3d3"
                        text: styleData.text
                    }

                    itemDelegate.background: Rectangle {
                        color: styleData.selected ? "#addff8" : "transparent"
                        opacity: 1.5
                    }

                    __scrollerStyle: ScrollViewStyle {}
                }

                property Component __popupStyle: Style {
                    property int __maxPopupHeight: 400
                    property int submenuOverlap: 100

                    property Component menuItemPanel: Text {
                        text: "NOT IMPLEMENTED"
                        color: "red"
                        font {
                            pixelSize: 14
                            bold: true
                        }
                    }

                    property Component __scrollerStyle: null
                }
            }

            model: modelTxt
            width: mainRetangulo.width
            height: mainRetangulo.height
            onCurrentIndexChanged: mainRetangulo.emitirText(box.currentText)

            //        Rectangle{
            //            id:retanguloRetangulo
            //            height: parent.height - 2
            //            width: tamanhoQuadradoDropBox == 0 ? 50 : tamanhoQuadradoDropBox
            //            anchors.right: parent.right
            //            anchors.verticalCenter: parent.verticalCenter
            //            anchors.margins: 1
            //            opacity: 0.5

            //            Rectangle{
            //            width: 2
            //            height: parent.height
            //            color: box.focus ? "#11cdef" : "#d3d3d3"

            //            }

            //        Image {
            //            id:img
            //            width: 25
            //            height: 20
            //            opacity: box.focus ? 1 : 0.7
            //            source: "img/img15.svg"
            //            anchors.centerIn: parent
            //            anchors.leftMargin: 5
            //        }
            //       }
        }
    }
}
