#include "escolamodel.h"

EscolaModel::EscolaModel( QObject* parent ) :
    QObject( parent ) {}

int EscolaModel::idEscola() const {
    return _idEscola;
}

void EscolaModel::setIdEscola( int idEscola ) {
    _idEscola = idEscola;
}

QString EscolaModel::nomeEscola() const {
    return _nomeEscola;
}

void EscolaModel::setNomeEscola( const QString& nomeEscola ) {
    _nomeEscola = nomeEscola;
}

QString EscolaModel::usuario() const {
    return _usuario;
}

void EscolaModel::setUsuario( const QString& usuario ) {
    _usuario = usuario;
}

QString EscolaModel::telefone() const {
    return _telefone;
}

void EscolaModel::setTelefone( const QString& telefone ) {
    _telefone = telefone;
}

QString EscolaModel::estadoUf() const {
    return _estadoUf;
}

void EscolaModel::setEstadoUf( const QString& estadoUf ) {
    _estadoUf = estadoUf;
}

QString EscolaModel::cidade() const {
    return _cidade;
}

void EscolaModel::setCidade( const QString& cidade ) {
    _cidade = cidade;
}

QString EscolaModel::rua() const {
    return _rua;
}

void EscolaModel::setRua( const QString& rua ) {
    _rua = rua;
}

QString EscolaModel::bairro() const {
    return _bairro;
}

void EscolaModel::setBairro( const QString& bairro ) {
    _bairro = bairro;
}

QString EscolaModel::email() const {
    return _email;
}

void EscolaModel::setEmail( const QString& email ) {
    _email = email;
}

QString EscolaModel::data() const {
    return _data;
}

void EscolaModel::setData( const QString& data ) {
    _data = data;
}

QString EscolaModel::senha() const {
    return _senha;
}

void EscolaModel::setSenha( const QString& senha ) {
    _senha = senha;
}
