#ifndef CADASTROESCOLAENDPOINT_H
#define CADASTROESCOLAENDPOINT_H

#include <QObject>

class EscolaEndpoint {
public:
    EscolaEndpoint();

    QStringList getUfs() const;
    QStringList getCity( const QString& uf ) const;
};

#endif // CADASTROESCOLAENDPOINT_H
