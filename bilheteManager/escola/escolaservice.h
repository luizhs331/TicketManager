#ifndef CADASTROESCOLASERVICE_H
#define CADASTROESCOLASERVICE_H

#include "escolamodel.h"

class EscolaService {
public:
    EscolaService();

    EscolaModel* findSchool();

    void changeNameSchool ( const QString& nameSchool );

    bool registerSchool( QObject* model );
    int recoverLastIdSchool();
    int idCurrentSchool();

};

#endif // CADASTROESCOLASERVICE_H
