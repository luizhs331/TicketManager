#include "escolarepository.h"

#include <QtSql>
#include <QDebug>
#include <QDateTime>

#define DIRECTORY "selectsqls/escola/"

void EscolaRepository::changeNameSchool( const QString& nameSchool, int idEscola ) {

    QSqlQuery query;
    query.prepare( "update escola set nomeescola = :NOME_ESCOLA where idescola = :ID_ESCOLA" );

    query.bindValue( ":NOME_ESCOLA", nameSchool );
    query.bindValue( ":ID_ESCOLA", idEscola );

    query.exec();

}

int EscolaRepository::recoverLastIdSchool() {

    QSqlQuery query;
    query.prepare( "select idescola AS ID_ESCOLA from escola where idescola = (SELECT MAX(idescola) FROM escola)" );
    query.exec();

    query.next();

    return query.value( "ID_ESCOLA" ).toInt();

}

bool EscolaRepository::registerSchool( const EscolaModel* model ) {

    QSqlQuery query;

    query.prepare( Sqls::createQuery( INSERT_INTO_ESCOLA, "selectsqls/escola/" ) );

    query.bindValue( NOME_ESCOLA, model->nomeEscola() );
    query.bindValue( USUARIO, model->usuario() );
    query.bindValue( TELEFONE, model->telefone() );
    query.bindValue( ESTADOUF, model->estadoUf() );
    query.bindValue( CIDADE, model->cidade() );
    query.bindValue( EMAIL, model->email() );
    query.bindValue( BAIRRO, model->bairro() );
    query.bindValue( RUA, model->rua() );
    query.bindValue( DATA, QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) );
    query.bindValue( SENHA, model->senha() );

    return query.exec();

}

EscolaModel* EscolaRepository::findSchool( int idEscola ) {

    QSqlQuery query;
    EscolaModel* escolaModel = new EscolaModel();

    query.prepare( "select nomeescola AS NOME_ESCOLA from escola where idescola = :ID_ESCOLA" );
    query.bindValue( ":ID_ESCOLA", idEscola );
    query.exec();

    query.next();

    escolaModel->setNomeEscola( query.value( "NOME_ESCOLA" ).toString() );

    return escolaModel;
}

void EscolaRepository::updateSchool() {};

