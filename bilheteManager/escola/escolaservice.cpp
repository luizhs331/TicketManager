#include "escolaservice.h"

#include "escolarepository.h"
#include "escolamodel.h"

#include <usuario/usuariomodel.h>
#include <usuario/usuarioservice.h>

EscolaService::EscolaService() {}

EscolaModel* EscolaService::findSchool() {
    return EscolaRepository().findSchool( idCurrentSchool() );
}

void EscolaService::changeNameSchool( const QString& nameSchool ) {
    EscolaRepository().changeNameSchool( nameSchool, idCurrentSchool() );
}

bool EscolaService::registerSchool( QObject* model ) {

    bool success = true;

    try {
        EscolaModel* escola = static_cast<EscolaModel*>( model );

        UsuarioModel* usuario = new UsuarioModel();
        usuario->setNomeUsuario( escola->usuario() );
        usuario->setSenhaUsuario( UsuarioService().criptografiaSenha( escola->senha() ) );

        success = EscolaRepository().registerSchool( escola );
        usuario->setIdEscolaUsuario( EscolaRepository().recoverLastIdSchool() );

        UsuarioService().insertUserWithIdOrNot( usuario );

    } catch ( ... ) {
        success = false;
    }
    return success;
}

int EscolaService::recoverLastIdSchool() {
    return EscolaRepository().recoverLastIdSchool();
}

int EscolaService::idCurrentSchool() {
    UsuarioModel* usuario = UsuarioService().recuperarUsuarioLogado();
    int idEscola = usuario->idEscolaUsuario();
    delete usuario;

    return idEscola;
}
