#include "escolaendpoint.h"
#include <worker/worker.h>

#include <QJsonDocument>
#include <QtConcurrent>
#include <QJsonObject>
#include <QJsonArray>

#define URL_ESTADOS "https://servicodados.ibge.gov.br/api/v1/localidades/estados"
#define URL_CIDADES "https://servicodados.ibge.gov.br/api/v1/localidades/estados/%0/municipios"

EscolaEndpoint::EscolaEndpoint() {}

QStringList EscolaEndpoint::getUfs() const {

    QStringList ufs = {};

    QByteArray response = QtConcurrent::run( []() {
        return Worker().get( URL_ESTADOS );
    } );

    QVariantList listJson = QJsonDocument::fromJson( response ).array().toVariantList();

    for( QVariant json : listJson ) {
        ufs.append( json.toJsonObject()["sigla"].toString() );
    }

    return ufs;

}

QStringList EscolaEndpoint::getCity( const QString& uf ) const {

    QStringList citys = {};

    QByteArray response =  QtConcurrent::run( [&]() {
        return Worker().get( QString( URL_CIDADES ).arg( uf )  );
    } );

    QVariantList listJson = QJsonDocument::fromJson( response ).array().toVariantList();

    for( QVariant json : listJson ) {
        citys.append( json.toJsonObject()["nome"].toString() );
    }

    return citys;

}
