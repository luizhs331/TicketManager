#include "escolacontroller.h"

#include "escolaendpoint.h"
#include "escolaservice.h"

#include <QVariant>

EscolaController::EscolaController( QObject* parent ) :
    QObject( parent ) {}

EscolaModel* EscolaController::findSchool() {
    return EscolaService().findSchool();
}

void EscolaController::changeNameSchool( const QString& nameSchool ) {
    EscolaService().changeNameSchool( nameSchool );
}

bool EscolaController::registerSchool( QObject* model ) {
    return EscolaService().registerSchool( model );
}

QStringList EscolaController::selectUfs() const {
    return EscolaEndpoint().getUfs();
}

QStringList EscolaController::selectCity( const QString& uf ) const {
    return EscolaEndpoint().getCity( uf );
}
