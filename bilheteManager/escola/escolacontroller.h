#ifndef CADASTROESCOLACONTROLLER_H
#define CADASTROESCOLACONTROLLER_H

#include <QObject>

#include "escolamodel.h"

class EscolaController : public QObject {
    Q_OBJECT
public:
    explicit EscolaController( QObject* parent = nullptr );

public slots:

    EscolaModel* findSchool();

    void changeNameSchool ( const QString& nameSchool );
    bool registerSchool( QObject* model );

    QStringList selectUfs() const;
    QStringList selectCity( const QString& uf ) const;

};

#endif // CADASTROESCOLACONTROLLER_H
