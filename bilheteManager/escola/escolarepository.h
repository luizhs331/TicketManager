#ifndef CADASTROESCOLAREPOSITORY_H
#define CADASTROESCOLAREPOSITORY_H

#include "sqls.h"

#include "escolamodel.h"

class EscolaRepository /*: public Sqls*/ {
public:

    void changeNameSchool ( const QString& nameSchool, int idEscola );
    int recoverLastIdSchool();

    bool registerSchool( const EscolaModel* model );
    void updateSchool();
    EscolaModel* findSchool( int idEscola );

    static constexpr const char* ID_ESCOLA = ":IDESCOLA";
    static constexpr const char* NOME_ESCOLA = ":NOMEESCOLA";
    static constexpr const char* USUARIO = ":USUARIO";
    static constexpr const char* TELEFONE = ":TELEFONE";
    static constexpr const char* ESTADOUF = ":ESTADOUF";
    static constexpr const char* CIDADE = ":CIDADE";
    static constexpr const char* EMAIL = ":EMAIL";
    static constexpr const char* BAIRRO = ":BAIRRO";
    static constexpr const char* RUA = ":RUA";
    static constexpr const char* DATA = ":DATA";
    static constexpr const char* SENHA = ":SENHA";

    // SQLS
    static constexpr const char* INSERT_INTO_ESCOLA = "INSERT_INTO_ESCOLA.txt";

};

#endif // CADASTROESCOLAREPOSITORY_H
