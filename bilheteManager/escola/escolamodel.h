#ifndef ESCOLAMODEL_H
#define ESCOLAMODEL_H

#include <QObject>

class EscolaModel : public QObject {

    Q_OBJECT
    Q_PROPERTY( int idEscola READ idEscola WRITE setIdEscola )
    Q_PROPERTY( QString nomeEscola READ nomeEscola WRITE setNomeEscola NOTIFY nomeEscolaChanged )
    Q_PROPERTY( QString usuario READ usuario WRITE setUsuario )
    Q_PROPERTY( QString telefone READ telefone WRITE setTelefone )
    Q_PROPERTY( QString estadoUf READ estadoUf WRITE setEstadoUf )
    Q_PROPERTY( QString cidade READ cidade WRITE setCidade )
    Q_PROPERTY( QString rua READ rua WRITE setRua )
    Q_PROPERTY( QString bairro READ bairro WRITE setBairro )
    Q_PROPERTY( QString email READ email WRITE setEmail )
    Q_PROPERTY( QString data READ data WRITE setData )
    Q_PROPERTY( QString senha READ senha WRITE setSenha );

public:
    explicit EscolaModel( QObject* parent = nullptr );

    int idEscola() const;
    void setIdEscola( int idEscola );

    QString nomeEscola() const;
    void setNomeEscola( const QString& nomeEscola );

    QString usuario() const;
    void setUsuario( const QString& usuario );

    QString telefone() const;
    void setTelefone( const QString& telefone );

    QString estadoUf() const;
    void setEstadoUf( const QString& estadoUf );

    QString cidade() const;
    void setCidade( const QString& cidade );

    QString rua() const;
    void setRua( const QString& rua );

    QString bairro() const;
    void setBairro( const QString& bairro );

    QString email() const;
    void setEmail( const QString& email );

    QString data() const;
    void setData( const QString& data );

    QString senha() const;
    void setSenha( const QString& senha );

signals:
    void nomeEscolaChanged();

private:

    int _idEscola;
    QString _nomeEscola;
    QString _usuario;
    QString _telefone;
    QString _estadoUf;
    QString _cidade;
    QString _rua;
    QString _bairro;
    QString _email;
    QString _data;
    QString _senha;

};

#endif // ESCOLAMODEL_H
