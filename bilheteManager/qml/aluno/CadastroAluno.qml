import QtQuick 2.6
import QtQuick.Controls 2.5

Popup {
    id: popup
    focus: true
    modal: true
    anchors.centerIn: parent

    closePolicy: function () {
        return Popup.CloseOnEscape || Popup.CloseOnPressOutsideParent
    }

    background: Rectangle {
        border.color: "#fff"
        radius: 5
    }
}
