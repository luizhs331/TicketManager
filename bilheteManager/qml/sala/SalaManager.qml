import QtQuick 2.6
import QtQuick.Controls 2.5

Rectangle {
    id: rectangleMain
    anchors.centerIn: parent

    signal openScreen(var typeScreen)

    onOpenScreen: function (typeScrenn) {

        switch (typeScrenn) {
        case 'CADASTRAR_SALA':
            cadastroSala.open()
            break
        case 'UPDATE_SALA':
            break
        case 'DELETAR_SALA':
            break
        }
    }

    CadastroSala {
        id: cadastroSala
        width: rectangleMain.width / 1.8
        height: rectangleMain.height / 1.5
    }
}
