import QtQuick 2.6
import QtQuick.Controls 2.5

import br.com.saladto 1.0
import br.com.salacontroller 1.0

import "../components"

Popup {
    id: popup
    focus: true
    modal: true
    anchors.centerIn: parent

    closePolicy: function () {
        return Popup.CloseOnEscape || Popup.CloseOnPressOutsideParent
    }

    background: Rectangle {
        border.color: "#fff"
        radius: 5
    }

    SalaController {
        id: salaController
    }

    SalaDto {
        id: salaDto
        nomeSala: txtNomeTurma.currentText
        nrSala: txtNumeroSala.currentText
        nrAndar: txtNumeroAndar.currentText
        nrCapacidade: txtCapacidade.currentText
        complemento: txtComplemento.currentText
    }

    Rectangle {
        id: recSecundary
        width: popup.width / 1.1
        height: popup.height / 1.1
        anchors.centerIn: parent

        Column {
            width: recSecundary.width
            height: recSecundary.height
            anchors.centerIn: popup
            spacing: 40

            Column {
                spacing: 30

                Label {
                    text: "Cadastrar sala"
                    font.family: "Helvetica"
                    font.pointSize: 30
                    color: "#474a51"
                    opacity: 0.8
                }

                Row {
                    id: rowInformationsPrimary
                    spacing: 20
                    z: 1

                    TextFieldWithText {
                        id: txtNomeTurma
                        width: (recSecundary.width / 2) - (rowInformationsPrimary.spacing / 2)
                        height: recSecundary.height / 7
                        titleField: "Nome sala"
                        borderColorValidate: true
                        isMandatoryField: true
                    }

                    TextFieldWithText {
                        id: txtCapacidade
                        width: (recSecundary.width / 2) - (rowInformationsPrimary.spacing / 2)
                        height: recSecundary.height / 7
                        titleField: "Capacidade"
                        borderColorValidate: true
                        isMandatoryField: true
                    }
                }

                Row {
                    id: rowInformationSecundary
                    spacing: 20
                    z: 0

                    TextFieldWithText {
                        id: txtNumeroAndar
                        width: (recSecundary.width / 2) - (rowInformationSecundary.spacing / 2)
                        height: recSecundary.height / 7
                        titleField: "Nº Andar"
                        borderColorValidate: true
                    }

                    TextFieldWithText {
                        id: txtNumeroSala
                        width: (recSecundary.width / 2) - (rowInformationSecundary.spacing / 2)
                        height: recSecundary.height / 7
                        titleField: "Nº Sala"
                        borderColorValidate: true
                    }
                }
                TextFieldWithText {
                    id: txtComplemento
                    width: (recSecundary.width)
                    height: recSecundary.height / 5
                    titleField: "Complemento"
                    borderColorValidate: true
                    isMandatoryField: true
                }
            }

            ButtonCriado {
                id: btnConfirmarProcesso
                width: recSecundary.width / 4
                height: recSecundary.height / 7.5
                buttonName: "Confirmar"
                onClicked: {
                    salaController.insertSala(salaDto)
                }
            }
        }
    }
}
