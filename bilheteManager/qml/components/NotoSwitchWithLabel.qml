import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Item {

    Button {
        id: button
        width: 250
        height: 50
        background: Rectangle {
            color: "transparent"
            border.color: button.focus || control.focus ? "red" : "blue"
            border.width: 2
            radius: 5
        }

        onClicked: {
            row.checked = !row.checked
            button.focus = true
        }

        Row {
            id: row
            width: parent.width
            anchors.verticalCenter: parent.verticalCenter

            property alias checked: control.checked

            Switch {
                id: control
                checked: true

                indicator: Rectangle {
                    implicitWidth: 48
                    implicitHeight: 26
                    x: control.width - width - control.rightPadding
                    y: parent.height / 2 - height / 2
                    radius: 13
                    color: control.checked ? "green" : "red"

                    Rectangle {
                        x: {
                            var spacing = width * 1.1
                            return control.checked ? (parent.width - spacing) : spacing - width
                        }
                        width: 22
                        height: 22
                        radius: 13
                        anchors.verticalCenter: parent.verticalCenter

                        Behavior on x {
                            NumberAnimation {
                                duration: 200
                            }
                        }
                    }
                }
            }

            Label {
                text: "Endereco Email"
                font.pointSize: 12
                anchors.verticalCenter: parent.verticalCenter
            }
        }
    }
}
