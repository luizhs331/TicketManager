import QtQuick 2.6
import QtQuick.Controls 2.5

Rectangle {
    id: idTextField
    color: "transparent"

    property RegExpValidator customRegex
    property alias titleField: lblTitle.text
    property alias currentText: textField.text
    property alias placeholderText: textField.placeholderText
    property alias enabled: textField.enabled
    property alias hasFocusEnable: textField.focus

    property int textSize: 10
    property int align: TextInput.Right

    property bool typeIsPassword: false
    property bool isMandatoryField: false
    property bool showTextErro: false

    property string borderColor: ""
    property string textErro: "( Campo obrigatório )"

    property var borderColorValidate: undefined

    signal validaCorBorda
    signal fieldFocusChanged
    signal fieldTextChange

    onValidaCorBorda: {
        validarCorBordaTextFild()
    }

    function validIfContains(caracter) {
        showTextErro = !currentText.includes(caracter)
        return showTextErro
    }

    function notIsEmpty() {
        showTextErro = currentText === ''
        return !showTextErro
    }

    function validarCorBordaTextFild() {
        if (borderColorValidate === false && hasFocusEnable) {
            borderColorValidate = undefined
            return "#d41b1b"
        } else if (hasFocusEnable) {
            return "#0fdbff"
        }
        return "#dfe0e1"
    }

    Column {
        id: column
        width: idTextField.width
        height: idTextField.height
        spacing: 5
        Row {
            width: idTextField.width
            spacing: 10

            Label {
                id: mandatoryFild
                text: '*'
                font.family: "Helvetica"
                font.pointSize: textSize
                color: "#ff0000"
                opacity: 1
                visible: isMandatoryField
            }

            Label {
                id: lblTitle
                text: titleField
                font.family: "Helvetica"
                font.pointSize: textSize
                color: "#6c6c80"
                opacity: 1
            }

            Label {
                id: lblError
                font.family: "Helvetica"
                text: textErro
                font.pointSize: textSize
                color: "#ff0000"
                visible: showTextErro
                opacity: 1
            }
        }

        TextField {
            id: textField
            width: column.width
            height: column.height - (column.spacing * 2) - lblTitle.height
            validator: customRegex
            horizontalAlignment: align
            echoMode: typeIsPassword ? TextInput.Password : TextInput.Normal
            background: Rectangle {
                radius: 5
                color: enabled ? "#fff" : "#dfe0e1"
                border.color: validarCorBordaTextFild()
            }

            onFocusChanged: fieldFocusChanged()
            onActiveFocusChanged: fieldFocusChanged()
            onTextChanged: fieldTextChange()
        }
    }
}
