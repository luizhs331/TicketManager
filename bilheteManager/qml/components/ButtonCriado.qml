import QtQuick 2.6
import QtQuick.Controls 2.5

Button {
    id: genericButton
    width: genericButtonMouseArea.pressed ? widthButton * aumentarTamanhoWidth : widthButton
    height: genericButtonMouseArea.pressed ? heightButton * aumentarTamanhoHeight : heightButton

    property int widthButton
    property int heightButton
    property double aumentarTamanhoWidth: 1.05
    property double aumentarTamanhoHeight: 1.05
    property string buttonName: ""
    property string colorButtonPressed: "#11cdef"
    property string colorBorderPressed: "#11cdef"
    property string colorBorder: "#dfe0e1"
    property var tpButtonTransparent: 'C'

    function colorButton() {
        if (tpButtonTransparent == 'N') {
            return genericButtonMouseArea.containsMouse ? colorButtonPressed : "transparent"
        }
        return genericButtonMouseArea.containsMouse ? colorButtonPressed : "#dfe0e1"
    }

    Label {
        text: buttonName
        font.family: "Helvetica"
        font.pointSize: genericButtonMouseArea.pressed ? 17 : 15
        color: genericButtonMouseArea.containsMouse ? "#fff" : "black"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        opacity: genericButtonMouseArea.containsMouse ? 1 : 0.6
    }
    spacing: 10
    anchors.topMargin: 2
    background: Rectangle {
        radius: 5
        border.color: genericButtonMouseArea.containsMouse ? colorBorderPressed : colorBorder
        border.width: 2
        color: colorButton()
    }
    MouseArea {
        id: genericButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
            genericButton.clicked()
        }
    }
}
