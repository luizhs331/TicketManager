import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.1
import br.com.configgeral 1.0

Rectangle {
    id: idAnimation
    width: 330
    height: 150
    color: "transparent"
    anchors.right: parent.right
    visible: false

    property var fromYinit
    property var fromYdone
    property bool validaStatusAnimation
    property string mensage

    property var mensagemErro: ""
    property var mensagemSuccess: ""

    signal validarAnimacao

    onValidarAnimacao: {
        if (validaStatusAnimation === true) {
            idAnimation.visible = true
            idAnimation.validaStatusAnimation = true
            idAnimation.mensage = mensagemSuccess
            idAnimation.runAnimation()
        }

        if (validaStatusAnimation === false) {
            idAnimation.visible = true
            idAnimation.validaStatusAnimation = false
            idAnimation.mensage = mensagemErro
            idAnimation.runAnimation()
        }
    }

    function runAnimation() {
        animationLoading.start()
        animation.running = true
    }

    SequentialAnimation on y {
        id: animation

        PropertyAnimation {
            from: (retaguloAnimacao.y - fromYinit)
            to: (retaguloAnimacao.y - fromYdone)
            duration: 150
        }
        PropertyAnimation {
            id: animationDuration
            from: (retaguloAnimacao.y - fromYdone)
            to: (retaguloAnimacao.y - fromYdone)
            duration: 2500
        }
        //  PropertyAnimation { from:( retaguloAnimacao.y - 150 ); to:( retaguloAnimacao.y - 150 ); duration: 1350;}
        //  PropertyAnimation { from:( retaguloAnimacao.y - 150 ); to:( retaguloAnimacao.y - 150 ); duration: 1350;}
        PropertyAnimation {
            to: -400
            duration: 600
            running: function () {
                while (running) {
                    idAnimation.visible = true
                }
                idAnimation.visible = false
            }
        }
    }

    Column {
        width: idAnimation.width
        height: idAnimation.height
        spacing: 20

        RectangularGlow {
            id: effect1
            anchors.fill: retaguloAnimacao
            glowRadius: 2
            cornerRadius: 2
            spread: 0.1
            color: "#d3d3d3"
        }

        Rectangle {
            id: retaguloAnimacao
            width: 280
            height: 75
            anchors.centerIn: parent
            color: "#fff"
            radius: 2
            border.width: 0.0

            Row {
                height: retaguloAnimacao.height
                width: retaguloAnimacao.width / 1.2
                anchors.leftMargin: 40
                anchors.horizontalCenter: parent.horizontalCenter

                spacing: 10

                Image {
                    id: imgV
                    opacity: validaStatusAnimation ? 0.6 : 1
                    anchors.verticalCenter: parent.verticalCenter
                    width: validaStatusAnimation ? 30 : 40
                    height: validaStatusAnimation ? 30 : 40
                    source: validaStatusAnimation ? "img/img10.png" : "img/fail.png"
                }

                Label {
                    id: textId
                    anchors.verticalCenter: parent.verticalCenter
                    text: mensage
                    wrapMode: "WordWrap"
                    font.pointSize: 11
                    opacity: 1
                }
            }

            Rectangle {
                id: reactableAnimationLoading
                width: 0
                height: 8
                color: validaStatusAnimation ? "#b7d8b7" : "#ff6961"
                opacity: 8.0
                anchors.bottom: retaguloAnimacao.bottom

                PropertyAnimation {
                    id: animationLoading
                    target: reactableAnimationLoading
                    properties: "width"
                    from: 0
                    to: 280
                    duration: 1800
                }
            }
        }
    }
}
