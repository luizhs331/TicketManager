#ifndef USUARIOSERVICE_H
#define USUARIOSERVICE_H

#include <QObject>

#include <usuario/usuariomodel.h>

class UsuarioService {

public:
    void sincronizarUsuarios();
    void updateUsuarioLogado( UsuarioModel* usuario );
    void insertUserWithIdOrNot( const UsuarioModel* usuarioModel ) const;

    bool loginApproved( const QString& nomeUsuario, const QString& senhaUsuario );

    UsuarioModel* recuperarUsuarioLogado();
    QString criptografiaSenha( const QString& senha );

};

#endif // USUARIOSERVICE_H
