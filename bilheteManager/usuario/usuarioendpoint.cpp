#include "usuarioendpoint.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QCoreApplication>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonObject>
#include <QJsonArray>

#define ENDPOINT_POST "http://localhost:3333/usuarios"

#define ENDPOINT_GET "http://localhost:3333/usuarios/get"

#include "worker/worker.h"

UsuarioEndPoint::UsuarioEndPoint( QObject* parent ) :
    QObject( parent ) {
    _nerworkAcessManager = new QNetworkAccessManager();
}

void UsuarioEndPoint::post( UsuarioModel* usuario ) {
    QNetworkRequest request = QNetworkRequest( QUrl( ENDPOINT_POST ) );
    request.setHeader( QNetworkRequest::ContentTypeHeader, "application/json" );
    _nerworkAcessManager->post( request, toJson( usuario ) );
}

QList<UsuarioModel*> UsuarioEndPoint::getUsuarios() {

    QJsonObject objeto = toJson( Worker().get( ENDPOINT_GET ) ).object();

    QJsonArray objetoJson = objeto["data"].toArray();
    QJsonObject response = objeto["response"].toObject();

    QList<UsuarioModel*> usuarios = toUsuarioModel( objetoJson );

    return usuarios;
}

void UsuarioEndPoint::readyRead() {
    qInfo() << "readyRead";
    QNetworkReply* reply  = qobject_cast<QNetworkReply*>( sender() );
    QByteArray array = reply->readAll();
    if( reply ) qInfo() << reply->readAll();
}

QList<UsuarioModel*> UsuarioEndPoint::toUsuarioModel( QJsonArray json ) {

    QList<UsuarioModel*> usuarios = {};

    for( QJsonValue value : json ) {

        UsuarioModel* usuario = new UsuarioModel();

        usuario->setIdUsuario( value["idUsuario"].toString().toInt() );
        usuario->setNomeUsuario( value["nomeUsuario"].toString() );
        usuario->setSenhaUsuario( value["senha"].toString() );

        usuarios.append( usuario );
    }

    return usuarios;
}

QByteArray UsuarioEndPoint::toJson( UsuarioModel* usuario ) {
    QJsonObject response;

    response[ "idUsuario" ] = usuario->idUsuario();
    response[ "nomeUsuario" ] = usuario->nomeUsuario();
    response[ UsuarioModel::SENHA_USUARIO ] = usuario->senhaUsuario();

    QJsonDocument doc( response );

    return doc.toJson();
}

QJsonDocument UsuarioEndPoint::toJson( QByteArray json ) {
    return QJsonDocument::fromJson( json );
}

