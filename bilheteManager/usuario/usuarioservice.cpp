#include "usuarioservice.h"

#include <QDebug>
#include <QCryptographicHash>

#include "usuario/usuariorepository.h"
#include "usuario/usuarioendpoint.h"

#include <configbancogeral.h>

bool UsuarioService::loginApproved( const QString& nomeUsuario, const QString& senhaUsuario ) {

    bool success = false;

    QString senhaUsuarioEncriptada = criptografiaSenha( senhaUsuario );
    UsuarioModel* usuarioModel = UsuarioRepository().validarUsuarioExistente( nomeUsuario, senhaUsuarioEncriptada );

    if( usuarioModel && !nomeUsuario.trimmed().isEmpty() && !senhaUsuario.trimmed().isEmpty() ) {
        if( usuarioModel->nomeUsuario().toUpper() == nomeUsuario && usuarioModel->senhaUsuario() == senhaUsuarioEncriptada ) {
            updateUsuarioLogado( usuarioModel );
            success = true;
        }
    }
    delete( usuarioModel );
    return success;
}

void UsuarioService::sincronizarUsuarios() {
    qInfo() << "UsuarioService::sincronizarUsuarios()";

    QList<UsuarioModel*> usuarios = UsuarioEndPoint().getUsuarios();

    if( usuarios.count() > 0 ) {

        for( UsuarioModel*& usuario : usuarios ) {
            usuario->setSenhaUsuario( UsuarioService::criptografiaSenha( usuario->senhaUsuario() ) );
            insertUserWithIdOrNot( usuario );
        }

        // Todo implementar update or insert , fazer primeiro o update e validar se alguma linha foi afetada , senao foi realizar o insert
    }
    qInfo() << "UsuarioService::sincronizarUsuarios()";
}

UsuarioModel* UsuarioService::recuperarUsuarioLogado() {
    qInfo() << "UsuarioService::recuperarUsuarioLogado";
    return UsuarioRepository().recuperarUsuarioLogado();
    qInfo() << "UsuarioService::recuperarUsuarioLogado";
}

void UsuarioService::updateUsuarioLogado( UsuarioModel* usuario ) {
    qInfo() << "UsuarioService::salvarUsuarioLogado";
    UsuarioRepository().updateUsuarioLogado( usuario );
    qInfo() << "UsuarioService::salvarUsuarioLogado";
}

QString UsuarioService::criptografiaSenha( const QString& senha ) {
    qInfo() << "UsuarioService::criptografiaSenha";
    QCryptographicHash criptografia( QCryptographicHash::Md5 );
    criptografia.addData( senha.toLatin1() );
    qInfo() << "UsuarioService::criptografiaSenha";
    return criptografia.result().toHex().data();

}

void UsuarioService::insertUserWithIdOrNot( const UsuarioModel* usuarioModel ) const {

    qInfo() << "UsuarioService::insertUserWithIdOrNot";

    QString idUsuario = QString::number( usuarioModel->idUsuario() );

    if( idUsuario.trimmed().isEmpty() || idUsuario.toInt() == 0 ) {
        UsuarioRepository().insertUserWithAutoIncrement( usuarioModel );
    }else {
        UsuarioRepository().insertUserWithId( usuarioModel );
    }

    qInfo() << "UsuarioService::insertUserWithIdOrNot";

}
