#ifndef USUARIOENDPOINT_H
#define USUARIOENDPOINT_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QJsonDocument>

#include "usuario/usuariomodel.h"

class UsuarioEndPoint : public QObject {
    Q_OBJECT
public:
    explicit UsuarioEndPoint( QObject* parent = nullptr );
    void post( UsuarioModel* usuario );
    QList<UsuarioModel*> getUsuarios();
    QByteArray toJson( UsuarioModel* usuario );
    QJsonDocument toJson( QByteArray json );
    void readyRead();

    QList<UsuarioModel*> toUsuarioModel( QJsonArray json );

private:
    QNetworkAccessManager* _nerworkAcessManager;
signals:

};

#endif // USUARIOENDPOINT_H
