#ifndef USUARIOCONTROLLER_H
#define USUARIOCONTROLLER_H

#include <QObject>

#include "usuario/usuariomodel.h"

class UsuarioController : public QObject {
    Q_OBJECT
public:
    explicit UsuarioController( QObject* parent = nullptr );
    ~UsuarioController();

    UsuarioModel* recuperarUsuarioLogado();
    void updateUsuarioLogado( UsuarioModel* usuario );

signals:

};

#endif // USUARIOCONTROLLER_H
