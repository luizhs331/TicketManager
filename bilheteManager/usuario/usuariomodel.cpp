#include "usuariomodel.h"

UsuarioModel::UsuarioModel( QObject* parent ) :
    QObject( parent ) {}
UsuarioModel::~UsuarioModel() {}

QString UsuarioModel::senhaUsuario() const {
    return _senhaUsuario;
}

void UsuarioModel::setSenhaUsuario( const QString& senhaUsuario ) {
    _senhaUsuario = senhaUsuario;
}

QString UsuarioModel::nomeUsuario() const {
    return _nomeUsuario;
}

void UsuarioModel::setNomeUsuario( const QString& nomeUsuario ) {
    _nomeUsuario = nomeUsuario;
}

int UsuarioModel::idUsuario() const {
    return _idUsuario;
}

void UsuarioModel::setIdUsuario( int idUsuario ) {
    _idUsuario = idUsuario;
}

int UsuarioModel::idEscolaUsuario() const
{
    return _idEscolaUsuario;
}

void UsuarioModel::setIdEscolaUsuario(int idEscolaUsuario)
{
    _idEscolaUsuario = idEscolaUsuario;
}
