#include "usuariocontroller.h"

#include "usuario/usuarioservice.h"

UsuarioController::UsuarioController( QObject* parent ) :
    QObject( parent ) {}
UsuarioController::~UsuarioController() {}

UsuarioModel* UsuarioController::recuperarUsuarioLogado() {
    return UsuarioService().recuperarUsuarioLogado();
}

void UsuarioController::updateUsuarioLogado( UsuarioModel* usuario ) {
    UsuarioService().updateUsuarioLogado( usuario );
}
