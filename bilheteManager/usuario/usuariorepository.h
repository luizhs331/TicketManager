#ifndef USUARIOREPOSITORY_H
#define USUARIOREPOSITORY_H

#include <QObject>
#include "usuario/usuariomodel.h"

class UsuarioRepository {

public:
    void clearUsers();
    void updateUsuarioLogado( UsuarioModel* usuario );
    void resetUserLogged();

    void insertUserWithAutoIncrement( const UsuarioModel* usuario );
    void insertUserWithId( const UsuarioModel* usuario );

    UsuarioModel* recuperarUsuarioLogado();
    UsuarioModel* validarUsuarioExistente( const QString& dsNomeUsuario, const QString& senha );

    // SQLS

    static constexpr const char* RECUPERAR_USUARIO_LOGADO = "RECUPERAR_USUARIO_LOGADO.txt";
    static constexpr const char* VALIDAR_USUARIO_EXISTENTE = "VALIDAR_USUARIO_EXISTENTE.txt";
    static constexpr const char* UPDATE_USUARIO_LOGADO = "UPDATE_USUARIO_LOGADO.txt";
    static constexpr const char* INSERT_WITH_AUTO_INCREMENT = "INSERT_WITH_AUTO_INCREMENT.txt";
    static constexpr const char* INSERT_WITH_ID = "INSERT_WITH_ID.txt";

};

#endif // USUARIOREPOSITORY_H
