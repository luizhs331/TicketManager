#ifndef USUARIOMODEL_H
#define USUARIOMODEL_H

#include <QObject>

class UsuarioModel : public QObject {
    Q_OBJECT
public:
    explicit UsuarioModel( QObject* parent = nullptr );
    ~UsuarioModel();

    QString senhaUsuario() const;
    void setSenhaUsuario( const QString& senhaUsuario );

    QString nomeUsuario() const;
    void setNomeUsuario( const QString& nomeUsuario );

    int idUsuario() const;
    void setIdUsuario( int idUsuario );

    int idEscolaUsuario() const;
    void setIdEscolaUsuario( int idEscolaUsuario );

    static constexpr const char* NOME_USUARIO = "dsnomeusuario";
    static constexpr const char* SENHA_USUARIO = "senha";
    static constexpr const char* ID_USUARIO = "idusuario";

private:

    int _idUsuario = 0;
    int _idEscolaUsuario = 0;

    QString _nomeUsuario = "";
    QString _senhaUsuario = "";

signals:

};

#endif // USUARIOMODEL_H
