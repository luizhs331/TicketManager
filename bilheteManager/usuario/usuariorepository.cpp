#include "usuariorepository.h"
#include "configbancogeral.h"

#include <QtSql>

#include <sqls.h>

UsuarioModel* UsuarioRepository::validarUsuarioExistente( const QString& dsNomeUsuario, const QString& senha ) {

    QSqlQuery query;

    UsuarioModel* usuarioModel = new UsuarioModel();

    query.prepare( Sqls::createQuery( VALIDAR_USUARIO_EXISTENTE, "selectsqls/usuario/" ) );

    query.bindValue( ":DS_NOME_USUARIO", dsNomeUsuario );
    query.bindValue( ":SENHA", senha );

    if( query.exec() ) {

        while( query.next() ) {
            usuarioModel->setNomeUsuario( query.value( "DS_NOME_USUARIO" ).toString() );
            usuarioModel->setIdUsuario( query.value( "ID_USUARIO" ).toInt() );
            usuarioModel->setSenhaUsuario( query.value( "SENHA" ).toString() );
        }
    }

    return usuarioModel;
}

void UsuarioRepository::updateUsuarioLogado( UsuarioModel* usuario ) {

    QSqlQuery query;
    resetUserLogged();

    query.prepare( Sqls::createQuery( UPDATE_USUARIO_LOGADO, "selectsqls/usuario/" ) );

    query.bindValue( ":ID_USUARIO", QString::number( usuario->idUsuario() ) );

    query.exec();

}

UsuarioModel* UsuarioRepository::recuperarUsuarioLogado() {
    QSqlQuery query;

    UsuarioModel* usuarioModel = new UsuarioModel();

    query.prepare( Sqls::createQuery( RECUPERAR_USUARIO_LOGADO, "selectsqls/usuario/" ) );

    if( query.exec() ) {

        while( query.next() ) {
            usuarioModel->setNomeUsuario( query.value( "DS_NOME_USUARIO" ).toString() );
            usuarioModel->setIdUsuario( query.value( "ID_USUARIO" ).toInt() );
            usuarioModel->setSenhaUsuario( query.value( "SENHA" ).toString() );
            usuarioModel->setIdEscolaUsuario( query.value( "ID_ESCOLA" ).toInt() );
        }
    }

    return usuarioModel;
}

void UsuarioRepository::resetUserLogged() {
    QSqlQuery query;
    query.prepare( "UPDATE usuario SET fgusuariologado = 'F'" );
    query.exec();
}

void UsuarioRepository::insertUserWithAutoIncrement( const UsuarioModel* usuario ) {

    QSqlQuery query;

    query.prepare( Sqls::createQuery( INSERT_WITH_AUTO_INCREMENT, "selectsqls/usuario/" ) );

    query.bindValue( ":DS_NOME_USUARIO", usuario->nomeUsuario() );
    query.bindValue( ":SENHA", usuario->senhaUsuario() );
    query.bindValue( ":ID_ESCOLA", usuario->idEscolaUsuario() );

    query.exec();
}

void UsuarioRepository::insertUserWithId( const UsuarioModel* usuario ) {

    QSqlQuery query;

    query.prepare( Sqls::createQuery( INSERT_WITH_ID, "selectsqls/usuario/" ) );

    query.bindValue( ":ID_USUARIO", usuario->idUsuario() );
    query.bindValue( ":DS_NOME_USUARIO", usuario->nomeUsuario() );
    query.bindValue( ":SENHA", usuario->senhaUsuario() );
    query.bindValue( ":ID_ESCOLA", usuario->idEscolaUsuario() );

    query.exec();

}

void UsuarioRepository::clearUsers() {

    QSqlQuery query;
    query.prepare( "DELETE FROM usuario" );
    query.exec();

}

// IMPLEMENTAR BUSCAR USUARIO LOGADO PELA TABELA controle_movimento_usuario

//SELECT
//  cmu.fgusuariologado
//FROM
//  controle_movimento_usuario cmu
//LEFT JOIN usuario u
//ON cmu.idusuario = u.idusuario
//ORDER BY cmu.idmovimento;


