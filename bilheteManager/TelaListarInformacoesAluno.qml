import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0

Rectangle {
    id: telaListarInformacoesAluno
    width: parent.width
    height: parent.height
    color: "transparent"

    property bool hasModeAllTickets: false

    Column {
        width: parent.width
        height: parent.height
        spacing: 20

        NotoRowLayout {
            title: "Nome aluno:"
            texto: modelData.nome
            colorTitle: "#414143"
            visible: !hasModeAllTickets ? true : false
        }

        NotoRowLayout {
            title: "Total de bilhetes:"
            texto: modelData.numeroTotalBilhetes
            colorTitle: "#414143"
        }

        NotoRowLayout {
            title: "Bilhetes de entrada:"
            texto: modelData.numeroBilhetesEntrada
            colorTitle: "#414143"
        }

        NotoRowLayout {
            title: "Bilhetes de Saida:"
            texto: modelData.numeroBilhetesSaida
            colorTitle: "#414143"
        }

        NotoRowLayout {
            title: "Bilhetes de Suspensao:"
            texto: "Sem Implementacao"
            colorTitle: "#414143"
        }

        NotoRowLayout {
            title: "Turma do aluno:"
            texto: modelData.turmaDoAluno
            visible: !hasModeAllTickets ? true : false
            colorTitle: "#414143"
        }

        NotoRowLayout {
            title: "Turno do aluno:"
            texto: modelData.turnoDoAluno
            visible: !hasModeAllTickets ? true : false
            colorTitle: "#414143"
        }

        NotoRowLayout {
            title: "Numero de bilhetes emitidos:"
            texto: "Sem Implementacao"
            colorTitle: "#414143"
        }

        NotoRowLayout {
            title: "Ultimo bilhete emitido em:"
            texto: modelData.dataUltimoBilhete
            colorTitle: "#414143"
        }
    }
}
