#ifndef HELPERCONVERTER_H
#define HELPERCONVERTER_H

#endif // HELPERCONVERTER_H

#include <QObject>

namespace helperConverter {

class converterEnum {
public:
    template<class TypeEnum >
    char toChar( TypeEnum tipo ) {
        QByteArray ba = tipo.toLocal8Bit();
        const char* c_str2 = ba.data();
        return *c_str2;
    }
};
}

using ConverterEnum = helperConverter::converterEnum;
