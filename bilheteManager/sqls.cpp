#include "sqls.h"

Sqls::Sqls() {
    ConfigBancoGeral::dataBaseOpenConexion();
    execMigration();
}

QString Sqls::createQuery( const QString text, const QString directory ) {

    QString retorno;

    QFile f( directory + text );
    if( f.open( QFile::ReadOnly ) ) {
        retorno = f.readAll();
    }

    return retorno;
}
void Sqls::runQuerry( QList<QString> list ) {

    QSqlQuery query;

    for( QString current : list ) {
        query.prepare( createQuery( current, CONST_DIRECTORY_MIGRATION ) );
        query.exec();
    }

}

void Sqls::execMigration() {

    QList<QString> listMigrations = {
        V1_ESTRUTURA_INICIAL_DATABASE // V1_CREATE_TABLE_ESCOLA, V2_CREATE_TABLE_USUARIO
    };

    runQuerry( listMigrations );

}
