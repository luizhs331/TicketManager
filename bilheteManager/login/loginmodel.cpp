#include "loginmodel.h"

LoginModel::LoginModel() = default;
LoginModel::~LoginModel() {}

int LoginModel::idUsuario() const {
    return _idUsuario;
}

void LoginModel::setIdUsuario( int value ) {
    _idUsuario = value;
}

QString LoginModel::senhaUsuario() const {
    return _senhaUsuario;
}

void LoginModel::setSenhaUsuario( const QString& value ) {
    _senhaUsuario = value;
}

QString LoginModel::nomeUsuario() const {
    return _nomeUsuario;
}

void LoginModel::setNomeUsuario( const QString& value ) {
    _nomeUsuario = value;
}
