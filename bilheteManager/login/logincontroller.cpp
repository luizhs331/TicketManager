#include "logincontroller.h"
#include "loginservice.h"

LoginController::LoginController() {}

void LoginController::recuperarUsuarioLogado() {
    LoginModel* usuarioLogado = LoginService().recuperarUsuarioLogado();
    setNomeUsuarioLogado( usuarioLogado->nomeUsuario() );
};

bool LoginController::loginApproved( const QString& nomeUsuario, const QString& senhaUsuario ) {
    return LoginService().loginApproved( nomeUsuario, senhaUsuario );
}

QString LoginController::nomeUsuarioLogado() const {
    return _nomeUsuarioLogado;
}

void LoginController::setNomeUsuarioLogado( const QString& value ) {
    _nomeUsuarioLogado = value;
}
