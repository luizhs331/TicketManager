#ifndef LOGINMODEL_H
#define LOGINMODEL_H

#include <QObject>

class LoginModel : public QObject {
    Q_OBJECT
public:

    LoginModel();

    ~LoginModel();

    int idUsuario() const;
    void setIdUsuario( int value );

    QString senhaUsuario() const;
    void setSenhaUsuario( const QString& value );

    QString nomeUsuario() const;
    void setNomeUsuario( const QString& value );

private:

    int _idUsuario = 0;

    QString _nomeUsuario = "";
    QString _senhaUsuario = "";

};

#endif // LOGINMODEL_H
