#ifndef LOGINSERVICE_H
#define LOGINSERVICE_H

#include "loginmodel.h"

class LoginService {
public:
    LoginService();
    bool loginApproved( const QString& nomeUsuario, const QString& senhaUsuario );
    LoginModel* recuperarUsuarioLogado();
};

#endif // LOGINSERVICE_H
