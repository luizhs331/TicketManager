#include "loginservice.h"
#include "loginrepository.h"

#include <usuario/usuariomodel.h>
#include <usuario/usuarioservice.h>

#include <escola/escolacontroller.h>

LoginService::LoginService() {}

bool LoginService::loginApproved( const QString& nomeUsuario, const QString& senhaUsuario ) {
    return UsuarioService().loginApproved( nomeUsuario, senhaUsuario );
}

LoginModel* LoginService::recuperarUsuarioLogado() {
//    UsuarioService().sincronizarUsuarios();
    return reinterpret_cast<LoginModel*>( UsuarioService().recuperarUsuarioLogado() );
    // Neste caso tmb poderia ser Utilizado o dynamic_cast
}
