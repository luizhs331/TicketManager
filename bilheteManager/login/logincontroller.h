#ifndef LOGINCONTROLLER_H
#define LOGINCONTROLLER_H

#include <QObject>

#include "loginmodel.h"

class LoginController : public QObject {
    Q_OBJECT
    Q_PROPERTY( QString nomeUsuarioLogado READ nomeUsuarioLogado WRITE setNomeUsuarioLogado NOTIFY nomeUsuarioLogadoChanged )
public:
    LoginController();
    QString nomeUsuarioLogado() const;
    void setNomeUsuarioLogado( const QString& value );

signals:
    void nomeUsuarioLogadoChanged();

public slots:
    bool loginApproved( const QString& nomeUsuario, const QString& senhaUsuario );
    void recuperarUsuarioLogado();

private:
    QString _nomeUsuarioLogado = "";
};

#endif // LOGINCONTROLLER_H
