import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12

import QtQml 2.2

import br.com.configgeral 1.0
import br.com.bilheteentradaousaida 1.0

import "qml/components"

Rectangle {

    id: root
    color: "#f1f1f1"

    signal salvarArquivo
    signal voltarTela
    signal centroBilhete(var description, var nome, var assinatura, var dateString, var turma, var turno)
    signal cabecalho(var tipoBilhete, var motivo)
    signal retornarPrint(var retorno)
    signal opcaoGerarPdf(var opcaoGerarPdf)

    property date currentDate: new Date()
    property string dateString
    property string retornoPrintar: 'Pressione Salvar Para Saida Do bilhete'
    property var retornoTurmas: configGeral.comboBox
    property var retornoTextComboBoxTipo
    property var retornoTextComboBoxTurno
    property var retornoTextComboBoxTurmaAluno
    property var locale: Qt.locale()

    onCentroBilhete: function (description, nome, assinatura, dataString, turma, turno) {
        bilheteEntradaOuSaida.centroBilhete(description, nome, assinatura,
                                            dataString, turma, turno)
    }
    onCabecalho: function (tipoBilhete, motivo) {
        bilheteEntradaOuSaida.cabesalhoBilhete(tipoBilhete, motivo)
    }

    onOpcaoGerarPdf: function (opcaoGerarPdf) {
        bilheteEntradaOuSaida.setOpcaoGerarPdf(opcaoGerarPdf)
    }

    onSalvarArquivo: function () {
        bilheteEntradaOuSaida.salvarArquivoFormatado()
    }

    BilheteEntradaOuSaida {
        id: bilheteEntradaOuSaida
    }

    ConfigGeral {
        id: configGeral
    }

    function validarEnable() {
        if (nome.text === '' || motivo.text === '' || assinatura.text === ''
                || retornoTextComboBoxTurmaAluno === ''
                || retornoTextComboBoxTurno === '') {
            return false
        }
        return true
    }

    function textFieldDisable() {
        btn.enabled = false
        mycomboTurno1.enabled = false
        mycomboTurno.enabled = false
        myswitch.enabled = false
        description.enabled = false
        nome.enabled = false
        motivo.enabled = false
        assinatura.enabled = false
        btn2.enabled = false
        bilheteTipoBilhete.enabled = false
    }

    function textFieldEnable() {
        btn.enabled = true
        mycomboTurno1.enabled = true
        mycomboTurno.enabled = true
        myswitch.enabled = true
        description.enabled = true
        nome.enabled = true
        motivo.enabled = true
        assinatura.enabled = true
        btn2.enabled = true
        bilheteTipoBilhete.enabled = true
    }

    onRetornarPrint: {
        retornoPrintar = bilheteEntradaOuSaida.arquivoPrintar()
    }

    QtObject {
        Component.onCompleted: {
            dateString = currentDate.toLocaleDateString()
            print(dateString)
        }
    }

    function reset() {
        description.text = ''
        nome.text = ''
        motivo.text = ''
        assinatura.text = ''
        textFieldTurma.focus = true
    }

    Rectangle {
        width: parent.width
        height: parent.height
        color: "#f1f1f1"

        Row {
            width: root.width
            height: root.height
            spacing: 0

            Rectangle {
                id: container
                width: parent.width / 1.9
                height: root.height / 1.1
                anchors.verticalCenter: parent.verticalCenter
                radius: 10
                color: "#f1f1f1"

                Rectangle {
                    anchors.fill: parent
                    color: "#f1f1f1"
                    width: parent.width
                    anchors.margins: 55
                    anchors.topMargin: 35

                    Column {
                        id: column
                        width: parent.width
                        spacing: 7

                        RowLayout {
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            anchors.bottom: container.top
                            Label {
                                text: "GERAR-PDF" /*+ " = " + myswitch.position*/
                                font.pointSize: 13
                                horizontalAlignment: Label.AlignHCenter
                                verticalAlignment: Label.AlignVCenter
                            }
                            Switch {
                                id: myswitch
                                text: myswitch.position == 1 ? "SIM" : "NAO"
                                font.pointSize: 13
                            }
                        }

                        Row {
                            id: rowNotoComboBox
                            width: parent.width
                            spacing: 20

                            NotoComboBox {
                                id: bilheteTipoBilhete
                                width: root.width / 4.6
                                height: root.height / 16
                                isTextConcat: true
                                modelTxt: ["", "ENTRADA", "SAIDA"]
                                textEditComboBox: "BILHETE DE:"
                                onEmitirText: function (texto) {
                                    root.retornoTextComboBoxTipo = texto
                                }
                            }
                            NotoComboBox {
                                id: mycomboTurno
                                width: root.width / 4.6
                                height: root.height / 16
                                isTextConcat: true
                                modelTxt: ["", "MANHA", "TARDE", "NOITE"]
                                textEditComboBox: "TURNO ALUNO:"
                                onEmitirText: function (texto) {
                                    root.retornoTextComboBoxTurno = texto
                                }
                            }
                        }

                        Row {
                            width: root.width

                            NotoComboBox {
                                id: mycomboTurno1
                                width: root.width / 2.3 + rowNotoComboBox.spacing
                                height: root.height / 16
                                modelTxt: retornoTurmas
                                isTextConcat: true
                                textEditComboBox: "TURMA ALUNO:"
                                onEmitirText: function (texto) {
                                    root.retornoTextComboBoxTurmaAluno = texto
                                }
                            }
                        }

                        TextField {
                            id: nome
                            placeholderText: "Nome Aluno"
                            width: parent.width / 2
                            anchors.left: parent.left
                            height: root.height / 16.5
                            background: Rectangle {
                                radius: 5
                                border.color: nome.focus ? "#11cdef" : "#d3d3d3"
                            }
                        }

                        TextField {
                            id: motivo
                            placeholderText: "Motivo"
                            width: root.width / 2.3 + rowNotoComboBox.spacing
                            height: root.height / 16.5
                            background: Rectangle {
                                radius: 5
                                border.color: motivo.focus ? "#11cdef" : "#d3d3d3"
                            }
                        }
                        TextField {
                            id: assinatura
                            placeholderText: "Assinatura Pedagogo"
                            width: parent.width / 2
                            height: root.height / 16.5
                            background: Rectangle {
                                radius: 5
                                border.color: assinatura.focus ? "#11cdef" : "#d3d3d3"
                            }
                        }

                        TextArea {
                            id: description
                            placeholderText: "Complemento"
                            width: root.width / 2.3 + rowNotoComboBox.spacing
                            height: root.height / 4
                            wrapMode: "WordWrap"
                            background: Rectangle {
                                border.color: description.focus ? "#11cdef" : "#d3d3d3"
                                radius: 5
                            }
                        }
                        Row {
                            spacing: 20

                            Button {
                                id: btn
                                width: ratangulo.width / 3
                                height: ratangulo.height / 11
                                spacing: 10
                                background: Rectangle {
                                    color: colorButtonSalvar()
                                    anchors.left: btn2.right
                                    radius: 5

                                    function colorButtonSalvar() {

                                        var colorText = ""
                                        var opacity = 1.0
                                        var colorButton = "#a9a9a9"

                                        if (!mouseAreaBtnSalvar.enabled) {
                                            colorText = "#fff"
                                            colorButton = "#a9a9a9"
                                        } else if (mouseAreaBtnSalvar.enabled
                                                   && mouseAreaBtnSalvar.containsMouse) {
                                            colorText = "#fff"
                                            opacity = 1.0
                                            colorButton = "#11cdef"
                                        } else {
                                            opacity = 0.6
                                            colorText = "#000"
                                            colorButton = "#dfe0e1"
                                        }

                                        labelTxtSalvar.color = colorText
                                        labelTxtSalvar.opacity = opacity
                                        return colorButton
                                    }

                                    Label {
                                        id: labelTxtSalvar
                                        text: "Salvar"
                                        font.family: "Helvetica"
                                        font.pointSize: 15
                                        color: !mouseAreaBtnSalvar.enabled ? "#fff" : "#000"
                                        anchors.horizontalCenter: parent.horizontalCenter
                                        anchors.verticalCenter: parent.verticalCenter
                                        opacity: !mouseAreaBtnSalvar.enabled ? 0.8 : 0.6
                                    }
                                }
                                MouseArea {
                                    id: mouseAreaBtnSalvar
                                    anchors.fill: parent
                                    hoverEnabled: true
                                    enabled: root.validarEnable()

                                    onClicked: {
                                        root.centroBilhete(
                                                    description.text,
                                                    nome.text, assinatura.text,
                                                    dateString,
                                                    retornoTextComboBoxTurmaAluno,
                                                    retornoTextComboBoxTurno)
                                        root.cabecalho(
                                                    retornoTextComboBoxTipo,
                                                    /*emitidopor.text*/ motivo.text)
                                        root.textFieldDisable()
                                        telaLoading.visible = true
                                        root.opcaoGerarPdf(myswitch.position)
                                        root.salvarArquivo()
                                        root.retornarPrint(retornoPrintar)
                                        root.reset()
                                    }
                                }
                            }

                            ButtonCriado {
                                id: btn2
                                width: ratangulo.width / 3
                                height: ratangulo.height / 11
                                spacing: 10
                                buttonName: "Voltar"
                                onClicked: {
                                    root.voltarTela()
                                }
                            }
                        }
                    }
                }
            }

            Row {
                width: parent.width
                height: parent.height

                Rectangle {
                    id: ratangulo
                    width: parent.width / 2.3
                    height: parent.height / 1.1
                    color: "#f1f1f1"

                    Rectangle {

                        width: root.width / 2.2
                        height: root.height / 1.4

                        anchors.centerIn: parent
                        color: "#fff"
                        border.color: "#11cdef"
                        border.width: 1
                        radius: 2

                        TextArea {
                            id: textArea
                            font.pointSize: 12
                            anchors.centerIn: parent
                            text: retornoPrintar
                            color: "#000"
                            enabled: false
                            background: Rectangle {
                                border.color: "#9da7b1"
                                radius: 3
                            }
                        }
                    }
                }
            }
        }
    }

    TelaLoading {
        id: telaLoading
        visible: false
        width: parent.width / 4
        height: parent.height / 2
        anchors.centerIn: parent
        statusFailOrSucess: bilheteEntradaOuSaida.selectAnimationFailOrSucess
        animationRunnigFalseOrTrue: bilheteEntradaOuSaida.animationRunnig

        onButtonPressed: {
            root.textFieldEnable()
            telaLoading.close()
        }
    }
}
