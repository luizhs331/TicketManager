import QtQuick 2.6

Item {

    property bool usuarioLogado: mainWindow.hasUsuarioLogado

    Shortcut {
        sequence: "F11"
        context: Qt.ApplicationShortcut
        onActivated: function () {

            if (validarFullScreenWindow) {
                mainWindow.validarFullScreenWindow = false
            } else {
                mainWindow.validarFullScreenWindow = true
            }

            if (usuarioLogado) {
                mainWindow.validarFullScreen(mainWindow.validarFullScreenWindow)
            }
        }
    }

    Shortcut {
        sequence: "F2"
        context: Qt.ApplicationShortcut
        onActivated: function () {
            if (usuarioLogado) {
                mainWindow.abrirHelTeclas()
            }
        }
    }

    Shortcut {
        sequence: "E"
        context: Qt.ApplicationShortcut
        onActivated: function () {
            if (usuarioLogado) {
                mainWindow.abrirTelaAtalhoCadastro()
            }
        }
    }

    Shortcut {
        sequence: "L"
        context: Qt.ApplicationShortcut
        onActivated: function () {
            if (usuarioLogado) {
                mainWindow.abrirTelaDeBusca()
            }
        }
    }

    Shortcut {
        sequence: "F1"
        context: Qt.ApplicationShortcut
        onActivated: function () {
            if (usuarioLogado) {
                mainWindow.abrirPopupExcluirBilhete()
            }
        }
    }

    Shortcut {
        sequence: "F8"
        context: Qt.ApplicationShortcut
        onActivated: function () {
            if (usuarioLogado) {
                mainWindow.abrirTelaUpdateBilhete()
            }
        }
    }

    Shortcut {
        sequence: "Q" // /*"Ctrl+Q"*/
        context: Qt.ApplicationShortcut
        onActivated: function () {
            if (usuarioLogado) {
                mainWindow.abrirTelaConfig()
            }
        }
    }

    Shortcut {
        sequence: "Esc"
        context: Qt.ApplicationShortcut
        onActivated: function () {
            if (usuarioLogado) {
                if (fecharAplicacao.visible) {
                    fecharAplicacao.visible = false
                } else {
                    mainWindow.abrirPoPupCloseAplicacao()
                }
            }
        }
    }
}
