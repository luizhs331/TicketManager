#ifndef SQLS_H
#define SQLS_H

#include <QObject>

#include <QSql>
#include <QSqlQuery>
#include <QFile>

#include <configbancogeral.h>

class Sqls {
public:
    Sqls();
    static QString createQuery( const QString text, const QString directory );
    static void runQuerry( QList<QString> list );
    static void execMigration();

    static constexpr const char* CONST_DIRECTORY_SELECTS = "selectsqls/";
    static constexpr const char* CONST_DIRECTORY_MIGRATION = "migration/";

    // Selects
    static constexpr const char* V1_SELECT_ULTIMO_ID = "V1_SELECT_ULTIMO_ID.txt";

    // Migrations
    static constexpr const char* V1_ESTRUTURA_INICIAL_DATABASE = "V1_ESTRUTURA_INICIAL_BANCO.txt";
    static constexpr const char* V1_CREATE_TABLE_ESCOLA = "V1_CREATE_TABLE_ESCOLA.txt";
    static constexpr const char* V2_CREATE_TABLE_USUARIO = "V2_CREATE_TABLE_USUARIO.txt";
};

#endif // SQLS_H
