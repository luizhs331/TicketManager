import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

RowLayout {
    id: rowLayout
    Layout.fillWidth: true
    spacing: 6

    property string title
    property string texto

    property string colorText: "#87cefa"
    property string colorTitle: "#27273f"

    property int pointSize: 16

    property int titlePreferredWidth: 100
    property int textPreferredWidth: 500

    property bool isManualAlign: false

    onVisibleChanged: {

        if (isManualAlign) {
            name1.Layout.preferredWidth = titlePreferredWidth
            name2.Layout.preferredWidth = textPreferredWidth
        } else {
            name1.width = isManualAlign ? 0 : 200
            name2.width = isManualAlign ? 0 : 200
        }
    }

    Label {
        id: name1
        text: title
        font.pointSize: pointSize
        font.italic: true
        color: colorTitle
        wrapMode: Label.Wrap
        font.family: "Calibri"
    }

    Label {
        id: name2
        text: texto
        font.pointSize: pointSize
        font.italic: true
        color: colorText
        wrapMode: Label.Wrap
    }
}
