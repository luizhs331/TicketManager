import QtQuick 2.0
import QtQuick.Layouts 1.4
import QtQuick.Controls 2.0
import br.com.configgeral 1.0

Item {
    id: segundaTab

    ConfigGeral {
        id: configGeral
    }

    property bool fgIntegracaoAtiva: configGeral.selectDadosIntegracao()

    signal buttonSavePressed
    signal processoConcluidoStatus(var result)

    onButtonSavePressed: {
        configGeral.updateDadosIntegracao(mySwitchEnviarDados.position)
        processoConcluidoStatus(true)
    }

    Rectangle {
        id: terceiroRetangulo
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        color: "#f1f1f1"

        Column {
            anchors.centerIn: parent
            width: parent.width - (segundaTab.width / 10)
            height: parent.height - (segundaTab.width / 10)
            spacing: 10

            Label {
                font.pointSize: 18
                color: "#000"
                text: "Configurações de integração"
            }

            RowLayout {
                Layout.fillWidth: true
                Layout.fillHeight: true

                Label {
                    text: "Enviar bilhete"
                    font.pixelSize: 16
                    horizontalAlignment: Label.AlignHCenter
                    verticalAlignment: Label.AlignVCenter
                }

                Switch {
                    id: mySwitchEnviarDados
                    checked: fgIntegracaoAtiva
                    text: mySwitchEnviarDados.position == 1 ? "SIM" : "NAO"
                    font.pointSize: 13
                }
            }
        }
    }
}
