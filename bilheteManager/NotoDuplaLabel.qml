import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.12

Rectangle {
    width: parent.width / 1.5
    height: parent.height / 6
    anchors.centerIn: parent
    radius: 10
    visible: false
    color: "#dfe0e1"

    property string textLabel
    property string colorRetangleNumberColor

    Rectangle {
        id: rectangueLabelDescricao
        width: parent.width - 60
        height: parent.height
        anchors.right: parent.right
        color: "transparent"
        Label {
            text: textLabel
            font.family: "Helvetica"
            font.pointSize: 11
            color: "#000"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            opacity: 0.7
        }
    }

    Rectangle {
        anchors.left: parent.left
        width: 60
        height: parent.height
        color: colorRetangleNumberColor
        Label {
            text: "1"
            font.family: "Helvetica"
            font.pointSize: 18
            color: "#fff"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            opacity: mouseAreaNovoBilhete.containsMouse ? 1 : 0.6
        }
    }
}
