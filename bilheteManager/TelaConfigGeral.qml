import QtQuick 2.6
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.1
import QtQml 2.2

import QtQuick.Controls.Universal 2.0

import br.com.configgeral 1.0

import "qml/components"

Rectangle {
    id: retangulo
    anchors.fill: parent
    color: colorBackground

    ConfigGeral {
        id: configGeral
    }

    // Material.theme : Material.Dark
    Universal.accent: Universal.Red

    // Universal.foreground: Universal.Indigo
    property string colorBackground: "#f1f1f1"
    property bool validAnimationFailOrSucess: configGeral.animationFailOrSucess
    property var retornoTextComboBox
    property var mensagemFailOrSucess: "Sucesso!"

    signal voltarPagina
    signal mudarResulucaoTela(var index)
    signal buttonSalvarPressed
    signal runAnimationSuccessOrFail(var success)

    onButtonSalvarPressed: {
        stack.currentItem.buttonSavePressed()
    }

    onRunAnimationSuccessOrFail: function (success) {
        imgSucess.visible = true
        imgSucess.validaStatusAnimation = success
        imgSucess.runAnimation(success)
        animation.running = true
    }

    function fecharTela() {
        retangulo.visible = false
    }

    ListModel {
        id: nameModelListEsquerda
        ListElement {
            name: "Basicas"
            enumType: "CONFIG_BASICAS"
        }
        ListElement {
            name: "Integrações"
            enumType: "INTEGRACOES"
        }

        ListElement {
            name: "Escola"
            enumType: "CONFIG_ESCOLA"
        }

        ListElement {
            name: "Turmas"
            enumType: "CONFIG_TURMA"
        }
    }

    function validarButtonClicked(enumType) {

        switch (enumType) {
        case "CONFIG_BASICAS":
            stack.push(configBasica)
            break
        case "INTEGRACOES":
            stack.push(configIntegracao)
            break
        case "CONFIG_ESCOLA":
            stack.push(configEscola)
            break
        case "CONFIG_TURMA":
            stack.push(configTurmas)
            break
        }
    }

    Row {
        width: retangulo.width

        Rectangle {
            id: retanguloEsquerda
            height: retangulo.height
            width: retangulo.width / 4.5
            color: "#f1f1f1"
            anchors.left: parent
            border.color: "#dfe0e1"

            Flickable {
                id: flickableMenuEsquerda
                width: retanguloEsquerda.width
                height: parent.height / 1.1
                contentHeight: todoListMenuEsqueda.height
                anchors.centerIn: parent
                clip: true

                Column {
                    id: todoListMenuEsqueda
                    width: flickableMenuEsquerda.width
                    spacing: 5

                    Row {
                        spacing: 10

                        anchors.left: todoListMenuEsqueda.left
                        anchors.leftMargin: 30

                        Image {
                            id: imgMensagem
                            width: 30
                            height: 30
                            source: "img/img.png"
                        }

                        Label {
                            id: labelOpcoes
                            text: "Opções"
                            font.family: "Helvetica"
                            font.pointSize: 20
                            color: "#000"
                            opacity: 1
                        }
                    }

                    Repeater {
                        model: nameModelListEsquerda

                        Column {
                            width: todoListMenuEsqueda.width
                            anchors.horizontalCenter: retanguloEsquerda.horizontalCenter
                            height: 50

                            Rectangle {
                                width: todoListMenuEsqueda.width
                                height: 1
                                color: "#dfe0e1"
                            }

                            Rectangle {
                                id: recTeferenceWidth
                                width: retanguloEsquerda.width - 1
                                height: 55
                                color: mouseAreaConfig.containsMouse ? "#f5faf9" : "transparent"

                                Rectangle {
                                    id: rectangleAnimationSelected
                                    height: 55
                                    width: 5
                                    color: "#11cdef"
                                    visible: mouseAreaConfig.containsMouse
                                }

                                Button {
                                    id: buttonConfig
                                    width: textConfig.width
                                    height: textConfig.height
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.left: recTeferenceWidth.left
                                    anchors.leftMargin: 30

                                    style: ButtonStyle {
                                        background: Rectangle {
                                            color: "transparent"
                                        }
                                    }

                                    Label {
                                        id: textConfig
                                        text: name
                                        font.family: "Helvetica"
                                        font.pointSize: 14
                                        color: "#000"
                                        opacity: mouseAreaConfig.containsMouse ? 0.8 : 0.6
                                        anchors.verticalCenter: buttonConfig.verticalCenter
                                    }
                                    MouseArea {
                                        id: mouseAreaConfig
                                        hoverEnabled: true
                                        anchors.fill: parent
                                        onClicked: {
                                            validarButtonClicked(enumType)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        Column {

            StackView {
                id: stack
                width: retangulo.width - retanguloEsquerda.width
                height: retangulo.height - (retanguloEsquerda.width / 2.5)
                initialItem: configBasica

                delegate: StackViewDelegate {
                    pushTransition: StackViewTransition {}
                }
            }

            Component {
                id: flickableId
                Flickable {
                    width: stack.width
                    height: stack.height
                    contentHeight: stack.height
                    clip: true
                }
            }

            ConfiguracoesBasicas {
                id: configBasica
                visible: false

                onProcessoConcluidoStatus: function (result) {
                    runAnimationSuccessOrFail(result)
                }
            }

            ConfiguracoesDeIntegracao {
                id: configIntegracao
                visible: false

                onProcessoConcluidoStatus: function (result) {
                    runAnimationSuccessOrFail(result)
                }
            }

            ConfiguracoesTurma {
                id: configTurmas
                visible: false

                onProcessoConcluidoStatus: function (result) {
                    runAnimationSuccessOrFail(result)
                }
            }

            ConfiguracoesEscola {
                id: configEscola
                visible: false

                onProcessoConcluidoStatus: function (result) {
                    runAnimationSuccessOrFail(result)
                }
            }

            Rectangle {
                id: rectangleBottom
                color: colorBackground
                width: retangulo.width
                height: retangulo.height / 6
                z: 0

                Column {
                    id: columnBottom
                    width: retangulo.width / 2
                    height: rectangleBottom.height
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: (rectangleBottom.height / 2) - (buttonVoltar.height / 2)

                    Rectangle {
                        width: rectangleBottom.width
                        height: 2
                        color: "#dfe0e1"
                        opacity: 1
                        z: 1
                    }

                    Row {
                        id: linha
                        width: columnBottom.width
                        height: columnBottom.height
                        spacing: 20
                        z: 1

                        Rectangle {
                            color: "transparent"
                            width: 40
                            height: 50
                        }

                        ButtonCriado {
                            width: parent.width / 4
                            height: parent.height / 2.5
                            buttonName: "Salvar"
                            colorButtonPressed: "#afc0ff"
                            colorBorderPressed: "#afc0ff"
                            onClicked: {
                                retangulo.buttonSalvarPressed()
                            }
                        }
                        ButtonCriado {
                            id: buttonVoltar
                            width: parent.width / 4
                            height: parent.height / 2.5
                            buttonName: "Voltar"
                            colorButtonPressed: "#afc0ff"
                            colorBorderPressed: "#afc0ff"
                            onClicked: {
                                retangulo.voltarPagina()
                                retangulo.ativarCheck()
                            }
                        }
                    }
                }
            }
        }
    }

    NotoFailOrSucess {
        id: imgSucess
        width: 330
        height: 150
        anchors.right: parent.right
        color: "transparent"
        visible: false
        mensage: mensagemFailOrSucess.toString()
        mensagemErro: "Erro!"
        fromYinit: 45
        fromYdone: 25
    }
}
