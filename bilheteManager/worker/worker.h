#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QAuthenticator>
#include <QNetworkProxy>

class Worker : public QObject {
    Q_OBJECT
public:
    explicit Worker( QObject* parent = nullptr );

signals:

public slots:
    QByteArray get( const QString& location, const int& timeout = 5000 );
    void post( QString location, QByteArray data );
private slots:
    void readyRead();
    void authenticationRequired( QNetworkReply* reply, QAuthenticator* authenticator );
    void encrypted( QNetworkReply* reply );
    void finished( QNetworkReply* reply );
    void networkAccessibleChanged( QNetworkAccessManager::NetworkAccessibility acessible );
    void preSharedKeyAuthenticationRequired( QNetworkReply* reply, QSslPreSharedKeyAuthenticator* authenticator );
    void proxyAuthenticationRequired( const QNetworkProxy& proxy, QAuthenticator* authenticator );
    void sslErrors( QNetworkReply* reply, const QList<QSslError>& errors );
    int requestStatus( int timeout );

private:
    QNetworkAccessManager manager;
    QNetworkReply* reply;
};

#endif // WORKER_H
