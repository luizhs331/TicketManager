#include "worker.h"

#include <QEventLoop>
#include <QTimer>

Worker::Worker( QObject* parent ) :
    QObject( parent ) {

    connect( &manager, &QNetworkAccessManager::authenticationRequired, this, &Worker::authenticationRequired );
    connect( &manager, &QNetworkAccessManager::encrypted, this, &Worker::encrypted );
    connect( &manager, &QNetworkAccessManager::networkAccessibleChanged, this, &Worker::networkAccessibleChanged );
    connect( &manager, &QNetworkAccessManager::preSharedKeyAuthenticationRequired, this, &Worker::preSharedKeyAuthenticationRequired );
    connect( &manager, &QNetworkAccessManager::proxyAuthenticationRequired, this, &Worker::proxyAuthenticationRequired );
    connect( &manager, &QNetworkAccessManager::sslErrors, this, &Worker::sslErrors );
}

QByteArray Worker::get( const QString& location, const int& timeout ) {

    qInfo() << "get server ...";

    QEventLoop loop;
    QByteArray array;

    reply = manager.get( QNetworkRequest( QUrl( location ) ) );

    QObject::connect( reply, &QNetworkReply::readyRead, &loop, &QEventLoop::quit );
    QObject::connect( reply, &QNetworkReply::readyRead, this, [&]() {
        reply = qobject_cast<QNetworkReply*>( sender() );
        array = reply->readAll();
        if( reply ) qInfo() << array;
    } );

    requestStatus( timeout );

    qInfo() << "[RETURN]" << array;

    return array;
}

void Worker::post( QString location, QByteArray data ) {

    qInfo() << "Posting server ...";
    QNetworkRequest request = QNetworkRequest( QUrl( location ) );
    request.setHeader( QNetworkRequest::ContentTypeHeader, "application/json" );
    QNetworkReply* reply = manager.post( request, data );
    connect( reply, &QNetworkReply::readyRead, this, &Worker::readyRead );

}

void Worker::readyRead() {
    qInfo() << "readyRead";
    reply  = qobject_cast<QNetworkReply*>( sender() );
    QByteArray array = reply->readAll();
    if( reply ) qInfo() << array;
}

void Worker::authenticationRequired( QNetworkReply* reply, QAuthenticator* authenticator ) {
    Q_UNUSED( reply );
    Q_UNUSED( authenticator );
    qInfo() << "authenticationRequired";

}

void Worker::encrypted( QNetworkReply* reply ) {
    Q_UNUSED( reply );
    qInfo() << "encrypted";
}

void Worker::finished( QNetworkReply* reply ) {
    Q_UNUSED( reply );
    qInfo() << "finished";
}

void Worker::networkAccessibleChanged( QNetworkAccessManager::NetworkAccessibility acessible ) {
    Q_UNUSED( acessible );
    qInfo() << "networkAccessibleChanged";
}

void Worker::preSharedKeyAuthenticationRequired( QNetworkReply* reply, QSslPreSharedKeyAuthenticator* authenticator ) {
    Q_UNUSED( authenticator );
    Q_UNUSED( reply );
    qInfo() << "preSharedKeyAuthenticationRequired";
}

void Worker::proxyAuthenticationRequired( const QNetworkProxy& proxy, QAuthenticator* authenticator ) {
    Q_UNUSED( proxy );
    Q_UNUSED( authenticator );
    qInfo() << "proxyAuthenticationRequired";
}

void Worker::sslErrors( QNetworkReply* reply, const QList<QSslError>& errors ) {
    Q_UNUSED( reply );
    Q_UNUSED( errors );
    qInfo() << "sslErrors";

}

int Worker::requestStatus( int timeout ) {

    QTimer timer;
    timer.setSingleShot( true );

    QEventLoop loop;
    QObject::connect( &timer, &QTimer::timeout, &loop, &QEventLoop::quit );
    QObject::connect( reply, &QNetworkReply::finished, &loop, &QEventLoop::quit );
    timer.start( timeout );
    loop.exec();

    if( timer.isActive() ) {
        timer.stop();
        if( reply->error() > 0 ) {
            qDebug() << "Erro Request";
            return 404;
        }else {

            int status = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
            if ( status >= 200 && status < 300 ) { // Success
                qDebug() << "Sucess";
                return 200;
            }
        }
    } else {
        // timeout
        QObject::disconnect( reply, &QNetworkReply::finished, &loop, &QEventLoop::quit );
        reply->abort();
        return 599;
    }
    return 404;

}
