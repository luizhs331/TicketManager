import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12

import br.com.bilheteentradaousaida 1.0
import br.com.processocontroller 1.0

import "qml/components"

PoPupGenerico {

    id: processo
    modal: false
    visible: false
    width: parent.width
    height: parent.height
    anchors.centerIn: parent

    colorTop: "#f06363"
    sizePointSize: 17
    heightRectangleTop: 100
    descricaoPoPup: descricaoProcesso

    property string descricaoProcesso
    property string idBilhete

    signal processAccepted
    signal processoStatusValidar(var status)

    onVisibleChanged: {
        textFieldSenhaProcesso.hasFocusEnable = true
    }

    function validarSenhaProcesso() {

        var processoLiberado = processoController.validarSenhaProcesso(
                    textFieldSenhaProcesso.currentText)

        if (processoLiberado) {
            textFieldSenhaProcesso.currentText = ""
            textFieldSenhaProcesso.borderColorValidate = true
            processo.visible = false
            processo.processAccepted()
        } else {
            textFieldSenhaProcesso.borderColorValidate = false
        }
    }

    ProcessoController {
        id: processoController
    }

    BilheteEntradaOuSaida {
        id: bilheteEntradaOuSaida
    }

    Column {
        anchors.centerIn: parent
        spacing: 50

        Rectangle {
            width: processo.width / 3.5
            height: processo.height / 8
            color: "transparent"
        }

        Column {
            anchors.horizontalCenter: parent.horizontalCenter

            TextFieldWithText {
                id: textFieldSenhaProcesso
                width: processo.width / 1.1
                height: processo.height / 4.5
                titleField: ""
                typeIsPassword: true
                align: TextInput.AlignHCenter
                borderColor: popup.borderColor
                customRegex: RegExpValidator {
                    regExp: /[0-9A-F]+/
                }
            }
        }

        Row {
            spacing: 10
            anchors.horizontalCenter: parent.horizontalCenter

            ButtonCriado {
                id: btnConfirmarProcesso
                width: processo.width / 3.5
                height: processo.height / 6
                buttonName: "Confirmar"
                onClicked: {
                    processo.validarSenhaProcesso()
                }
            }

            ButtonCriado {
                id: btnVoltarProcesso
                width: processo.width / 3.5
                height: processo.height / 6
                buttonName: "Voltar"
                onClicked: {
                    processo.close()
                }
            }
        }
    }
}
