QT += core network quick printsupport sql quickcontrols2 concurrent widgets

CONFIG += c++11 qml_debug #or CONFIG+=declarative_debug for QtQuick 1

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ScreenControllers/menuinicalcontroller.cpp \
    aluno/alunocontroller.cpp \
    aluno/alunodelegate.cpp \
    aluno/alunorepository.cpp \
    aluno/alunoservice.cpp \
    bilheteentradaousaida/bilheteentradaousaidacontroller.cpp \
    bilheteentradaousaida/bilheteentradaousaidaendpoint.cpp \
    bilheteentradaousaida/bilheteentradaousaidamodel.cpp \
    bilheteentradaousaida/bilheteentradaousaidarepository.cpp \
    bilheteentradaousaida/bilheteentradaousaidaservice.cpp \
    bilheteentradaousaidadto.cpp \
        bilheteformatar.cpp \
        cabecalhobilhete.cpp \
    commons/basicselectionobject.cpp \
    components/stepcontroller.cpp \
    components/stepmodel.cpp \
    escola/escolacontroller.cpp \
    escola/escolaendpoint.cpp \
    escola/escolamodel.cpp \
    escola/escolarepository.cpp \
    escola/escolaservice.cpp \
        centrobilhete.cpp \
        configbancogeral.cpp \
    configcaixageral/configcaixageralcontroller.cpp \
    configcaixageral/configcaixageralmodel.cpp \
    configcaixageral/configcaixageralrepository.cpp \
    configcaixageral/configcaixageralservice.cpp \
        gerararquivopdf.cpp \
    login/logincontroller.cpp \
    login/loginmodel.cpp \
    login/loginrepository.cpp \
    login/loginservice.cpp \
        main.cpp \
    notificacao/notificacoescontroller.cpp \
    notificacao/notificacoesmodel.cpp \
    notificacao/notificacoesrepository.cpp \
    notificacao/notificacoesservice.cpp \
    processo/processocontroller.cpp \
    processo/processoservice.cpp \
    sala/salacontroller.cpp \
    sala/saladelegate.cpp \
    sala/saladto.cpp \
    sala/salamodel.cpp \
    sala/salarepository.cpp \
    sala/salaservice.cpp \
        salvartextstring.cpp \
    sql/notosqlquery.cpp \
    sqls.cpp \
        tododtoinformacoesteladebusca.cpp \
    turma/cadastroturma/cadastroturmamanager.cpp \
    turma/turmacontroller.cpp \
    turma/turmadto.cpp \
    turma/turmamodel.cpp \
    turma/turmarepository.cpp \
    turma/turmaservice.cpp \
    usuario/usuariocontroller.cpp \
    usuario/usuarioendpoint.cpp \
    usuario/usuariomodel.cpp \
    usuario/usuariorepository.cpp \
    usuario/usuarioservice.cpp \
    worker/worker.cpp

RESOURCES += qml.qrc \
    sqls.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    ScreenControllers/menuinicalcontroller.h \
    aluno/alunocontroller.h \
    aluno/alunodelegate.h \
    aluno/alunorepository.h \
    aluno/alunoservice.h \
    bilheteentradaousaida/bilheteentradaousaidacontroller.h \
    bilheteentradaousaida/bilheteentradaousaidaendpoint.h \
    bilheteentradaousaida/bilheteentradaousaidamodel.h \
    bilheteentradaousaida/bilheteentradaousaidarepository.h \
    bilheteentradaousaida/bilheteentradaousaidaservice.h \
    bilheteentradaousaidadto.h \
    bilheteformatar.h \
    cabecalhobilhete.h \
    commons/basicselectionobject.h \
    commons/icons.h \
    components/stepcontroller.h \
    components/stepmodel.h \
    escola/escolacontroller.h \
    escola/escolaendpoint.h \
    escola/escolamodel.h \
    escola/escolarepository.h \
    escola/escolaservice.h \
    centrobilhete.h \
    configbancogeral.h \
    configcaixageral/configcaixageralcontroller.h \
    configcaixageral/configcaixageralmodel.h \
    configcaixageral/configcaixageralrepository.h \
    configcaixageral/configcaixageralservice.h \
    gerararquivopdf.h \
    helperconverter.h \
    login/logincontroller.h \
    login/loginmodel.h \
    login/loginrepository.h \
    login/loginservice.h \
    notificacao/notificacoescontroller.h \
    notificacao/notificacoesmodel.h \
    notificacao/notificacoesrepository.h \
    notificacao/notificacoesservice.h \
    notificacao/tiponotificacaoenum.h \
    processo/processocontroller.h \
    processo/processoservice.h \
    sala/salacontroller.h \
    sala/saladelegate.h \
    sala/saladto.h \
    sala/salamodel.h \
    sala/salarepository.h \
    sala/salaservice.h \
    salvartextstring.h \
    sql/notosqlquery.h \
    sqls.h \
    tododtoinformacoesteladebusca.h \
    turma/cadastroturma/cadastroturmamanager.h \
    turma/turmacontroller.h \
    turma/turmadto.h \
    turma/turmamodel.h \
    turma/turmarepository.h \
    turma/turmaservice.h \
    usuario/usuariocontroller.h \
    usuario/usuarioendpoint.h \
    usuario/usuariomodel.h \
    usuario/usuariorepository.h \
    usuario/usuarioservice.h \
    worker/worker.h

DISTFILES += \
#    ../../../Downloads/img10.png \
#    ../../../Downloads/img12.jpg \
#    ../../../Downloads/img2.png \
#    ../../../Downloads/img3.png \
#    ../../../Downloads/img4.png \
#    ../../../Downloads/img5.png \
#    ../../../Downloads/img6.png \
#    ../../../Downloads/img7.png \
#    ../../../Downloads/img8.png \
#    ../../../Downloads/img9.png \
