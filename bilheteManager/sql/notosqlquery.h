#ifndef NOTOSQLQUERY_H
#define NOTOSQLQUERY_H

#include <QSqlQuery>

class NotoSqlQuery : public QSqlQuery{
public:
    void bindParam( QString placeholder, const QVariant& val,QSql::ParamType type = QSql::In);
    void bindParam(int pos, const QVariant& val, QSql::ParamType type = QSql::In);
};

#endif // NOTOSQLQUERY_H
