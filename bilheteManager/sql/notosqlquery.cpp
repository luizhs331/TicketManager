#include "notosqlquery.h"

void NotoSqlQuery::bindParam( QString placeholder, const QVariant &val, QSql::ParamType type){
    bindValue( placeholder.prepend(":"), val, type);
}

void NotoSqlQuery::bindParam(int pos, const QVariant &val, QSql::ParamType type){
   bindValue( pos, val, type );
}
