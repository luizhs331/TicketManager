INSERT INTO bilhetes_entrada_saida(
    ds_nome_aluno,
    ds_nome_escola,
    tp_bilhete,
    ds_descricao_bilhete,
    ds_assinatura_pedagogo,
    dt_data_emissao,
    dh_hora_emissao,
    ds_motivo,
    ds_turma,
    ds_turno_aluno
)VALUES( 
    :NOMEALUNO,
    :NOMEESCOLA,
    :TPBILHETE,
    :DESCRICAOBILHETE,
    :ASSINATURAPEDAGOGO,
    :DATAEMISSAO,
    :HORAEMISSAO,
    :MOTIVO,
    :TURMA,
    :TURNOALUNO
) 

