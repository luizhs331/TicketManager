SELECT
 idsala AS ID_SALA,
 nomesala AS NOME_SALA,
 capacidade AS CAPACIDADE,
 numeroandar AS NR_ANDAR,
 numerosala AS NR_SALA,
 complemento AS COMPLEMENTO
FROM sala
WHERE nomesala
LIKE :NOME_SALA;
