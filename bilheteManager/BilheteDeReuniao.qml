import QtQuick 2.0
import QtQuick.Controls 2.0

Rectangle {
    anchors.fill: parent
    color: "#f1f1f1"

    Rectangle {
        width: parent.width / 2.8
        height: parent.height / 1.1
        color: "#f1f1f1"
        radius: 10
        border.color: "#d3d3d3"
        anchors.centerIn: parent
    }
}
