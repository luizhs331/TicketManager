import QtQuick 2.0
import QtQuick.Controls 2.0
import br.com.configgeral 1.0

import "qml/components"

Item {
    id: configuracoesTurma

    ConfigGeral {
        id: configGeral
    }

    property var turmas: configGeral.comboBox

    signal buttonSavePressed
    signal processoConcluidoStatus(var result)

    Rectangle {
        id: segundoRetangulo
        width: configuracoesTurma.width
        height: configuracoesTurma.height
        anchors.centerIn: configuracoesTurma
        color: colorBackground

        Column {
            id: columm
            anchors.centerIn: segundoRetangulo
            width: configuracoesTurma.width - (configuracoesTurma.width / 10)
            height: configuracoesTurma.height - (configuracoesTurma.width / 10)
            spacing: 20

            Label {
                font.pointSize: 18
                color: "#000"
                text: "Configurações das turmas"
            }

            TextFieldWithText {
                id: txtNomeEscola
                width: columm.width / 2
                height: configuracoesTurma.height / 11
                titleField: "Adicionar turma"
                isMandatoryField: false
                textSize: 13
            }

            Column {
                spacing: 5

                Label {
                    font.pointSize: 13
                    color: "#6c6c80"
                    text: "Deletar turma"
                }

                NotoComboBox {
                    modelTxt: turmas
                    textEditComboBox: "Turma:"
                    width: columm.width / 2
                    height: configuracoesTurma.height / 20
                    tamanhoQuadradoDropBox: 45
                    enabled: !excluirTurmaCheckBox.checked
                    onEmitirText: function (texto) {
                        primeraTab.retornoTextComboBox = texto
                    }
                }
            }
        }
    }
}
