#include "bilheteentradaousaidadto.h"

#include <QDebug>

BilheteEntradaOuSaidaDto::BilheteEntradaOuSaidaDto() :
    _idBilhete( 0 ),
    _descriprion( "" ),
    _motivo( "" ),
    _assinatura( "" ),
    _nome( "" ),
    _data( "" ),
    _hora( "" ),
    _nomeEscola( "" ),
    _tipoBilhete( "" ),
    _turma( "" ),
    _turno( "" )
{}

BilheteEntradaOuSaidaDto::~BilheteEntradaOuSaidaDto() {
    qInfo() << "Objeto Destruido";
}

QString BilheteEntradaOuSaidaDto::turno() const {
    return _turno;
}

void BilheteEntradaOuSaidaDto::setTurno( const QString& turno ) {
    _turno = turno;
}

QString BilheteEntradaOuSaidaDto::turma() const {
    return _turma;
}

void BilheteEntradaOuSaidaDto::setTurma( const QString& turma ) {
    _turma = turma;
}

QString BilheteEntradaOuSaidaDto::tipoBilhete() const {
    return _tipoBilhete;
}

void BilheteEntradaOuSaidaDto::setTipoBilhete( const QString& tipoBilhete ) {
    _tipoBilhete = tipoBilhete;
}

QString BilheteEntradaOuSaidaDto::nomeEscola() const {
    return _nomeEscola;
}

void BilheteEntradaOuSaidaDto::setNomeEscola( const QString& nomeEscola ) {
    _nomeEscola = nomeEscola;
}

QString BilheteEntradaOuSaidaDto::hora() const {
    return _hora;
}

void BilheteEntradaOuSaidaDto::setHora( const QString& hora ) {
    _hora = hora;
}

QString BilheteEntradaOuSaidaDto::data() const {
    return _data;
}

void BilheteEntradaOuSaidaDto::setData( const QString& data ) {
    _data = data;
}

QString BilheteEntradaOuSaidaDto::nome() const {
    return _nome;
}

void BilheteEntradaOuSaidaDto::setNome( const QString& nome ) {
    _nome = nome;
}

QString BilheteEntradaOuSaidaDto::assinatura() const {
    return _assinatura;
}

void BilheteEntradaOuSaidaDto::setAssinatura( const QString& assinatura ) {
    _assinatura = assinatura;
}

QString BilheteEntradaOuSaidaDto::motivo() const {
    return _motivo;
}

void BilheteEntradaOuSaidaDto::setMotivo( const QString& motivo ) {
    _motivo = motivo;
}

QString BilheteEntradaOuSaidaDto::descricao() const {
    return _descriprion;
}

void BilheteEntradaOuSaidaDto::setDescricao( const QString& descriprion ) {
    _descriprion = descriprion;
}

int BilheteEntradaOuSaidaDto::idBilhete() const {
    return _idBilhete;
}

void BilheteEntradaOuSaidaDto::setIdBilhete( int idBilhete ) {
    _idBilhete = idBilhete;
}


