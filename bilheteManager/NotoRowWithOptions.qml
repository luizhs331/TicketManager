import QtQuick 2.0
import QtQuick.Controls 2.0

import "qml/components"

Rectangle {
    id: retanguloMain
    width: 250
    color: "#393939"
    anchors.left: parent
    border.color: "#393939"

    property string title: ""
    property QtObject modelCustom
    property bool isButtonVisible: false

    signal optionsSelected(var option, var attributes, var hasAttributes, var hasParent)

    signal buttonPressed

    ButtonCriado {
        id: btnVoltar
        widthButton: 75
        heightButton: 50
        visible: isButtonVisible
        tpButtonTransparent: "N"
        colorBorder: "transparent"
        colorButtonPressed: "transparent"
        colorBorderPressed: "transparent"

        Image {
            id: imgV
            opacity: 1
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: 30
            height: 30
            source: "img/arrow.png"
        }

        onClicked: {
            buttonPressed()
        }
    }

    Column {
        anchors.centerIn: parent
        height: retanguloMain.height / 1.1
        width: retanguloMain.width

        Flickable {
            id: flickable
            width: parent.width
            height: parent.height / 1.1
            contentHeight: todoListMenuEsqueda.height
            clip: true

            Column {
                id: todoListMenuEsqueda
                width: parent.width
                spacing: 10

                Label {
                    id: labelOpcoes
                    text: "Opções"
                    font.family: "Helvetica"
                    font.pointSize: 15
                    color: "#fff"
                    opacity: 1
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Repeater {
                    model: modelCustom

                    Column {

                        Row {
                            width: flickable.width
                            anchors.horizontalCenter: retanguloMain.horizontalCenter
                            height: imgConfig.height * 2
                            x: 50
                            spacing: 5

                            Rectangle {
                                width: 30
                                height: 30
                                radius: 360
                                color: "#fff"
                                anchors.verticalCenter: parent.verticalCenter

                                Image {
                                    id: imgConfig
                                    anchors.centerIn: parent
                                    width: 15
                                    height: 15
                                    source: img
                                }
                            }

                            Button {
                                id: buttonConfig
                                width: textConfig.width
                                height: textConfig.height
                                anchors.verticalCenter: parent.verticalCenter
                                background: Rectangle {
                                    color: "transparent"
                                }
                                Label {
                                    id: textConfig
                                    text: name
                                    font.family: "Helvetica"
                                    font.pointSize: 13
                                    color: "#fff"
                                    opacity: mouseAreaConfig.containsMouse ? 1 : 0.6
                                }
                                MouseArea {
                                    id: mouseAreaConfig
                                    hoverEnabled: true
                                    anchors.fill: parent
                                    onClicked: {
                                        optionsSelected(enumType, attributes,
                                                        hasAttributes,
                                                        hasParent)
                                        //                                    validarButtonClicked(enumType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // TODO vai se alterado em breve , feito assim so para teste
    }
}
