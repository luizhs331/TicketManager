import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12

Rectangle {
    id: columnNotification
    width: parent.width
    height: parent.height
    radius: 3

    property var linkImg: ""
    property var textEdit: ""
    property var valueEdit: ""
    property var colorText: "#fff"

    property int fointSize: 13
    property bool showButtons: false

    signal buttonSeeDetailsClicked
    signal buttonIgnorClicked

    Column {
        width: parent.width
        height: parent.height

        Rectangle {
            visible: showButtons
            width: parent.width
            height: parent.height / 3
            anchors.bottom: parent.bottom
            color: "transparent"

            Rectangle {
                width: parent.width / 1.05
                height: parent.height / 1.1
                color: "transparent"
                anchors.horizontalCenter: parent.horizontalCenter

                Row {
                    width: parent.width
                    height: parent.height
                    spacing: 3

                    Button {
                        id: buttonSeeDetails
                        width: parent.width / 3
                        height: parent.height / 1.2
                        anchors.verticalCenter: parent.verticalCenter

                        Label {
                            text: "Ver Detalhes"
                            color: "#fff"
                            font.pointSize: 6
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                        }

                        background: Rectangle {
                            color: mouseAreaButtonTentarNovamente.containsMouse ? "#87ceea" : "#87b2ea"
                            border.color: "#87ceea"
                            border.width: 1
                            radius: 2
                        }

                        MouseArea {
                            id: mouseAreaButtonTentarNovamente
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: columnNotification.buttonSeeDetailsClicked()
                        }
                    }
                    Button {
                        width: parent.width / 3
                        height: parent.height / 1.2
                        anchors.verticalCenter: parent.verticalCenter

                        Label {
                            text: "Ignorar"
                            color: "#fff"
                            font.pointSize: 6
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                        }

                        background: Rectangle {
                            color: mouseAreaButtonIgnorar.containsMouse ? "#454652" : "#474a51"
                            border.color: "#474a51"
                            border.width: 1
                            radius: 2
                        }

                        MouseArea {
                            id: mouseAreaButtonIgnorar
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: columnNotification.buttonIgnorClicked()
                        }
                    }
                }
            }
        }

        Rectangle {
            width: parent.width / 1.1
            height: parent.height / 1.5
            anchors.centerIn: parent
            color: "transparent"

            Rectangle {
                id: retanguloImg
                width: parent.width / 1.2
                height: parent.width / 1.5
                anchors.top: parent.top
                color: "transparent"

                Row {
                    width: parent.width
                    spacing: 25

                    Image {
                        id: img
                        width: retanguloImg.width / 5
                        height: retanguloImg.height / 4.3
                        opacity: 1
                        source: linkImg
                    }
                    Column {
                        width: parent.width
                        height: parent.height / 1.6

                        Label {
                            id: textLabelBilhetes
                            text: textEdit
                            font.family: "Helvetica"
                            font.pointSize: fointSize
                            color: colorText
                            opacity: 1
                        }

                        Label {
                            id: textLabelValue
                            text: valueEdit
                            font.family: "Helvetica"
                            font.pointSize: fointSize
                            color: colorText
                            opacity: 1
                            anchors.top: textLabelBilhetes.bottom
                            anchors.left: textLabelBilhetes.left
                        }
                    }
                }
            }
        }
    }
}
