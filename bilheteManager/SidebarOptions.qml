import QtQuick 2.0
import QtQuick.Controls 2.0

import "qml/aluno/"
import "qml/sala/"

Rectangle {
    id: sideBarOptionsId

    property Item componentGeneric

    property int widthSiteLeft: retanguloEsquerda.width

    TurmasManager {
        id: turmaManager
        width: lobby.width
        height: lobby.height
        anchors.centerIn: lobby
        visible: false
    }

    SalaManager {
        id: salaManager
        width: lobby.width
        height: lobby.height
        anchors.centerIn: lobby
        visible: false
    }

    AlunoManager {
        id: alunoManager
        width: lobby.width
        height: lobby.height
        anchors.centerIn: lobby
        visible: false
    }

    NotoRowWithOptions {
        id: retanguloEsquerda
        width: 250
        height: sideBarOptionsId.height
        title: "Opcoes"
        modelCustom: nameModelListEsquerda

        onOptionsSelected: function (option, attributes, hasAttributes, hasParent) {

            validarButtonClicked(option)
            componentGeneric.openScreen(option)
            retanguloEsquerda.isButtonVisible = (hasAttributes || hasParent)

            if (hasAttributes) {
                modelCustom = attributes
            }
        }

        onButtonPressed: {
            modelCustom = nameModelListEsquerda
            retanguloEsquerda.isButtonVisible = false
        }
    }

    ListModel {
        id: nameModelListEsquerda
        ListElement {
            name: "Configurações"
            img: "img/img.png"
            enumType: "CONFIG"
            hasAttributes: false
            hasParent: false
            attributes: []
        }
        ListElement {
            name: "Teclas de atalho"
            img: "img/tecla-do-teclado.svg"
            enumType: "CONFIG_TECLAS"
            hasAttributes: false
            hasParent: false
            attributes: []
        }
        ListElement {
            name: "Excluir bilhete"
            img: "img/img8.png"
            enumType: "DELETE_TICKET"
            hasAttributes: false
            hasParent: false
            attributes: []
        }
        ListElement {
            name: "Editar bilhete"
            img: "img/editar.svg"
            enumType: "EDITAR_BILHETE"
            hasParent: false
            hasAttributes: false
            attributes: []
        }

        ListElement {
            name: "Turma"
            img: "img/editar.svg"
            enumType: "TURMA"
            hasAttributes: true
            hasParent: false
            attributes: [

                ListElement {
                    name: "Criar turma"
                    img: "img/editar.svg"
                    enumType: "CRIAR_TURMA"
                    hasAttributes: false
                    hasParent: true
                    attributes: []
                },
                ListElement {
                    name: "Editar turma"
                    img: "img/editar.svg"
                    enumType: "EDITAR_TURMA"
                    hasAttributes: false
                    hasParent: true
                    attributes: []
                }
            ]
        }

        ListElement {
            name: "Sala"
            img: "img/editar.svg"
            enumType: "SALA"
            hasAttributes: true
            hasParent: false
            attributes: [

                ListElement {
                    name: "Criar sala"
                    img: "img/editar.svg"
                    enumType: "CADASTRAR_SALA"
                    hasAttributes: false
                    hasParent: true
                    attributes: []
                },
                ListElement {
                    name: "Editar sala"
                    img: "img/editar.svg"
                    enumType: "UPDATE_SALA"
                    hasAttributes: true
                    hasParent: true
                    attributes: []
                }
            ]
        }
        ListElement {
            name: "Aluno"
            img: "img/editar.svg"
            enumType: "ALUNO"
            hasAttributes: true
            hasParent: false
            attributes: [

                ListElement {
                    name: "Cadastrar aluno"
                    img: "img/editar.svg"
                    enumType: "CADASTRAR_ALUNO"
                    hasAttributes: false
                    hasParent: true
                    attributes: []
                },
                ListElement {
                    name: "Editar aluno"
                    img: "img/editar.svg"
                    enumType: "UPDATE_ALUNO"
                    hasAttributes: true
                    hasParent: true
                    attributes: []
                }
            ]
        }
    }

    //TODO Analisar possibilidade de trocar esse case. por stackView, criando assim elementos para cada opcao
    function validarButtonClicked(enumType) {

        switch (enumType) {
        case "CONFIG":
            retanguloPrincipal.visible = false
            telaConfigGeral.visible = true
            break
        case "CONFIG_TECLAS":
            mainWindow.abrirHelTeclas()
            break
        case "DELETE_TICKET":
            mainWindow.abrirPopupExcluirBilhete()
            break
        case "EDITAR_BILHETE":
            mainWindow.abrirTelaUpdateBilhete()
            break
        case "TURMA":
            componentGeneric = turmaManager
            break
        case "SALA":
            componentGeneric = salaManager
            break
        case "ALUNO":
            componentGeneric = alunoManager
            break
        }
    }
}
