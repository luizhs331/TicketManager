import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12

import br.com.notificacao 1.0
import br.com.configgeral 1.0
import br.com.bilheteentradaousaida 1.0
import br.com.menuinicialcontroller 1.0

import "qml/components"

Rectangle {

    id: lobby
    width: parent.width
    height: cadastro.height
    anchors.centerIn: parent
    color: colorBackground
    visible: true

    // TODO tentei utilizar o enum mas nao obtive sucesso
    enum TypeOperation {
        CONFIG,
        CONFIG_TECLAS,
        DELETE_TICKET,
        EDITAR_BILHETE,
        TURMA_OPCOES
    }

    property string colorBackground: "#f1f1f1"

    signal emitSignalFundo
    signal buscarPorNome
    signal bilheteExcluido

    property string retornoNomeEscola: ""
    property int idUltimoBilhete

    onVisibleChanged: {
        retornoNomeEscola = configGeral.retornoNomeEscola()
        idUltimoBilhete = bilheteEntradaOuSaida.selectUltimoIdBilhete
        imgSino.visible = validarMostrarSino()
    }

    onBilheteExcluido: {
        idUltimoBilhete = bilheteEntradaOuSaida.selectUltimoIdBilhete
    }

    function validarMostrarSino() {
        if (notificacoes.validarHasNotificacoes() > 0) {
            return true
        }
        return false
    }

    function borderColorNotificacoes(tpEnum) {

        switch (tpEnum) {
        case 'V':
            return "#e8c94a"
        case "F":
            return "#f70000"
        case "S":
            return "#74b23d"
        }
    }

    function selectImageNotificacoes(tpEnum) {

        switch (tpEnum) {
        case 'V':
            return "img/warning.png"
        case "F":
            return "img/fail.png"
        case "S":
            return "img/img12.png"
        }
    }

    ConfigGeral {
        id: configGeral
    }

    BilheteEntradaOuSaida {
        id: bilheteEntradaOuSaida
    }

    NotificacaoController {
        id: notificacoes
    }

    MenuInicialController {
        id: menuInicialController
    }
    PoPupInformative {
        id: informationPopup
        width: lobby.width / 2.5
        height: lobby.height / 2.5
    }

    Drawer {
        id: drawer
        width: 300
        height: parent.height
        modal: false
        edge: Qt.RightEdge
        background: Rectangle {
            border.color: "transparent"
            color: "transparent"
        }

        Flickable {
            id: flickable
            width: parent.width
            height: parent.height
            contentHeight: todoList.height
            anchors.centerIn: parent
            clip: true

            Column {
                id: todoList
                width: parent.width
                spacing: 10

                Repeater {
                    id: repeater

                    onVisibleChanged: {
                        model = notificacoes.notificacoesDto
                    }

                    ColumnNotification {
                        width: lobby.width / 6
                        height: lobby.height / 10
                        anchors.horizontalCenter: parent.horizontalCenter

                        border.color: borderColorNotificacoes(
                                          modelData.tipoEnum)
                        fointSize: 8
                        color: "#fff"
                        colorText: "#474a51"
                        showButtons: modelData.tipoEnum === 'V' ? true : false

                        linkImg: selectImageNotificacoes(modelData.tipoEnum)
                        textEdit: modelData.tituloMensagem + " ID:" + modelData.idBilhete
                        valueEdit: modelData.dsMensagem

                        onButtonSeeDetailsClicked: function () {
                            informationPopup.idBilhete = modelData.idBilhete
                            informationPopup.open()
                        }

                        onButtonIgnorClicked: function () {
                            menuInicialController.deleteOneNotificationByIdBilhete(
                                        modelData.idBilhete)

                            repeater.model = notificacoes.notificacoesDto
                        }
                    }
                }
            }
        }
    }

    Row {
        width: lobby.width
        height: lobby.height

        SidebarOptions {
            id: sidebar
            width: 250
            height: lobby.height
        }

        Rectangle {
            id: mainRetanguloCenter
            width: lobby.width - (sidebar.width * 1.2)
            height: lobby.height + 30
            color: colorBackground

            Column {
                width: mainRetanguloCenter.width
                height: lobby.height / 1.05
                anchors.verticalCenter: parent.verticalCenter
                spacing: 20

                Rectangle {
                    width: rowDadosVisuais.width * 1.15
                    height: 50
                    color: "#393939"
                    x: 55
                    radius: 2

                    Label {
                        text: "Dados"
                        font.pointSize: 15
                        color: "#fff"
                        font.family: "Helvetica"
                        anchors.leftMargin: 20
                        anchors.topMargin: 10
                        anchors.fill: parent
                    }
                }

                Row {
                    id: rowDadosVisuais
                    spacing: 25
                    width: children.width
                    height: parent.height
                    x: 55

                    ColumnInformation {
                        width: lobby.width / 5
                        height: lobby.height / 5

                        // Icon By https://www.flaticon.com/br/autores/catkuro
                        linkImg: "img/museum-icon.png"
                        textEdit: "Nome Escola"
                        valueEdit: "Value:" + retornoNomeEscola
                        color: "#fff"
                        colorText: "#434d5b"
                    }

                    ColumnInformation {
                        width: lobby.width / 5
                        height: lobby.height / 5

                        // Icon By https://www.flaticon.com/br/autores/catkuro
                        linkImg: "img/alarm-clock.png"
                        textEdit: "Dias em operação"
                        valueEdit: "Value: 30"
                        color: "#fff"
                        colorText: "#434d5b"
                    }

                    ColumnInformation {
                        id: terste
                        width: lobby.width / 5
                        height: lobby.height / 5

                        linkImg: "img/ticket-icon.png"
                        textEdit: "Bilhetes Emitidos"
                        valueEdit: "Value:" + idUltimoBilhete
                        color: "#fff"
                        colorText: "#434d5b"
                    }
                }
            }

            Column {
                width: mainRetanguloCenter.width
                height: parent.height / 1.6
                anchors.bottom: parent.bottom
                spacing: 20

                Rectangle {
                    width: rowDadosVisuais.width * 1.15
                    height: 50
                    color: "#393939"
                    x: 55
                    radius: 2

                    Label {
                        text: "Navegacao"
                        font.pointSize: 15
                        color: "#fff"
                        font.family: "Helvetica"
                        anchors.leftMargin: 20
                        anchors.topMargin: 10
                        anchors.fill: parent
                    }
                }

                Row {
                    width: children.width
                    height: parent.height
                    x: 55
                    spacing: 25

                    Button {
                        id: btnNovoBilhete
                        width: mouseAreaNovoBilhete.pressed ? lobby.width / 4.5 : lobby.width / 5.0
                        height: mouseAreaNovoBilhete.pressed ? lobby.height
                                                               / 4.5 : lobby.height / 5.0
                        Label {
                            text: "Novo Bilhete"
                            font.family: "Helvetica"
                            font.bold: true
                            font.pointSize: mouseAreaNovoBilhete.pressed ? 20 : 15
                            color: mouseAreaNovoBilhete.containsMouse ? "#fff" : "#414143"
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            opacity: mouseAreaNovoBilhete.containsMouse ? 1 : 0.6
                        }
                        background: Rectangle {
                            radius: 3
                            visible: true
                            border.color: "#dfe0e1"
                            color: mouseAreaNovoBilhete.containsMouse ? "#11cdef" /*ou #0fdbff*/
                                                                      : "#fff"
                        }
                        MouseArea {
                            id: mouseAreaNovoBilhete
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                lobby.emitSignalFundo()
                            }
                        }
                    }

                    Button {
                        id: btnBuscas
                        width: mouseAreaBuscarBilhete.pressed ? lobby.width
                                                                / 4.5 : lobby.width / 5.0
                        height: mouseAreaBuscarBilhete.pressed ? lobby.height
                                                                 / 4.5 : lobby.height / 5.0
                        Label {
                            text: "Buscas"
                            font.family: "Helvetica"
                            font.bold: true
                            font.pointSize: mouseAreaBuscarBilhete.pressed ? 20 : 15
                            color: mouseAreaBuscarBilhete.containsMouse ? "#fff" : "#414143"
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            opacity: mouseAreaBuscarBilhete.containsMouse ? 0.8 : 0.6
                        }
                        background: Rectangle {
                            radius: 4
                            border.color: "#dfe0e1"
                            color: mouseAreaBuscarBilhete.containsMouse ? "#11cdef" : "#fff"
                        }
                        MouseArea {
                            id: mouseAreaBuscarBilhete
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                lobby.buscarPorNome()
                            }
                        }
                    }
                    Rectangle {
                        width: lobby.width / 5.0
                        height: lobby.height / 5.0
                        color: "transparent"
                    }
                }
            }
        }
    }

    Label {
        text: "Ticket Manager versao 0.33v"
        font.family: "Helvetica"
        font.pointSize: 10
        color: "#fff"
        opacity: 0.6
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: retanguloEsquerda.horizontalCenter
    }

    Button {
        id: buttonMensagem
        width: 50
        height: 50
        y: 29
        x: lobby.width - ((lobby.width * 0.1) / 2.3)
        background: Rectangle {
            radius: 5
            visible: true
            anchors.rightMargin: 30
            color: "transparent"
        }
        Image {
            id: imgMensagem
            anchors.centerIn: parent
            width: 50
            height: 50
            source: "img/mensagem.png"

            Image {
                id: imgSino
                width: 20
                height: 20
                source: "img/sino.png"
                x: 30
                y: 1
            }
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                drawer.open()
                notificacoes.updateNotificacao()
                imgSino.visible = validarMostrarSino()
            }
        }
    }
}
