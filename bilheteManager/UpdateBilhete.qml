import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12
import br.com.configgeral 1.0
import br.com.bilheteentradaousaida 1.0

import "qml/components"

Popup {
    id: popup
    anchors.centerIn: parent
    modal: true
    focus: true
    z: 0

    signal hasBilheteWithId

    property QtObject bilhete
    property var retornoTurmas: configGeral.comboBox

    property int turno: configGeral.convertOptionForIndex(
                            mycomboTurno.modelTxt, bilhete.turno)

    property int tipoBilhete: configGeral.convertOptionForIndex(
                                  bilheteTipoBilhete.modelTxt,
                                  bilhete.tipoBilhete)

    property int turma: configGeral.convertOptionForIndex(
                            mycomboTurma.modelTxt, bilhete.turma)

    onBilheteChanged: {
        textNomeAluno.currentText = bilhete.nome
        textMotivo.currentText = bilhete.motivo
        description.text = bilhete.description
        textAssinaturaPedagogo.currentText = bilhete.assinatura
    }

    background: Rectangle {
        width: parent.width
        height: parent.height
        radius: 10
    }

    onClosed: {
        rectangle.visible = false
        rectangleBuscarBilhete.visible = true
    }

    closePolicy: function () {
        return Popup.CloseOnEscape || Popup.CloseOnPressOutsideParent
    }

    BilheteEntradaOuSaida {
        id: bilheteEntradaOuSaida
    }

    ConfigGeral {
        id: configGeral
    }

    function updateInformationForSave() {
        bilhete.nome = textNomeAluno.currentText
        bilhete.motivo = textMotivo.currentText
        bilhete.assinatura = textAssinaturaPedagogo.currentText
        bilhete.description = description.text
    }

    function clearCurrentText() {
        textNomeAluno.currentText = ""
        textMotivo.currentText = ""
        textAssinaturaPedagogo.currentText = ""
        description.text = ""
    }

    Rectangle {
        id: rectangleBuscarBilhete
        visible: true
        width: popup.width / 1.1
        height: popup.height / 1.2
        color: "transparent"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Column {
            spacing: popup.height / 8.2

            Label {
                text: "Atualizar Bilhete"
                font.family: "Helvetica"
                font.pointSize: 30
                color: "#474a51"
                anchors.horizontalCenter: popup.horizontalCenter
                opacity: 0.8
            }

            TextFieldWithText {
                id: idBilhete
                width: rectangleBuscarBilhete.width / 1.1
                height: rectangleBuscarBilhete.height / 3.2
                hasFocusEnable: true
                titleField: "ID Bilhete"
                borderColorValidate: true
                customRegex: RegExpValidator {
                    regExp: /[0-9A-F]+/
                }
            }

            ButtonCriado {
                buttonName: "Confirmar"
                colorButtonPressed: "#afc0ff"
                colorBorderPressed: "#afc0ff"
                width: rectangleBuscarBilhete.width / 2.5
                height: rectangleBuscarBilhete.height / 4
                onClicked: {

                    if (bilheteEntradaOuSaida.hasBilheteWithId(
                                idBilhete.currentText)) {

                        popup.hasBilheteWithId()
                        rectangleBuscarBilhete.visible = false
                        rectangle.visible = true
                        idBilhete.borderColorValidate = true

                        bilhete = bilheteEntradaOuSaida.selectBilheteByid(
                                    idBilhete.currentText)
                    } else {
                        idBilhete.borderColorValidate = false
                        animationFailOrSucess.validaStatusAnimation = false
                        animationFailOrSucess.validarAnimacao()
                    }
                }
            }
        }
    }

    Rectangle {
        id: rectangle
        visible: false
        width: popup.width / 1.1
        height: popup.height / 1.1
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        color: "transparent"

        Column {
            id: column
            height: rectangle.height
            width: rectangle.width
            spacing: 20

            Label {
                text: "Atualizar Bilhete"
                font.family: "Helvetica"
                font.pointSize: 30
                color: "#474a51"
                opacity: 0.8
                anchors.horizontalCenter: popup.horizontalCenter
            }

            Row {
                width: popup.width
                spacing: 20

                NotoComboBox {
                    id: bilheteTipoBilhete
                    width: 450
                    height: 50
                    isTextConcat: true
                    modelTxt: ["ENTRADA", "SAIDA"]
                    textEditComboBox: "BILHETE DE:"
                    currentIndexBox: tipoBilhete
                    onEmitirText: function (texto) {
                        bilhete.tipoBilhete = texto
                    }
                }
                NotoComboBox {
                    id: mycomboTurno
                    width: 450
                    height: 50
                    isTextConcat: true
                    modelTxt: ["MANHA", "TARDE", "NOITE"]
                    textEditComboBox: "TURNO ALUNO:"
                    currentIndexBox: turno
                    onEmitirText: function (texto) {
                        bilhete.turno = texto
                    }
                }
            }

            Row {
                width: popup.width

                NotoComboBox {
                    id: mycomboTurma
                    width: 680
                    height: 50
                    modelTxt: retornoTurmas
                    isTextConcat: true
                    textEditComboBox: "TURMA ALUNO:"
                    currentIndexBox: turma
                    onEmitirText: function (texto) {
                        bilhete.turma = texto
                    }
                }
            }

            TextFieldWithText {
                id: textNomeAluno
                width: 2 * textMotivo.width + 15
                height: rectangle.height / 9
                titleField: "Nome Aluno"
                currentText: bilhete.nome
            }

            Column {
                spacing: 20
                anchors.horizontalCenter: parent.horizontalCenter

                Row {
                    id: rowMotivoAssinatura
                    spacing: 20
                    width: rectangle.width

                    TextFieldWithText {
                        id: textMotivo
                        width: rectangle.width / 2 - rowMotivoAssinatura.spacing / 2
                        height: rectangle.height / 9
                        titleField: "Motivo"
                        currentText: bilhete.motivo
                    }

                    TextFieldWithText {
                        id: textAssinaturaPedagogo
                        width: rectangle.width / 2 - rowMotivoAssinatura.spacing / 2
                        height: rectangle.height / 9
                        titleField: "Assinatura Pedagogo"
                        currentText: bilhete.assinatura
                    }
                }

                TextArea {
                    id: description
                    placeholderText: "Complemento"
                    text: bilhete.description
                    width: rectangle.width
                    height: rectangle.height / 5
                    wrapMode: "WordWrap"
                    background: Rectangle {
                        border.color: description.focus ? "#11cdef" : "#d3d3d3"
                        radius: 5
                    }
                }
            }

            ButtonCriado {
                buttonName: "Salvar"
                colorButtonPressed: "#afc0ff"
                colorBorderPressed: "#afc0ff"
                width: rectangle.width / 4
                height: rectangle.height / 9
                onClicked: {
                    popup.updateInformationForSave()
                    bilheteEntradaOuSaida.updateBilhete(bilhete)
                    animationFailOrSucess.validaStatusAnimation = true
                    animationFailOrSucess.validarAnimacao()
                }
            }
        }
    }

    NotoFailOrSucess {
        id: animationFailOrSucess
        width: 330
        height: 150
        anchors.right: parent.right
        color: "transparent"
        visible: false
        mensagemSuccess: "Sucesso ao alterar bilhete"
        mensagemErro: "Bilhete não encontrado"
        fromYinit: 105
        fromYdone: 125
    }
}
