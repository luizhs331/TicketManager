#ifndef CONFIGBANCOGERAL_H
#define CONFIGBANCOGERAL_H

#include "bilheteentradaousaidadto.h"

class ConfigBancoGeral {

public:
    static void dataBaseOpenConexion();
    static void dataBaseOpenConexionPSQL();
};

#endif // CONFIGBANCOGERAL_H
