import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.4

import "qml/components"

Rectangle {
    id: rectangleMain

    property alias titleField: field.titleField
    property alias model: listView.model
    property alias isMandatoryField: field.isMandatoryField
    property alias showTextErro: field.showTextErro
    property alias currentText: field.currentText
    property alias placeholderText: field.placeholderText
    property alias visibleField: field.visible

    property int idOptionSelected

    signal textChanged

    function isValid() {
        return showTextErro = !idOptionSelected > 0
    }

    Column {
        spacing: -20
        TextFieldWithText {
            id: field
            width: rectangleMain.width
            height: rectangleMain.height / 3.5
            isMandatoryField: false
            currentText: ""
            textSize: 10
            placeholderText: ""
            onFieldTextChange: textChanged()
            onFieldFocusChanged: {
                rectangleListView.visible = hasFocusEnable
            }
        }

        Rectangle {
            id: rectangleListView
            color: "#d3d3d3"
            radius: 3
            border.color: "#fff"
            width: rectangleMain.width
            height: (rectangleMain.height - (field.height - 20))
            visible: true

            ListView {
                id: listView
                width: rectangleMain.width
                height: rectangleListView.height
                spacing: 2

                clip: true
                anchors.horizontalCenter: parent.horizontalCenter

                delegate: Rectangle {
                    id: rectangleListViewButton
                    width: rectangleMain.width
                    height: listView.height
                    color: "transparent"

                    Column {

                        Rectangle {
                            color: "transparent"
                            width: rectangleMain.width
                            height: 3
                        }

                        Button {
                            id: buttonListView
                            width: rectangleListViewButton.width - 8
                            height: rectangleListViewButton.height / 4
                            anchors.horizontalCenter: parent.horizontalCenter

                            Rectangle {
                                width: buttonListView.width / 1.05
                                height: buttonListView.height
                                anchors.centerIn: parent
                                color: "transparent"

                                Label {
                                    font.pointSize: 16
                                    color: {
                                        mouseAreaButtonListView.containsMouse
                                                || selected ? "#fff" : "#a3a3a3"
                                    }
                                    anchors.verticalCenter: parent.verticalCenter
                                    font.family: "helvetica"
                                    text: description
                                }
                            }

                            background: Rectangle {
                                color: {
                                    mouseAreaButtonListView.containsMouse
                                            || selected ? "#87cefa" : "#fff"
                                }
                                radius: 3
                            }
                            MouseArea {
                                id: mouseAreaButtonListView
                                anchors.fill: buttonListView
                                hoverEnabled: true
                                onClicked: {
                                    //                                    field.currentText = description
                                    idOptionSelected = idOption
                                    if (selected) {
                                        selected = false
                                    } else {
                                        selected = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
