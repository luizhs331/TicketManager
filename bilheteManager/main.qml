import QtQuick 2.6
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12
import QtQuick.Dialogs 1.3

import QtQml 2.2

import "qml/components"

ApplicationWindow {
    id: mainWindow
    width: 1600
    height: 900
    visible: true
    visibility: validarFullScreen()

    title: "ManagerSchool"

    // TODO refatorar a class main e a menuinical, pois as regras estao misturadas e confusas

    //    menuBar: MenuBar {
    //        background: Rectangle {
    //            color: "#393939"
    //            border.color: "transparent"
    //        }
    //        Menu {
    //            title: "File"

    //            MenuItem {
    //                text: "Open..."
    //            }
    //            MenuItem {
    //                text: "Close"
    //            }
    //        }

    //        Menu {
    //            title: "Edit"
    //            MenuItem {
    //                text: "Cut"
    //            }
    //            MenuItem {
    //                text: "Copy"
    //            }
    //            MenuItem {
    //                text: "Paste"
    //            }
    //        }
    //    }
    property bool validarFullScreenWindow
    property bool hasUsuarioLogado: false

    function validarFullScreen() {
        if (!validarFullScreenWindow) {
            return "Windowed"
        } else {
            return "FullScreen"
        }
    }

    function abrirPopupExcluirBilhete() {
        deletarBilhete.visible = true
    }

    function abrirPoPupCloseAplicacao() {
        fecharAplicacao.visible = true
    }

    function abrirHelTeclas() {
        teclas.visible = true
    }

    function fecharTelaConfigGeral() {
        retanguloPrincipal.visible = true
        telaConfigGeral.visible = false
    }

    function abrirTelaAtalhoCadastro() {
        retanguloPrincipal.visible = false
        toolBar.visible = false
        menuInicial.visible = false
        stack.push(primeiroELemento)
    }

    function abrirTelaConfig() {
        retanguloPrincipal.visible = false
        telaConfigGeral.visible = true
    }

    function abrirTelaDeBusca() {
        retanguloPrincipal.visible = false
        telaDebusca.visible = true
    }

    function abrirTelaUpdateBilhete() {
        updateBilhete.visible = true
    }

    header: ToolBar {
        id: toolBar
        visible: false
        background: Rectangle {
            border.color: "#d3d3d3"
            color: "#fff"
        }

        RowLayout {
            anchors.fill: parent
            ToolButton {
                id: toolButton
                background: Rectangle {
                    height: toolBar.height
                    width: 150
                    anchors.verticalCenter: parent.verticalCenter
                    color: "#474a51"

                    Label {
                        text: "Tipo Bilhete"
                        font.family: "Helvetica"
                        font.pointSize: 10
                        color: "#fff"
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        opacity: 1
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: menu.open()
                    }
                }
            }
            Label {
                id: titulo
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }

            ToolButton {
                text: ">"

                onClicked: {
                    stack.pop()
                }
            }
        }
    }

    StackView {
        id: stack
        anchors.fill: parent
    }

    Menu {
        id: menu

        MenuItem {
            text: "Bilhete de entrada e saida"
            onClicked: {
                titulo.text = "BILHETE DE ENTRADA E SAIDA"
                stack.push(primeiroELemento)
            }
        }

        MenuItem {
            text: "Bilhete de expulsao"
            onClicked: {
                stack.push(segundoElemento)
                titulo.text = "BILHETE DE EXPULSAO"
            }
        }
        MenuItem {
            text: "Bilhete de reuniao"
            onClicked: {
                stack.push(terceroElemento)
                titulo.text = "BILHETE DE REUNIAO"
            }
        }
    }

    Component {
        id: primeiroELemento

        Rectangle {
            id: retangulo
            anchors.fill: parent

            Cadastro {
                id: cadastro
                width: mainWindow.width
                height: mainWindow.height

                onVoltarTela: function () {
                    toolBar.visible = false
                    retanguloPrincipal.visible = true
                    cadastro.visible = false
                    menuInicial.visible = true
                }
            }
        }
    }

    Component {
        id: segundoElemento
        Rectangle {
            anchors.fill: parent
            BilheteDeReuniao {}
        }
    }

    Component {
        id: terceroElemento
        Rectangle {
            anchors.fill: parent
            color: "#f1f1f1"
        }
    }

    TeclasDeAtalho {}

    TelaConfigGeral {
        id: telaConfigGeral
        visible: false
        width: parent.width
        height: parent.height

        onMudarResulucaoTela: function (bool) {
            validarFullScreenWindow = bool
            mainWindow.visibility = mainWindow.validarFullScreen()
        }

        onVoltarPagina: function () {
            retanguloPrincipal.visible = true
            telaConfigGeral.visible = false
        }
    }

    TelaDeBusca {
        id: telaDebusca
        visible: false
        width: parent.width
        height: parent.height

        onVoltarPagina: function () {
            retanguloPrincipal.visible = true
            telaDebusca.visible = false
            cadastro.visible = false
        }
    }

    UpdateBilhete {
        id: updateBilhete
        visible: false
        onOpened: {
            width = parent.width / 2.5
            height = parent.height / 2.5
        }
        onHasBilheteWithId: {
            width = parent.width / 1.3
            height = parent.height / 1.3
        }
    }

    PopupExcluirBilhete {
        id: deletarBilhete
        visible: false
        anchors.centerIn: parent
        width: parent.width / 2.5
        height: parent.height / 2.5

        onBilheteExcluido: menuInicial.bilheteExcluido()
    }

    PoPupFecharAplicacao {
        id: fecharAplicacao
        visible: false
        anchors.centerIn: parent
        width: parent.width / 4
        height: parent.height / 4
    }

    PoPupHelpTeclas {
        id: teclas
        width: parent.width / 2.6
        height: parent.height / 1.2
        visible: false
    }

    TelaLogin {
        width: parent.width
        height: parent.height
        visible: true
        id: telaLogin

        onLoginAprovado: function () {
            mainWindow.hasUsuarioLogado = telaLogin.usuarioValido
            telaLogin.visible = false
            retanguloPrincipal.visible = true
        }
    }

    Rectangle {
        id: retanguloPrincipal
        anchors.fill: parent
        anchors.margins: 15
        visible: false
        color: "#fff"

        MenuInicial {
            width: mainWindow.width
            height: mainWindow.height
            visible: true
            id: menuInicial

            onEmitSignalFundo: function () {
                retanguloPrincipal.visible = false
                toolBar.visible = true
                menuInicial.visible = false
                stack.push(primeiroELemento)
            }

            onBuscarPorNome: function () {
                retanguloPrincipal.visible = false
                telaDebusca.visible = true
            }
        }
    }
}
