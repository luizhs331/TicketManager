import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12

import br.com.bilheteentradaousaida 1.0

PoPupGenericoPesquisa {
    id: excluirBilhete
    visible: false
    anchors.centerIn: parent
    width: parent.width
    height: parent.height

    titlePoPup: "Excluir Bilhete"
    buttonName: "Confirmar"
    customRegexPoPup: RegExpValidator {
        regExp: /[0-9A-F]+/
    }

    signal bilheteExcluido
    signal excluirBilhetePorId(var idBilhete)

    BilheteEntradaOuSaida {
        id: bilheteEntradaOuSaida
    }

    onClosed: {
        excluirBilhete.clearCurrentTextField()
    }

    onButtonPressed: {
        if (currentTextTextFild.length > 0) {
            excluirBilhete.excluirBilhetePorId(currentTextTextFild)
        }
    }

    onExcluirBilhetePorId: function (idBilhete) {
        if (bilheteEntradaOuSaida.hasBilheteWithId(idBilhete)) {
            excluirBilhete.borderColorValidateSignal(true)
            processoExcluirBilhete.visible = true
        } else {
            excluirBilhete.borderColorValidateSignal(false)
            excluirBilhete.validaApressentarMensagemDeErro()
        }
    }

    function validaApressentarMensagemDeSucesso() {
        animationFailOrSucess.validaStatusAnimation = true
        animationFailOrSucess.validarAnimacao()
    }

    function validaApressentarMensagemDeErro() {
        animationFailOrSucess.validaStatusAnimation = false
        animationFailOrSucess.validarAnimacao()
    }

    PoPupProcesso {
        id: processoExcluirBilhete
        width: parent.width * 1.1
        height: parent.height * 1.3
        descricaoProcesso: "Excluir Bilhete"
        idBilhete: currentTextTextFild
        visible: false

        onProcessAccepted: {
            if (bilheteEntradaOuSaida.excluirBilhetePorId(idBilhete)) {
                excluirBilhete.bilheteExcluido()
                excluirBilhete.validaApressentarMensagemDeSucesso()
                excluirBilhete.clearCurrentTextField()
            }
        }
    }

    NotoFailOrSucess {
        id: animationFailOrSucess
        width: 330
        height: 150
        anchors.right: parent.right
        color: "transparent"
        visible: false
        mensagemErro: "Bilhete não encontrado"
        mensagemSuccess: "Sucesso ao deletar bilhete"
        fromYinit: 105
        fromYdone: 125
        validaStatusAnimation: false
    }
}
