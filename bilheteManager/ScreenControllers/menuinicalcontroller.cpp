#include "menuinicalcontroller.h"

#include <notificacao/notificacoescontroller.h>

MenuInicalController::MenuInicalController( QObject* parent ) :
    QObject( parent ) {}

void MenuInicalController::deleteOneNotificationByIdBilhete( QString idBilhete ) const {
    NotificacoesController().deleteOneNotificationByIdBilhete( idBilhete );
}

void MenuInicalController::deleteAllNotifications() const {
    NotificacoesController().deleteAllNotifications();
}
