#ifndef MENUINICALCONTROLLER_H
#define MENUINICALCONTROLLER_H

#include <QObject>

class MenuInicalController : public QObject {
    Q_OBJECT
public:
    explicit MenuInicalController( QObject* parent = nullptr );

public slots:
    void deleteOneNotificationByIdBilhete( QString idBilhete ) const;
    void deleteAllNotifications() const;

};

#endif // MENUINICALCONTROLLER_H
