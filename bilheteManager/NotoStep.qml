import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.0

Rectangle {
    id: windowMain
    color: "transparent"

    property alias model: repeater.model

    Row {
        id: row
        width: recPai.width * (listModel.count - 1)
        height: windowMain.height
        anchors.verticalCenter: windowMain.verticalCenter
        anchors.horizontalCenter: windowMain.horizontalCenter
        spacing: 0

        Repeater {
            id: repeater

            //            model: listModel
            Rectangle {
                id: recPai
                width: !((repeater.count - 1)
                         !== index) ? circle.width : (windowMain.width / (repeater.count))
                height: 100
                anchors.verticalCenter: parent.verticalCenter

                Rectangle {
                    width: recPai.width
                    height: 5
                    color: modelData.done ? "#5ea8ca" : "#eaeaea"
                    anchors.verticalCenter: parent.verticalCenter
                    visible: (repeater.count - 1) != index
                }

                Rectangle {
                    id: circle
                    width: 50
                    height: 50
                    color: modelData.done ? "#5ea8ca" : "#fff"
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    radius: 360
                    border.color: {

                        modelData.selected ? "#5ea8ca" : (modelData.done ? "transparent" : "#eaeaea")
                    }

                    Image {
                        id: img
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        width: 30
                        height: 30
                        source: modelData.done ? "img/check.svg" : modelData.imgLink
                        visible: modelData.done || modelData.selected
                    }
                }

                Label {
                    anchors.topMargin: 7
                    text: modelData.title
                    color: "#000"
                    anchors.top: circle.bottom
                    anchors.horizontalCenter: circle.horizontalCenter
                }
            }
        }
    }
}
