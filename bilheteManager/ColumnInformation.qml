import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12

Rectangle {
    id: mainRectangle
    width: parent.width
    height: parent.height
    border.color: "#fff" // color #11cdef
    radius: 3

    property var linkImg: ""
    property var textEdit: ""
    property var valueEdit: ""
    property var colorText: "#afc0ff"

    property int fontSizeEdit: 14

    //    FontLoader {
    //        id: fixedFont
    //        name: "Calibri"
    //    }
    RectangularGlow {
        id: effect1
        anchors.fill: rectangleSecundary
        glowRadius: 1
        cornerRadius: 1
        spread: 100
        color: "#d3d3d3"
    }

    Rectangle {
        id: rectangleSecundary
        width: mainRectangle.width
        height: mainRectangle.height

        Column {
            id: column
            width: parent.width
            height: parent.height / 2
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            Rectangle {
                id: retanguloImg
                width: parent.width / 4.5
                height: parent.width / 4.5
                color: "transparent"
                anchors.horizontalCenter: parent.horizontalCenter

                Image {
                    id: img
                    width: retanguloImg.width / 1.2
                    height: retanguloImg.height / 1.2
                    opacity: 1
                    source: linkImg
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }

            Column {
                width: parent.width
                height: parent.height / 1.6

                Label {
                    id: textLabelBilhetes
                    text: textEdit
                    font.family: "Helvetica"
                    font.pointSize: fontSizeEdit
                    color: "#afc0ff"
                    opacity: 1
                    x: 65
                    y: 1
                }

                Label {
                    id: textLabelValue
                    text: valueEdit
                    font.family: "Helvetica"
                    font.pointSize: fontSizeEdit
                    color: colorText
                    opacity: 1
                    anchors.top: textLabelBilhetes.bottom
                    anchors.left: textLabelBilhetes.left
                }
            }
        }
    }
}
