#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QQuickView>
#include <QQmlContext>

#include "bilheteformatar.h"

#include <bilheteentradaousaida/bilheteentradaousaidacontroller.h>
#include <login/logincontroller.h>
#include <processo/processocontroller.h>
#include <notificacao/notificacoescontroller.h>
#include <escola/escolacontroller.h>
#include <ScreenControllers/menuinicalcontroller.h>
#include <turma/turmacontroller.h>
#include <configcaixageral/configcaixageralcontroller.h>
#include <sala/saladto.h>
#include <sala/salacontroller.h>
#include <turma/cadastroturma/cadastroturmamanager.h>

#include <bilheteentradaousaidadto.h>
#include <turma/turmadto.h>

#include <escola/escolamodel.h>


#include <sqls.h>

int main( int argc, char* argv[] ) {
    QQuickStyle::setStyle( "Default" );

    QGuiApplication app( argc, argv );

    QCoreApplication::setOrganizationName( "ticketManager" );
    QCoreApplication::setOrganizationDomain( "ticketManager.com" );
    QCoreApplication::setApplicationName( "Star Runner" );
    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );

    qmlRegisterType<ConfigCaixaGeralController>( "br.com.configgeral", 1, 0, "ConfigGeral" );
    qmlRegisterType<BilheteEntradaOuSaidaController>( "br.com.bilheteentradaousaida", 1, 0, "BilheteEntradaOuSaida" );
    qmlRegisterType<LoginController>( "br.com.logincontroller", 1, 0, "LoginController" );
    qmlRegisterType<ProcessoController>( "br.com.processocontroller", 1, 0, "ProcessoController" );
    qmlRegisterType<NotificacoesController>( "br.com.notificacao", 1, 0, "NotificacaoController" );
    qmlRegisterType<EscolaController>( "br.com.escolacontroller", 1, 0, "EscolaController" );
    qmlRegisterType<MenuInicalController>( "br.com.menuinicialcontroller", 1, 0, "MenuInicialController" );
    qmlRegisterType<TurmaController>( "br.com.turmacontroller", 1, 0, "TurmaController" );
    qmlRegisterType<SalaController>( "br.com.salacontroller", 1, 0, "SalaController" );

    // Managers

    qmlRegisterType<CadastroTurmaManager>( "br.com.cadastroturmamanager", 1, 0, "CadastroTurmaManager" );


    // Dtos / Models

    qmlRegisterType<EscolaModel>( "br.com.escolamodel", 1, 0, "EscolaModel" );
    qmlRegisterType<TurmaDto>( "br.com.turmadto", 1, 0, "TurmaDto" );
    qmlRegisterType<SalaDto>( "br.com.saladto", 1, 0, "SalaDto" );
    qmlRegisterType<BilheteEntradaOuSaidaDto>( "br.com.bilheteEntradaousaidadto", 1, 0, "BilheteEntradaOuSaidaDto" );

    // Analisar possibilidade de assim que possivel utilizar o qmake para poder incluir os pacotes no .pro

    new Sqls;

    QQmlApplicationEngine engine;

//    EscolaModel escolaModel;
//    engine.rootContext()->setContextProperty( "escolaModel", &escolaModel );

    const QUrl url( QStringLiteral( "qrc:/main.qml" ) );
    QObject::connect( &engine, &QQmlApplicationEngine::objectCreated,
                      &app, [url]( QObject* obj, const QUrl& objUrl ) {
        if ( !obj && url == objUrl )
            QCoreApplication::exit( -1 );
    }, Qt::QueuedConnection );
    engine.load( url );

    return app.exec();
}
//Default Style
//Fusion Style
//Imagine Style
//Material Style
//Universal Style
