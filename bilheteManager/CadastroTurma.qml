import QtQuick 2.0
import QtQuick.Controls 2.0

import br.com.turmadto 1.0
import br.com.turmacontroller 1.0

import br.com.cadastroturmamanager 1.0
import br.com.salacontroller 1.0

import "qml/components"

Popup {
    id: popup
    focus: true
    modal: true
    anchors.centerIn: parent

    closePolicy: function () {
        return Popup.CloseOnEscape || Popup.CloseOnPressOutsideParent
    }

    property QtObject listSalas
    property int step: CadastroTurmaManager.DADOS_BASICOS

    background: Rectangle {
        border.color: "#fff"
        radius: 5
    }

    CadastroTurmaManager {
        id: turmaManager
    }

    TurmaController {
        id: turmaController
    }

    SalaController {
        id: salaController
    }

    onVisibleChanged: {
        listObject = salaController.findAllBasicSalas()
    }

    function validFildsDadosBasicos() {

        var isValid = true

        isValid &= nomeTurma.notIsEmpty()
        isValid &= dtFinal.notIsEmpty()
        isValid &= dtInicio.notIsEmpty()

        isValid &= sala.isValid()
        return isValid
    }

    function advanceStep() {

        switch (step) {
        case CadastroTurmaManager.DADOS_BASICOS:
            if (validFildsDadosBasicos()) {
                step = CadastroTurmaManager.ALUNOS
                turmaManager.changeStep(step)
                recDadosBasicos.visible = false
                recAlunos.visible = true
                btnVoltar.visible = true
            }
            break
        case CadastroTurmaManager.ALUNOS:
            step = CadastroTurmaManager.CONFIRMACAO
            turmaManager.changeStep(CadastroTurmaManager.CONFIRMACAO)
            recAlunos.visible = false
            recConfirmacao.visible = true
            break
        case CadastroTurmaManager.CONFIRMACAO:
            turmaController.insertTurma(turmaDto)
        }
    }

    function backStep() {

        switch (step) {
        case CadastroTurmaManager.DADOS_BASICOS:
            break
        case CadastroTurmaManager.ALUNOS:
            step = CadastroTurmaManager.DADOS_BASICOS
            turmaManager.changeStep(step)
            recDadosBasicos.visible = true
            recAlunos.visible = false
            btnVoltar.visible = false
            break
        case CadastroTurmaManager.CONFIRMACAO:
            step = CadastroTurmaManager.ALUNOS
            turmaManager.changeStep(CadastroTurmaManager.ALUNOS)
            recConfirmacao.visible = false
            recAlunos.visible = true
            break
        }
    }

    TurmaDto {
        id: turmaDto
        nomeTurma: nomeTurma.currentText
        idSala: sala.idOptionSelected
        dtFinal: dtFinal.currentText
        dtInicio: dtInicio.currentText /*Date.fromLocaleString(Qt.locale(),                                                                                                                                                                                                                                                                                                                                                                                                           "Tue 2013-09-17 10:56:06",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             "ddd yyyy-MM-dd hh:mm:ss")*/
    }

    Column {
        id: column
        spacing: 40
        anchors.horizontalCenter: parent.horizontalCenter

        Column {
            spacing: 25
            Label {
                text: "Criar turma"
                font.family: "Helvetica"
                font.pointSize: 30
                color: "#474a51"
                opacity: 0.8
                anchors.horizontalCenter: parent.horizontalCenter
            }

            NotoStep {
                width: popup.width
                height: popup.height / 7
                anchors.horizontalCenter: column.horizontalCenter
                model: turmaManager.steps()
            }
        }

        Rectangle {
            id: recSecundary
            width: popup.width / 1.2
            height: popup.height / 2.4
            anchors.horizontalCenter: parent.horizontalCenter

            Rectangle {
                id: recDadosBasicos
                width: recSecundary.width
                height: recSecundary.height
                Column {
                    width: recSecundary.width
                    height: recSecundary.height
                    anchors.centerIn: popup
                    spacing: 50

                    Column {
                        spacing: 40
                        anchors.verticalCenter: parent.verticalCenter

                        Row {
                            id: rowInformationsPrimary
                            spacing: 20
                            z: 1

                            TextFieldWithText {
                                id: nomeTurma
                                width: (recSecundary.width / 2)
                                       - (rowInformationsPrimary.spacing / 2)
                                height: recSecundary.height / 3.5
                                titleField: "Nome turma"
                                borderColorValidate: true
                                isMandatoryField: true
                            }

                            NotoTextFieldWithSearch {
                                id: sala
                                width: (recSecundary.width / 2)
                                       - (rowInformationsPrimary.spacing / 2)
                                height: recSecundary.height / 3.5
                                isMandatoryField: true
                                onTextChanged: {
                                    sala.model = salaController.findBasicSalasByName(
                                                sala.currentText)
                                }

                                titleField: "Sala"

                                Keys.onReturnPressed: {
                                    fieldSearch.visible = false
                                }
                            }
                        }

                        Row {
                            id: rowInformationSecundary
                            spacing: 20
                            z: 0

                            TextFieldWithText {
                                id: dtInicio
                                width: (recSecundary.width / 2)
                                       - (rowInformationSecundary.spacing / 2)
                                height: recSecundary.height / 3.5
                                titleField: "Data inicial"
                                borderColorValidate: true
                                customRegex: RegExpValidator {
                                    regExp: /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/
                                }
                            }

                            TextFieldWithText {
                                id: dtFinal
                                width: (recSecundary.width / 2)
                                       - (rowInformationSecundary.spacing / 2)
                                height: recSecundary.height / 3.5
                                titleField: "Data final"
                                borderColorValidate: true
                                customRegex: RegExpValidator {
                                    regExp: /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/
                                }
                            }
                        }
                    }
                }
            }

            Rectangle {
                id: recAlunos
                width: recSecundary.width
                height: recSecundary.height
                visible: false
                color: "#000"

                ListModel {
                    id: modelA

                    ListElement {
                        idOption: "1"
                        description: "asdas"
                        selected: true
                    }
                }

                NotoListingWithSearch {
                    width: recAlunos.width
                    height: recAlunos.height
                    model: modelA
                    anchors.horizontalCenter: parent.horizontalCenter
                    placeholderText: "Buscar por Nome/CPF/CNPJ"
                }
            }

            Rectangle {
                id: recConfirmacao
                width: recSecundary.width
                height: recSecundary.height
                visible: false

                ListModel {
                    id: modelB

                    ListElement {
                        idOption: "1"
                        description: "asdas"
                        selected: true
                    }
                }

                Column {
                    width: recAlunos.width
                    height: recAlunos.height
                    spacing: 10

                    Row {
                        width: recAlunos.width
                        spacing: (recAlunos.width / 2) - rowNomeTurma.width

                        // TODO mudar o nome do NotoRowLayout para NotoPairLabel;
                        // TODO trocar essa rows pelo component NotoPairLabel
                        Row {
                            id: rowNomeTurma
                            width: children.width
                            Text {
                                text: "Nome turma: "
                                font.family: "Helvetica"
                                font.pointSize: 15
                                color: "#474a51"
                                opacity: 0.9
                                font.bold: true
                            }
                            Text {
                                text: nomeTurma.currentText
                                font.family: "Helvetica"
                                font.pointSize: 15
                                color: "#474a51"
                                opacity: 1
                            }
                        }

                        Row {

                            Text {
                                text: "Sala: "
                                font.family: "Helvetica"
                                font.pointSize: 15
                                color: "#474a51"
                                opacity: 0.9
                                font.bold: true
                            }

                            Text {
                                text: sala.currentText
                                font.family: "Helvetica"
                                font.pointSize: 15
                                color: "#474a51"
                                opacity: 1
                            }
                        }
                    }
                    Row {
                        width: recAlunos.width
                        spacing: (recAlunos.width / 2) - rowDataInicial.width

                        Row {
                            id: rowDataInicial
                            width: children.width
                            Text {
                                text: "Data Inicial: "
                                font.family: "Helvetica"
                                font.pointSize: 15
                                color: "#474a51"
                                opacity: 0.9
                                font.bold: true
                            }
                            Text {
                                text: dtInicio.currentText
                                font.family: "Helvetica"
                                font.pointSize: 15
                                color: "#474a51"
                                opacity: 1
                            }
                        }

                        Row {

                            Text {
                                text: "Data Final: "
                                font.family: "Helvetica"
                                font.pointSize: 15
                                color: "#474a51"
                                opacity: 0.9
                                font.bold: true
                            }
                            Text {
                                text: dtFinal.currentText
                                font.family: "Helvetica"
                                font.pointSize: 15
                                color: "#474a51"
                                opacity: 1
                            }
                        }
                    }

                    NotoListingWithSearch {
                        width: recAlunos.width
                        height: recAlunos.height
                        model: modelA
                        anchors.horizontalCenter: parent.horizontalCenter
                        visibleField: false
                    }
                }
            }
        }

        Rectangle {
            width: popup.width / 1.2
            height: recSecundary.height / 4
            anchors.horizontalCenter: parent.horizontalCenter

            Row {
                height: parent.height
                width: parent.width
                spacing: 10

                ButtonCriado {
                    id: btnConfirmarProcesso
                    width: recSecundary.width / 4
                    height: parent.height
                    buttonName: "Confirmar"
                    onClicked: {

                        advanceStep()
                    }
                }
                ButtonCriado {
                    id: btnVoltar
                    width: recSecundary.width / 4
                    height: parent.height
                    buttonName: "Voltar"
                    visible: false
                    onClicked: {
                        backStep()
                    }
                }
            }
        }
    }
}
