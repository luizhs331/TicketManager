import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12
import br.com.bilheteentradaousaida 1.0

import "qml/components"

Popup {
    id: popupInformative
    anchors.centerIn: parent
    modal: true
    focus: true
    z: 0

    property int idBilhete: 0

    closePolicy: function () {
        return Popup.CloseOnEscape || Popup.CloseOnPressOutsideParent
    }

    BilheteEntradaOuSaida {
        id: bilheteEntradaOuSaida
    }

    background: Rectangle {
        width: popupInformative.width
        height: popupInformative.height
        radius: 10
    }

    Rectangle {
        id: retangleInformation
        width: popupInformative.width / 1.1
        height: popupInformative.height / 1.4
        anchors.centerIn: parent
        color: "transparent"

        Column {
            width: retangleInformation.width
            height: retangleInformation.height
            anchors.left: parent.left
            spacing: 35

            Row {

                width: retangleInformation.width

                NotoRowLayout {
                    title: "Error"
                    texto: "404"
                    colorText: "#fbf546"
                    colorTitle: "#474a51"
                    pointSize: 30
                    isManualAlign: true
                    titlePreferredWidth: popupInformative.width / 6
                    textPreferredWidth: popupInformative.width / 6
                }

                NotoRowLayout {
                    id: rowIdBilhete
                    title: "ID Bilhete"
                    texto: idBilhete
                    colorText: "#fbf546"
                    colorTitle: "#474a51"
                    pointSize: 16
                    isManualAlign: true
                    titlePreferredWidth: popupInformative.width / 6
                    textPreferredWidth: popupInformative.width / 3
                    y: 15
                }
            }

            NotoRowLayout {
                titlePreferredWidth: 0
                textPreferredWidth: popupInformative.width - 10
                isManualAlign: true
                texto: "Error Description : "
                       + "Erro ocorre pois nao foi possivel enviar as informacoes para o servidor "
                colorText: "#474a51"
            }

            ButtonCriado {
                buttonName: "Tentar Novamente"
                colorButtonPressed: "#afc0ff"
                colorBorderPressed: "#afc0ff"
                width: popupInformative.width / 3
                height: popupInformative.height / 4.5
                onClicked: {
                    if (bilheteEntradaOuSaida.reenviarTicket(idBilhete)) {
                        animationFailOuSucessValidation.validaStatusAnimation = true
                    }
                    animationFailOuSucessValidation.validaStatusAnimation = false
                    animationFailOuSucessValidation.validarAnimacao()
                }
            }
        }
    }

    NotoFailOrSucess {
        id: animationFailOuSucessValidation
        width: 330
        height: 150
        anchors.right: parent.right
        color: "transparent"
        visible: false
        mensagemErro: "Erro Ao enviar Bilhete"
        mensagemSuccess: "Sucesso Ao enviar Bilhete"
        fromYinit: 105
        fromYdone: 125
    }
}
