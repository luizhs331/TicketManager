import QtQuick 2.12
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12
import br.com.configgeral 1.0
import br.com.bilheteentradaousaida 1.0

import "qml/components"

Rectangle {
    id: mainRetangulo
    width: parent.width
    height: parent.height
    visible: false
    color: "#f1f1f1"
    onVisibleChanged: {
        textNome.focus = true
        numPage = 1
        numberOfPages = 1
        mainRetangulo.clear()
    }

    property var retornoTextComboBox
    property var turmas: configGeral.comboBox
    property var currentSearchName: ""
    property int numPage: 1
    property int numberOfPages: 1

    property bool isPagination: true

    signal listarBilhetes(var nome, var turma, var numPage)
    signal voltarPagina

    onListarBilhetes: function (nome, turma, numPage) {
        numberOfPages = bilheteEntradaOuSaida.listarBilhetes(nome,
                                                             turma, numPage)
    }

    BilheteEntradaOuSaida {
        id: bilheteEntradaOuSaida
    }

    ConfigGeral {
        id: configGeral
    }

    function retanguloLadoDireito() {
        retanguloDireita.visible = bilheteEntradaOuSaida.hasDtos()
        retanguloDireitaPai.visible = bilheteEntradaOuSaida.hasDtos()
    }

    function findTickets() {

        var numPage = isPagination ? mainRetangulo.numPage : 0

        var numAllPage = mainRetangulo.listarBilhetes(
                    currentSearchName,
                    mainRetangulo.retornoTextComboBox, numPage)
        mainRetangulo.dtoSetar()
        mainRetangulo.retanguloLadoDireito()
        return numAllPage
    }

    function clear() {
        textNome.text = ""
        turmaAluno.text = ""
        textNome.focus = true
    }

    function dtoLimpar() {
        repeater.model = 0
    }
    function dtoSetar() {
        repeater.model = bilheteEntradaOuSaida.dtos
    }

    Rectangle {
        id: retanguloComponents
        width: mainRetangulo.width / 1.05
        height: mainRetangulo.height / 1.05
        color: "#f1f1f1"

        anchors.centerIn: parent

        Image {
            id: img
            width: parent.width / 4
            height: parent.width / 4
            opacity: 0.5
            source: "img/lupa.png"
            anchors.centerIn: parent
            anchors.leftMargin: 5
        }

        Column {
            width: retanguloComponents.width
            height: retanguloComponents.height
            spacing: 35

            Row {
                id: row
                spacing: 15
                width: parent.width

                TextField {
                    id: textNome
                    width: parent.width / 1.56
                    height: 55
                    placeholderText: "Buscar Por Nome"
                    background: Rectangle {
                        radius: 10
                        border.color: textNome.focus ? "#11cdef" : "#d3d3d3"
                    }
                }
                RowLayout {
                    id: rouList
                    Layout.fillWidth: true
                    width: 200
                    height: 55
                    NotoComboBox {
                        modelTxt: turmas
                        isTextConcat: true
                        textEditComboBox: "Turma:"
                        width: 200
                        height: 55
                        onEmitirText: function (texto) {
                            mainRetangulo.retornoTextComboBox = texto
                        }
                    }
                }

                Keys.onReturnPressed: {
                    currentSearchName = textNome.text
                    findTickets()
                }

                //                Keys.onPressed: {
                //                    if ((event.key === 16777220)
                //                            || (event.key === Qt.Key_Enter)) {
                //                        currentSearchName = textNome.text
                //                        findTickets()
                //                        event.accepted = true
                //                    }
                //                }
                ButtonCriado {
                    id: btnDeBusca
                    width: parent.width / 5.5
                    height: 55
                    spacing: 10
                    buttonName: "Buscar"
                    colorButtonPressed: "#afc0ff"
                    colorBorderPressed: "#afc0ff"
                    onClicked: {
                        //      turmadoaluno.currentIndex == 1 ? turmadoaluno.currentText = "" : turmadoaluno.currentText
                        currentSearchName = textNome.text
                        findTickets()
                    }
                }
            }

            Row {

                id: rowId
                spacing: 20
                width: retanguloComponents.width

                Flickable {
                    id: flickable
                    width: retanguloComponents.width / 2 - 30
                    height: retanguloComponents.height / 1.3
                    contentHeight: todoList.height
                    clip: true

                    Column {
                        id: todoList
                        width: retanguloComponents.width
                        spacing: 10

                        Repeater {
                            model: bilheteEntradaOuSaida.dtos
                            id: repeater

                            RectangularGlow {
                                id: effect1
                                anchors.fill: todo
                                glowRadius: 10
                                cornerRadius: 10
                                spread: 0.1
                                color: "#d3d3d3"
                            }

                            Todo {

                                //                            color: /*Qt.rgba(244 , 248 , 251 , 1 )*/
                                id: todo
                                nome: modelData.nome
                                turma: modelData.turma
                                assinaturaPedagogo: modelData.assinatura
                                tipoBilhete: modelData.tipoBilhete
                                turno: modelData.turno
                                motivoEmissao: modelData.motivo
                                descricaoBilhete: modelData.description
                                horaEmissao: modelData.hora
                                dataEmissao: modelData.data
                                idBilhete: modelData.idBilhete
                                border.color: mouseArea.containsMouse ? "#87cefa" : "#d3d3d3"
                                width: todoList.width / 1.2 + 50

                                RotationAnimation on width {
                                    running: mouseArea.containsMouse
                                    from: todo.width
                                    to: todoList.width / 1.1 + 40
                                    duration: 200
                                }

                                RotationAnimation on height {
                                    running: mouseArea.containsMouse
                                    from: todo.height
                                    to: flickable.height / 2.05
                                    duration: 200
                                }

                                SequentialAnimation {
                                    running: !mouseArea.containsMouse
                                    NumberAnimation {
                                        target: todo
                                        property: "width"
                                        to: todoList.width / 1.2 + 85
                                        duration: 200
                                    }
                                }

                                SequentialAnimation {
                                    running: !mouseArea.containsMouse
                                    NumberAnimation {
                                        target: todo
                                        property: "height"
                                        to: flickable.height / 2.1
                                        duration: 200
                                    }
                                }

                                MouseArea {
                                    id: mouseArea
                                    anchors.fill: todo
                                    hoverEnabled: true
                                }

                                //                                Rectangle {
                                //                                    anchors.right: parent.right
                                //                                    color: "transparent"
                                //                                    width: 50
                                //                                    height: 50
                                //                                    visible: true

                                //                                    Image {
                                //                                        id: imgDelete
                                //                                        anchors.centerIn: parent
                                //                                        width: 20
                                //                                        height: 20
                                //                                        source: "img/imgDelete.jpg"
                                //                                        anchors.topMargin: 15

                                //                                        MouseArea {
                                //                                            id: mouseAreaImgDelete
                                //                                            hoverEnabled: true
                                //                                            anchors.fill: parent
                                //                                        }
                                //                                    }

                                //                                    Image {
                                //                                        id: imgUpdate
                                //                                        anchors.top: imgDelete.bottom
                                //                                        anchors.horizontalCenter: parent.horizontalCenter
                                //                                        width: mouseAreaImgUpdate.containsMouse ? 25 : 20
                                //                                        height: mouseAreaImgUpdate.containsMouse ? 25 : 20
                                //                                        source: "img/imgUpdate.png"
                                //                                        anchors.topMargin: 15

                                //                                        MouseArea {
                                //                                            id: mouseAreaImgUpdate
                                //                                            hoverEnabled: true
                                //                                            anchors.fill: parent
                                //                                        }
                                //                                    }
                                //                                }
                            }
                        }
                    }

                    ScrollBar.vertical: ScrollBar {
                        active: flickable.interactive
                    }
                }

                Rectangle {
                    id: retanguloDireitaPai
                    width: parent.width / 2.0 - 5
                    height: parent.height
                    visible: bilheteEntradaOuSaida.hasDtos()
                    color: "#fff"
                    radius: 10

                    RectangularGlow {
                        id: effect4
                        anchors.fill: retanguloDireita
                        glowRadius: 0.8
                        cornerRadius: 0.8
                        spread: 100
                        color: "#d3d3d3"
                    }

                    Rectangle {
                        id: retanguloDireita
                        width: parent.width
                        height: parent.height
                        color: "#fff"
                        visible: bilheteEntradaOuSaida.hasDtos()
                        radius: 2

                        Rectangle {
                            id: recTop
                            width: parent.width
                            height: 75
                            color: Qt.rgba(1, 0.550, 0.240, 0.72)
                            radius: 0
                            z: 1

                            Label {
                                text: "INFORMAÇÕES DOS BILHETES"
                                font.pointSize: 18
                                visible: hasModeAllTickets
                                anchors.centerIn: parent
                                color: "#fff"
                            }
                        }

                        Rectangle {
                            id: textRetangulo
                            width: parent.width / 1.1
                            height: parent.height / 1.1
                            color: "transparent"
                            anchors.top: recTop.bottom
                            anchors.horizontalCenter: parent.horizontalCenter
                            z: 0

                            Column {
                                width: parent.width
                                height: parent.height - (recTop.height * 0.6)
                                spacing: 30
                                anchors.centerIn: parent

                                Repeater {
                                    id: repeaterDireita
                                    model: bilheteEntradaOuSaida.dtosTelaBusca
                                    TelaListarInformacoesAluno {
                                        id: todoListarInformacoesAlunos
                                        hasModeAllTickets: currentSearchName.length
                                                           > 0 ? false : true
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Row {
                id: rowPage
                width: parent.width / 10
                spacing: 25

                Rectangle {
                    width: rowPage.width / 1.5
                    height: 55
                    color: "#dfe0e1"
                    radius: 3

                    Label {
                        text: numPage + '/' + numberOfPages
                        anchors.centerIn: parent
                        opacity: 0.6
                        font.bold: true
                    }
                }

                ButtonCriado {
                    id: btnVoltarPage
                    width: rowPage.width
                    height: 55
                    buttonName: "<"
                    colorBorderPressed: "#afc0ff"
                    colorButtonPressed: "#afc0ff"
                    visible: isPagination
                    onClicked: function () {
                        if (mainRetangulo.numPage > 1) {
                            mainRetangulo.numPage--
                            findTickets()
                        }
                    }
                }

                ButtonCriado {
                    id: btnAvancarPag
                    width: rowPage.width
                    height: 55
                    buttonName: ">"
                    colorBorderPressed: "#afc0ff"
                    colorButtonPressed: "#afc0ff"
                    visible: isPagination
                    onClicked: function () {
                        if (mainRetangulo.numPage < numberOfPages) {
                            mainRetangulo.numPage++
                            findTickets()
                        }
                    }
                }

                ButtonCriado {
                    id: btnVoltar
                    width: parent.width * 1.5
                    height: 55
                    buttonName: "Voltar"
                    colorBorderPressed: "#afc0ff"
                    colorButtonPressed: "#afc0ff"
                    onClicked: function () {
                        mainRetangulo.voltarPagina()
                        mainRetangulo.dtoLimpar()
                        retanguloDireita.visible = false
                        retanguloDireitaPai.visible = false
                    }
                }
            }
        }
    }
}
