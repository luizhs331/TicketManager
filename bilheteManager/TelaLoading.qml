import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12
import br.com.bilheteentradaousaida 1.0

Popup {
    id: telaLoading
    anchors.centerIn: parent
    modal: true
    focus: true
    z: 0

    background: Rectangle {
        width: parent.width
        height: parent.height
        radius: 10
    }

    closePolicy: function () {
        return Popup.CloseOnEscape || Popup.CloseOnPressOutsideParent
    }

    signal buttonPressed

    property bool statusFailOrSucess: false
    property bool animationRunnigFalseOrTrue: false

    property var buttonColorCheck
    property var msgSuccess: 'SUCCESS!'

    onAnimationRunnigFalseOrTrueChanged: {
        if (!animationRunnigFalseOrTrue) {
            img.playing = true
        }
    }

    onOpened: {
        if (!animationRunnigFalseOrTrue) {
            img.playing = true
        }
    }

    // FIXME Corrigir o status indefinido , criar um enum para gerenciar os 3 status ao inves de utilizar um bool
    function updateStateLoading(tipo) {

        switch (tipo) {
        case 'SUCCESS':
            img.playing = true
            msmSucesso.text = msgSuccess
            backgroundButton.color = '#74d175'
            return "img/loading.gif"
        case 'ERROR':
            msmSucesso.text = 'ERROR'
            backgroundButton.color = 'red'
            return "img/fail.png"
        }
    }

    function validarCorButton() {
        if (buttonColorCheck && statusFailOrSucess) {
            return "#74d175"
        } else if (buttonColorCheck && !statusFailOrSucess) {
            return "red"
        }
        return "#dfe0e1"
    }

    Column {
        id: columnLoading
        width: telaLoading.width
        height: telaLoading.height / 1.5
        anchors.centerIn: parent
        spacing: 40

        Label {
            id: msmSucesso
            text: 'LOADING'
            font.pointSize: 36
            color: statusFailOrSucess ? "#47ad49" : "red"
            anchors.horizontalCenter: columnLoading.horizontalCenter
            visible: true
        }

        Column {
            width: 80
            height: 80
            anchors.horizontalCenter: parent.horizontalCenter
            visible: animationRunnigFalseOrTrue
            spacing: 0

            Rectangle {
                id: animationGrenn
                width: telaLoading.width / 5.5
                height: telaLoading.width / 5.5
                radius: 360
                border.color: "#fff"
                color: "#47ad49"
                opacity: 0.3
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                visible: animationRunnigFalseOrTrue

                NumberAnimation on height {
                    loops: Animation.Infinite
                    from: animationGrenn.height
                    to: 80
                    duration: 2000
                }

                NumberAnimation on width {
                    loops: Animation.Infinite
                    from: animationGrenn.width
                    to: 80
                    duration: 2000
                }
            }

            Rectangle {
                id: animationWhite
                width: 30
                height: 30
                border.color: "#fff"
                color: "#fff"
                radius: 360
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                visible: animationRunnigFalseOrTrue

                NumberAnimation on height {
                    id: animation
                    loops: Animation.Infinite
                    from: animationWhite.height
                    to: 80
                    duration: 2000
                }

                NumberAnimation on width {
                    loops: Animation.Infinite
                    from: animationWhite.width
                    to: 80
                    duration: 2000
                }
            }
        }

        AnimatedImage {
            id: img
            width: 80
            height: 80
            source: updateStateLoading(statusFailOrSucess ? 'SUCCESS' : 'ERROR')
            visible: !animationRunnigFalseOrTrue
            opacity: 0.9
            playing: img.visible ? true : false
            anchors.horizontalCenter: columnLoading.horizontalCenter
        }

        Button {
            id: buttonVoltar
            width: mouseAreaSalvar.pressed ? parent.width / 1.6 : parent.width / 2.1
            height: mouseAreaSalvar.pressed ? parent.height / 5.5 : parent.height / 6.5
            anchors.horizontalCenter: columnLoading.horizontalCenter

            Label {
                id: lblButton
                text: "Confirmar"
                font.family: "Helvetica"
                font.pointSize: 15
                color: '#fff'
                focus: true
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
            }
            background: Rectangle {
                id: backgroundButton
                color: telaLoading.validarCorButton()
                radius: 5
            }
            MouseArea {
                id: mouseAreaSalvar
                hoverEnabled: true
                anchors.fill: parent
                onClicked: {
                    telaLoading.buttonPressed()
                }
            }
        }
    }
}
