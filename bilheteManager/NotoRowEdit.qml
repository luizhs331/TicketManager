import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

Rectangle {
    id: root
    color: backgroundColor

    property string backgroundColor: "transparent"

    property alias title: labelText.text
    property alias texto: assinatura.text

    onVisibleChanged: {
        assinatura.enabled = false
    }

    Row {
        id: row
        width: root.width
        height: parent.height

        Rectangle {
            width: parent.width / 2
            height: parent.height
            color: backgroundColor
            Rectangle {
                width: parent.width / 1.1
                height: parent.height
                color: "#11cdef"
                radius: 10
                anchors.horizontalCenter: parent.horizontalCenter
                Label {
                    id: labelText
                    text: title
                    font.family: "Helvetica"
                    font.pointSize: 12
                    color: "#fff"
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    opacity: mouseAreaNovoBilhete.containsMouse ? 1 : 0.6
                }
            }
        }

        Rectangle {
            color: backgroundColor
            width: parent.width / 2
            height: parent.height
            TextField {
                id: assinatura
                placeholderText: ""
                text: texto
                width: parent.width / 1.1
                height: parent.height
                anchors.horizontalCenter: parent.horizontalCenter
                background: Rectangle {
                    radius: 5
                    border.color: assinatura.focus ? /*"#87cefa"*/ "#11cdef" : "#d3d3d3"
                }
            }
        }
    }
}
