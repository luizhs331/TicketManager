import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import br.com.escolacontroller 1.0
import br.com.escolamodel 1.0

import "qml/components"

Popup {
    id: popup
    width: parent.width
    height: parent.height
    anchors.centerIn: parent
    modal: true
    visible: popupRectangle.visible
    focus: true
    z: 0

    EscolaModel {
        id: escolaModel
        nomeEscola: txtNomeEscola.currentText
        usuario: txtUsuario.currentText
        telefone: txtTelefone.currentText
        estadoUf: "PR" // cbUf.modelTxt
        cidade: "Dois Vizinhos" // cbCi.modelTxdadet
        rua: txtRua.currentText
        bairro: txtBairro.currentText
        email: txtEmail.currentText
        senha: txtSenha.currentText
    }

    function clearAllTextFilds() {
        txtNomeEscola.currentText = ''
        txtUsuario.currentText = ''
        txtTelefone.currentText = ''
        txtRua.currentText = ''
        txtBairro.currentText = ''
        txtEmail.currentText = ''
        txtSenha.currentText = ''
        cbUf.currentIndexBox = -1
        cbCidade.currentIndexBox = -1
    }

    function validateIfInformationsValid() {

        var isValid = true

        isValid &= txtNomeEscola.notIsEmpty()
        isValid &= txtUsuario.notIsEmpty()
        isValid &= txtBairro.notIsEmpty()
        isValid &= txtRua.notIsEmpty()
        isValid &= txtEmail.validIfContains('@')

        if (!txtSenha.notIsEmpty() || !txtSenhaConfirmacao.notIsEmpty()) {
            txtSenha.textErro = "( Campo obrigatório )"
        } else {
            txtSenha.showTextErro = txtSenha.currentText !== txtSenhaConfirmacao.currentText
            txtSenha.textErro = "( As senhas não correspondem )"
        }

        isValid &= !txtSenha.showTextErro

        return isValid
    }

    background: Rectangle {
        radius: 10
    }

    closePolicy: function () {
        return Popup.CloseOnEscape || Popup.CloseOnPressOutsideParent
    }

    EscolaController {
        id: cadastroEscola
    }

    Rectangle {
        width: popup.width / 1.2
        height: popup.height / 1.1
        anchors.centerIn: parent

        Label {
            id: textCadastroPrincipal
            text: "Cadastro"
            font.family: "Helvetica"
            font.pointSize: 25
            color: "#322153"
            opacity: 1
        }

        Flickable {
            id: flickable
            width: parent.width
            height: parent.height / 1.2
            contentHeight: columnFlickable.height
            anchors.centerIn: parent
            clip: true

            Column {
                id: columnFlickable
                width: parent.width
                spacing: 20

                Label {
                    id: textCadastro
                    text: "Dados"
                    font.family: "Helvetica"
                    font.pointSize: 15
                    color: "#322153"
                    opacity: 1
                }

                TextFieldWithText {
                    id: txtNomeEscola
                    width: columnFlickable.width
                    height: 65
                    titleField: "Nome Escola"
                    isMandatoryField: true
                }

                Row {
                    spacing: 10
                    width: columnFlickable.width

                    TextFieldWithText {
                        id: txtEmail
                        width: columnFlickable.width / 2.04
                        height: 65
                        titleField: "E-mail"
                        textErro: "( Email Invalido )"
                    }

                    TextFieldWithText {
                        id: txtTelefone
                        width: columnFlickable.width / 2.04
                        height: 65
                        titleField: "Telefone"
                        customRegex: RegExpValidator {
                            regExp: /^(\d{2})\D*(\d{5}|\d{4})\D*(\d{4})$/
                        }
                    }
                }

                TextFieldWithText {
                    id: txtUsuario
                    width: columnFlickable.width
                    height: 65
                    titleField: "Nome Usuario"
                    isMandatoryField: true
                }

                Row {
                    spacing: 10
                    width: columnFlickable.width

                    TextFieldWithText {
                        id: txtSenha
                        width: columnFlickable.width / 2.04
                        height: 65
                        titleField: "Senha"
                        typeIsPassword: true
                        isMandatoryField: true
                        textErro: "( As senhas não correspondem )"
                    }

                    TextFieldWithText {
                        id: txtSenhaConfirmacao
                        width: columnFlickable.width / 2.04
                        height: 65
                        titleField: "Confirme a senha"
                        typeIsPassword: true
                        isMandatoryField: true
                    }
                }
                Label {
                    id: txtEndereco
                    text: "Endereco"
                    font.family: "Helvetica"
                    font.pointSize: 13
                    color: "#322153"
                    opacity: 1
                }
                Column {
                    spacing: 45
                    Row {
                        spacing: 10

                        NotoComboBox {
                            id: cbUf
                            width: txtEmail.width
                            height: 50
                            fontSizeEdit: 13
                            modelTxt: [' ', 'Dois Vizinhos'] // cadastroEscola.selectUfs()
                            textEditComboBox: "Selecione Uma Uf"
                            isRequiredfield: true
                            currentIndexBox: -1
                            onEmitirText: function (texto) {
                                escolaModel.estadoUf = texto
                            }
                        }
                        NotoComboBox {
                            id: cbCidade
                            width: txtEmail.width
                            height: 50
                            fontSizeEdit: 13
                            modelTxt: [' ', 'Dois Vizinhos'] /*cadastroEscola.selectCity('PR')*/
                            textEditComboBox: "Selecione Uma cidade"
                            isRequiredfield: true
                            currentIndexBox: -1
                            onEmitirText: function (texto) {//                            escolaModel.cidade = texto
                            }
                        }
                    }
                    TextFieldWithText {
                        id: txtBairro
                        width: columnFlickable.width
                        height: 65
                        titleField: "Nome Bairro"
                        isMandatoryField: true
                    }
                }
                TextFieldWithText {
                    id: txtRua
                    width: columnFlickable.width
                    height: 65
                    titleField: "Nome Rua"
                    isMandatoryField: true
                }
                Rectangle {
                    color: "transparent"
                    width: columnFlickable.width
                    height: 50
                }
            }
        }
        ButtonCriado {
            id: buttonLogin
            widthButton: parent.width / 2.5
            heightButton: parent.height / 10
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: flickable.bottom
            colorButtonPressed: "#afc0ff"
            colorBorderPressed: "#afc0ff"
            buttonName: "Confirmar"
            onClicked: {
                if (validateIfInformationsValid()) {
                    if (cadastroEscola.registerSchool(escolaModel)) {
                        telaLoading.open()
                        clearAllTextFilds()
                    }
                }
            }
        }
    }

    TelaLoading {
        id: telaLoading
        statusFailOrSucess: true
        width: 375
        height: 375
        msgSuccess: 'Success!'

        onButtonPressed: {
            telaLoading.close()
            popup.close()
        }
    }
}
