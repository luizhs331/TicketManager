import QtQuick 2.0
import QtQuick.Controls 2.0
import br.com.escolacontroller 1.0

import br.com.escolamodel 1.0

import "qml/components"

Item {
    id: configEscola

    property EscolaModel school: escolaController.findSchool()

    EscolaController {
        id: escolaController
    }

    signal buttonSavePressed
    signal processoConcluidoStatus(var result)

    onButtonSavePressed: {
        escolaController.changeNameSchool(txtNomeEscola.currentText)
        processoConcluidoStatus(true)
    }

    Rectangle {
        id: segundoRetangulo
        width: configEscola.width
        height: configEscola.height
        anchors.centerIn: parent
        color: colorBackground

        Column {
            anchors.centerIn: parent
            width: configEscola.width - (configEscola.width / 10)
            height: configEscola.height - (configEscola.width / 10)
            spacing: 15

            Label {
                font.pointSize: 18
                color: "#000"
                text: "Configurações Escola"
            }

            TextFieldWithText {
                id: txtNomeEscola
                width: parent.width / 2
                height: configEscola.height / 11
                titleField: "Nome Escola"
                isMandatoryField: false
                currentText: school.nomeEscola
                textSize: 13
                placeholderText: "nome escola"
            }
        }
    }
}
