#include "notificacoescontroller.h"

#include "notificacao/notificacoesservice.h"
#include "notificacao/notificacoesmodel.h"

QList<QObject*> NotificacoesController::buscarNotificacoesMensagens() {

    QList<QObject*> dtos = {};

    QList<NotificacoesModel*> notificacoes = NotificacoesService().buscarNotificacoesMensagens();

    if( !notificacoes.isEmpty() ) {
        for( int i = notificacoes.count() - 1; i >=0; i-- ) {

            dtos.append( notificacoes.at( i ) );
        }
    }

    return dtos;
}

void NotificacoesController::updateNotificacao() const {
    NotificacoesService().updateNotificacao();
}

int NotificacoesController::validarHasNotificacoes() {
    QList<NotificacoesModel*> listNotificacoes = NotificacoesService().buscarNotificacoesMensagens();

    int nrNotificacoesNaoVisualizadas = 0;

    for( NotificacoesModel* notificacoes : listNotificacoes ) {
        notificacoes->fgVisualizado() ? false : nrNotificacoesNaoVisualizadas++;
    }

    return nrNotificacoesNaoVisualizadas;
}

void NotificacoesController::deleteOneNotificationByIdBilhete( QString idBilhete ) const {
    NotificacoesService().deleteOneNotificationByIdBilhete( idBilhete );
}

void NotificacoesController::deleteAllNotifications() const {
    NotificacoesService().deleteAllNotifications();
}
