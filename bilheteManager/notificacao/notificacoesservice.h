#ifndef NOTIFICACOESSERVICE_H
#define NOTIFICACOESSERVICE_H

#include "notificacao/notificacoesmodel.h"

class NotificacoesService {
public:

    void updateNotificacao() const;
    void deleteAllNotifications() const;
    void deleteOneNotificationByIdBilhete( QString idNotificacao ) const;
    void insertNotificacao( const NotificacoesModel* notificacoes ) const;

    QList<NotificacoesModel*> buscarNotificacoesMensagens();


};

#endif // NOTIFICACOESSERVICE_H
