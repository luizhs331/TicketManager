#ifndef NOTIFICACOESMODEL_H
#define NOTIFICACOESMODEL_H

#include <QObject>

#include "notificacao/tiponotificacaoenum.h"

class NotificacoesModel : public QObject {
    Q_OBJECT
    Q_PROPERTY( int idBilhete READ idBilhete WRITE setIdBilhete NOTIFY idBilheteChanged )
    Q_PROPERTY( QString dsMensagem READ dsMensagem WRITE setDsMensagem NOTIFY dsMensagemChanged )
    Q_PROPERTY( QString tituloMensagem READ tituloMensagem WRITE setTituloMensagem NOTIFY tituloMensagemChanged )
    Q_PROPERTY( QString tipoEnum READ enumToString NOTIFY enumChanged )
    Q_PROPERTY( bool fgVisualizado READ fgVisualizado NOTIFY fgVisualizadoChanged );

public:
    int idBilhete() const;
    void setIdBilhete( int value );

    QString dsMensagem() const;
    void setDsMensagem( const QString& value );

    QString tituloMensagem() const;
    void setTituloMensagem( const QString& value );

    TipoNotificacaoEnum tipoEnum() const;
    void setTipoEnum( const TipoNotificacaoEnum& value );

    QString enumToString();

    int idMensagem() const;
    void setIdMensagem( int idMensagem );

    bool fgVisualizado() const;
    void setFgVisualizado( bool fgVisualizado );

signals:
    void idBilheteChanged();
    void dsMensagemChanged();
    void tituloMensagemChanged();
    void enumChanged();
    void fgVisualizadoChanged();

private:

    int _idBilhete = 0;
    int _idMensagem = 0;

    bool _fgVisualizado = false;

    QString _dsMensagem = "";
    QString _tituloMensagem = "";
    TipoNotificacaoEnum _tipoEnum = TipoNotificacaoEnum::VERIFICAR;

};

#endif // NOTIFICACOESMODEL_H
