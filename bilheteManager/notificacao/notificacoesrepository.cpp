#include "notificacoesrepository.h"

#include <QtSql>
#include <QChar>

#include <sqls.h>

#include "helperconverter.h"

void NotificacoesRepository::insertNotificacao( const NotificacoesModel* notificacoes ) const {

    QSqlQuery query;

    char castEnum = static_cast<char>( notificacoes->tipoEnum() );

    query.prepare( Sqls::createQuery( INSERT_NOTIFICACAO, "selectsqls/notificacoes/" ) );

    query.bindValue( ":DS_MENSAGEM", notificacoes->dsMensagem() );
    query.bindValue( ":TITULO_MENSAGEM", notificacoes->tituloMensagem() );
    query.bindValue( ":TP_MENSAGEM", QString( castEnum ) );
    query.bindValue( ":ID_BILHETE", notificacoes->idBilhete() );

    query.exec();

}

void NotificacoesRepository::updateNotificacao() const {
    QSqlQuery query;
    query.prepare( "UPDATE notificacoes SET flag_visualizado = true" );
    query.exec();
}

QList<NotificacoesModel*> NotificacoesRepository::buscarNotificacoes() const {

    QSqlQuery query;

    QList<NotificacoesModel*> listRetorno = {};

    query.prepare( Sqls::createQuery( BUSCAR_NOTIFICACOES, "selectsqls/notificacoes/" ) );

    if( query.exec() ) {

        while( query.next() ) {

            NotificacoesModel* notificacaoModel = new NotificacoesModel();

            char retorno = ConverterEnum().toChar( query.value( NotificacoesRepository::TP_MENSAGEM ).toString() );

            notificacaoModel->setTipoEnum( static_cast<TipoNotificacaoEnum>( retorno ) );
            notificacaoModel->setIdMensagem( query.value( NotificacoesRepository::ID_MENSAGEM ).toInt() );
            notificacaoModel->setDsMensagem( query.value( NotificacoesRepository::DS_MENSAGEM ).toString() );
            notificacaoModel->setTituloMensagem( query.value( NotificacoesRepository::TITULO_MENSAGEM ).toString() );
            notificacaoModel->setFgVisualizado( query.value( NotificacoesRepository::FG_VISUALIZADO ).toBool() );
            notificacaoModel->setIdBilhete( query.value( NotificacoesRepository::ID_BILHETE ).toInt() );

            listRetorno.append( notificacaoModel );
        }
    }
    return listRetorno;
}

void NotificacoesRepository::deleteOneNotificationByIdBilhete( QString idBilhete ) const {
    QSqlQuery query;

    query.prepare( QString( "DELETE FROM notificacoes WHERE id_bilhete = %0" ).arg( idBilhete ) );

    query.exec();

}

void NotificacoesRepository::deleteAllNotifications() const {
    QSqlQuery query;

    query.prepare( QString( "DELETE FROM notificacoes" ) );

    query.exec();
}


