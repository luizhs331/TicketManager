#include "notificacoesmodel.h"

int NotificacoesModel::idBilhete() const{
    return _idBilhete;
}

void NotificacoesModel::setIdBilhete(int value){
    _idBilhete = value;
}

QString NotificacoesModel::dsMensagem() const{
    return _dsMensagem;
}

void NotificacoesModel::setDsMensagem(const QString &value){
    _dsMensagem = value;
}

QString NotificacoesModel::tituloMensagem() const{
    return _tituloMensagem;
}

void NotificacoesModel::setTituloMensagem(const QString &value){
    _tituloMensagem = value;
}

TipoNotificacaoEnum NotificacoesModel::tipoEnum() const{
    return _tipoEnum;
}

void NotificacoesModel::setTipoEnum( const TipoNotificacaoEnum &value ){
    _tipoEnum = value;
}

QString NotificacoesModel::enumToString(){
    return QString( static_cast<char>( _tipoEnum ) );
}

int NotificacoesModel::idMensagem() const
{
    return _idMensagem;
}

void NotificacoesModel::setIdMensagem(int idMensagem)
{
    _idMensagem = idMensagem;
}

bool NotificacoesModel::fgVisualizado() const
{
    return _fgVisualizado;
}

void NotificacoesModel::setFgVisualizado(bool fgVisualizado)
{
    _fgVisualizado = fgVisualizado;
}
