#ifndef NOTIFICACOESCONTROLLER_H
#define NOTIFICACOESCONTROLLER_H

#include <QObject>

class NotificacoesController : public QObject {
    Q_OBJECT
    Q_PROPERTY( QList<QObject*> notificacoesDto READ buscarNotificacoesMensagens NOTIFY notificacoesMensagem )
public:
    QList<QObject*> buscarNotificacoesMensagens();

public slots:
    void updateNotificacao() const;

    int validarHasNotificacoes();

    void deleteOneNotificationByIdBilhete( QString idBilhete ) const;
    void deleteAllNotifications() const;

signals:
    void notificacoesMensagem();

};

#endif // NOTIFICACOESCONTROLLER_H
