#ifndef NOTIFICACOESREPOSITORY_H
#define NOTIFICACOESREPOSITORY_H

#include <QObject>

#include "notificacao/notificacoesmodel.h"

class NotificacoesRepository : public QObject {
    Q_OBJECT
public:
    void updateNotificacao() const;
    void deleteAllNotifications() const;
    void deleteOneNotificationByIdBilhete( QString idBilhete ) const;
    void insertNotificacao( const NotificacoesModel* notificacoes ) const;

    QList<NotificacoesModel*> buscarNotificacoes() const;

    static constexpr const char* ID_MENSAGEM = "id_mensagem";
    static constexpr const char* TITULO_MENSAGEM = "titulo_mensagem";
    static constexpr const char* DS_MENSAGEM = "ds_mensagem";
    static constexpr const char* TP_MENSAGEM = "tp_mensagem";
    static constexpr const char* FG_VISUALIZADO = "flag_visualizado";
    static constexpr const char* ID_BILHETE = "id_bilhete";

    // SQLS

    static constexpr const char* BUSCAR_NOTIFICACOES = "BUSCAR_NOTIFICACOES.txt";
    static constexpr const char* INSERT_NOTIFICACAO = "INSERT_NOTIFICACAO.txt";

};

#endif // NOTIFICACOESREPOSITORY_H
