#ifndef TIPONOTIFICACAOENUM_H
#define TIPONOTIFICACAOENUM_H

#endif // TIPONOTIFICACAOENUM_H

enum class TipoNotificacaoEnum : char{

    VERIFICAR = 'V',
    FAIL = 'F',
    SUCCESS = 'S'
};
