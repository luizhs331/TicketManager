#include "notificacoesservice.h"

#include "notificacoesrepository.h"

void NotificacoesService::insertNotificacao( const NotificacoesModel* notificacoes ) const {
    NotificacoesRepository().insertNotificacao( notificacoes );
}

void NotificacoesService::updateNotificacao() const {
    NotificacoesRepository().updateNotificacao();
}

QList<NotificacoesModel*> NotificacoesService::buscarNotificacoesMensagens() {
    return NotificacoesRepository().buscarNotificacoes();
}

void NotificacoesService::deleteAllNotifications() const
{}

void NotificacoesService::deleteOneNotificationByIdBilhete( QString idBilhete ) const {
    NotificacoesRepository().deleteOneNotificationByIdBilhete( idBilhete );
}
