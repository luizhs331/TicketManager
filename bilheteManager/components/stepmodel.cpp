#include "stepmodel.h"

StepModel::StepModel( const QString& title, const QString& imgLink ) :
    _title(title),
    _imgLink(imgLink),
    _done(false),
    _selected(false){}

QString StepModel::title() const{
    return _title;
}

void StepModel::setTitle(const QString &title){
    _title = title;
}

QString StepModel::imgLink() const{
    return _imgLink;
}

void StepModel::setImgLink(const QString &imgLink){
    _imgLink = imgLink;
}

bool StepModel::done() const{
    return _done;
}

void StepModel::setDone(bool done){
    _done = done;
    emit doneChanged();
}

bool StepModel::selected() const{
    return _selected;
}

void StepModel::setSelected(bool selected){
    _selected = selected;
    emit selectedChanged();
}

void StepModel::alterStateStep(bool selected, bool done){
    setSelected(selected);
    setDone( done );
}
