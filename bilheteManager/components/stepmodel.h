#ifndef STEPMODEL_H
#define STEPMODEL_H

#include <QObject>

class StepModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(QString imgLink READ imgLink WRITE setImgLink NOTIFY imgLinkChanged)
    Q_PROPERTY(bool done READ done WRITE setDone NOTIFY doneChanged)
    Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectedChanged)
public:
    explicit StepModel( const QString& title = "", const QString& imgLink = "" );

    QString title() const;
    void setTitle(const QString &title);

    QString imgLink() const;
    void setImgLink(const QString &imgLink);

    bool done() const;
    void setDone(bool done);

    bool selected() const;
    void setSelected(bool selected);

    void alterStateStep( bool selected, bool done );

signals:
    void titleChanged();
    void imgLinkChanged();
    void doneChanged();
    void selectedChanged();

private:
    QString _title;
    QString _imgLink;
    bool _done;
    bool _selected;

};

#endif // STEPMODEL_H
