#ifndef STEPCONTROLLER_H
#define STEPCONTROLLER_H

#include <QObject>

#include <QList>
#include <QPair>

#include "stepmodel.h"

class StepController : public QObject
{
    Q_OBJECT
public:
    explicit StepController();

    QList<StepModel *> listSteps() const;
    void setListSteps(const QList<StepModel*> &listSteps);
    void initSteps( const QList<QPair<QString,QString>>& listSteps );

    void addStep( StepModel* step );

    void changeStep( int indexStepSelected );

    QList<QObject*> stepsToQObject();

    StepModel* stepByIndex( int index );
private:
    QList<StepModel*> _listSteps;

};

#endif // STEPCONTROLLER_H
