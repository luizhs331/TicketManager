#include "stepcontroller.h"

#include <QDebug>

StepController::StepController():
    _listSteps({}){}

QList<StepModel*> StepController::listSteps() const{
    return _listSteps;
}

void StepController::setListSteps(const QList<StepModel *> &listSteps)
{
    _listSteps = listSteps;
}

void StepController::initSteps( const QList<QPair<QString,QString>>& listSteps ){
    for( QPair<QString,QString> currentStep : listSteps ){
        addStep( new StepModel( currentStep.first, currentStep.second ) );
    }
}

void StepController::addStep( StepModel* step){
    _listSteps.append( step );
}

void StepController::changeStep(int indexStepSelected){

    qInfo() << "StepController::changeStep [INDEX_SELECTEC]" << indexStepSelected << "[STEP_COUNT]" <<  _listSteps.count();

    bool isLastStep = indexStepSelected == ( _listSteps.length() - 1 );

    int i = -1;
    for( StepModel* step : _listSteps ){
        i++;
        if( i > indexStepSelected ){
            step->alterStateStep( false, false );
        }else if( i < indexStepSelected || isLastStep ){
            step->alterStateStep( false, true );
        }else if( i == indexStepSelected ){
            step->alterStateStep( true, false );
        }
    }
}

QList<QObject*> StepController::stepsToQObject(){
    QList<QObject*> listObject;
    for( StepModel* step : _listSteps ){
        listObject.append(step);
    }
    return listObject;
}

StepModel* StepController::stepByIndex(int index){
    return _listSteps.at(index);
}
