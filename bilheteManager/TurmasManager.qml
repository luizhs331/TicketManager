import QtQuick 2.0
import QtQuick.Controls 2.0

import br.com.turmadto 1.0
import br.com.turmacontroller 1.0

Rectangle {

    id: rectangleMain
    anchors.centerIn: parent

    signal openScreen(var typeScreen)

    onOpenScreen: function (typeScrenn) {

        switch (typeScrenn) {
        case 'CRIAR_TURMA':
            cadastroTurma.open()
            break
        case 'TURMA_UPDATE':
            break
        case 'DELETAR_TURMA':
            break
        }
    }

    CadastroTurma {
        id: cadastroTurma
        width: rectangleMain.width / 1.6
        height: rectangleMain.height / 1.4
    }
}
