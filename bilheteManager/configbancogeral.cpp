#include "configbancogeral.h"

#include <QtSql>

#define CAMINHO_BANCO "/BancoDedados/databasebilhete.db"

void ConfigBancoGeral::dataBaseOpenConexion() {

    QSqlDatabase database = QSqlDatabase::addDatabase( "QSQLITE" );
    QString local = qApp->applicationDirPath();
    local.replace("/debug","");
    database.setDatabaseName( local + CAMINHO_BANCO );

    if( !database.open() ) {
        qCritical() << "Banco Nao Abriu configBancoGeral::dataBaseOpenConexion";
    }
}

void ConfigBancoGeral::dataBaseOpenConexionPSQL() {

    // Analisar Possibilidade de alterar banco de dados para psql

    QSqlDatabase db;

    db=QSqlDatabase::addDatabase( "QPSQL" );
    db.setHostName( "localhost" );
    db.setDatabaseName( "managerdatabase" );
    db.setUserName( "postgres" );
    db.setPassword( "postgres" );

}

