#ifndef BILHETEENTRADAOUSAIDASERVICE_H
#define BILHETEENTRADAOUSAIDASERVICE_H

#include <QObject>

#include "bilheteentradaousaidadto.h"
#include "bilheteentradaousaida/bilheteentradaousaidamodel.h"

typedef BilheteEntradaOuSaidaModel::TipoNotificacaoEnum TipoStatusBilhete;

class BilheteEntradaOuSaidaService {
public:
    void updateBilhete( QObject* dadosBilhete );
    void updateBilhete( BilheteEntradaOuSaidaModel* dadosBilhete );
    void gerarArquivoPdf( const QString& retorno, const QString& nomePdf ) const;

    bool insertBilheteEntradaOuSaida( BilheteEntradaOuSaidaModel* dadosBilhete );
    bool hasBilheteWithId( const QString& id );
    bool excluirBilhetePorId( const QString& id );
    bool reenviarTicket( QString idBilhete );

    int selectUltimoIdBilhete();
    int sendTicket( BilheteEntradaOuSaidaModel* dadosBilhete );

    static BilheteEntradaOuSaidaModel* selectBilheteById( const QString& id );

    BilheteEntradaOuSaidaModel* todoDtoToBilheteEntradaOuSaidaModel( const BilheteEntradaOuSaidaDto* dto ) const;
    BilheteEntradaOuSaidaDto* modelToDto( const BilheteEntradaOuSaidaModel* model ) const;
    QList<BilheteEntradaOuSaidaDto*> listModelToListDto( const QList<BilheteEntradaOuSaidaModel*> model ) const;

    TipoStatusBilhete validarBilheteEnviado( int idBilhete, int statusCode );

    QList<BilheteEntradaOuSaidaModel*> selectTodosOsBilhetes() const;
    QList<BilheteEntradaOuSaidaModel*> selectPeloNomeETurma( const QString& nome, const QString& turma );
    QList<BilheteEntradaOuSaidaModel*> buscarInformacoesBilhetes( const QString& nome, const QString& turma );

};

#endif // BILHETEENTRADAOUSAIDASERVICE_H
