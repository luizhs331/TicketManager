#include "bilheteentradaousaidaservice.h"

#include <QTimer>
#include <QDebug>
#include <memory> // need this for unique_ptr

#include "bilheteentradaousaidadto.h"
#include "bilheteformatar.h"
#include "salvartxt.h"
#include "salvartextstring.h"
#include "cabecalhobilhete.h"
#include "centrobilhete.h"
#include "gerararquivopdf.h"
#include "bilheteentradaousaida/bilheteentradaousaidarepository.h"
#include "bilheteentradaousaida/bilheteentradaousaidaendpoint.h"

#include <configcaixageral/configcaixageralservice.h>
#include <notificacao/notificacoesservice.h>

void BilheteEntradaOuSaidaService::gerarArquivoPdf( const QString& retorno, const QString& nomePdf ) const {
    qInfo() << "BilheteEntradaOuSaidaService::gerarArquivoPdf [RETORNO]" << retorno << "[NOME_PDF]" << nomePdf;
    GerarArquivoPdf().gerarArquivoPdf( retorno, nomePdf );
    qInfo() << "BilheteEntradaOuSaidaService::gerarArquivoPdf";
}

bool BilheteEntradaOuSaidaService::insertBilheteEntradaOuSaida( BilheteEntradaOuSaidaModel* dadosBilhete ) {
    bool success;
    qInfo() << "BilheteEntradaOuSaidaService::insertBilheteEntradaOuSaida";
    success = BilheteEntraDaOuSaidaRepository().insertBilheteEntradaOuSaida( dadosBilhete );
    qInfo() << "BilheteEntradaOuSaidaService::insertBilheteEntradaOuSaida [RETURN]";
    return success;
}

bool BilheteEntradaOuSaidaService::excluirBilhetePorId( const QString& id ) {
    bool success;
    qInfo() << "BilheteEntradaOuSaidaService::excluirBilhetePorId [ID_BILHETE]" << id;
    NotificacoesService().deleteOneNotificationByIdBilhete( id );
    success = BilheteEntraDaOuSaidaRepository().excluirBilhetePorId( id );
    qInfo() << "BilheteEntradaOuSaidaService::excluirBilhetePorId[RETURN]" << success;
    return success;
}

QList<BilheteEntradaOuSaidaModel*> BilheteEntradaOuSaidaService::selectPeloNomeETurma( const QString& nome, const QString& turma ) {
    qInfo() << "BilheteEntradaOuSaidaService::selectPeloNomeETurma [NAME]" << nome << "[CLASS]" << turma;

    QList<BilheteEntradaOuSaidaModel*> listDto;

    if( !turma.trimmed().isEmpty() && !nome.trimmed().isEmpty() ) {
        listDto = BilheteEntraDaOuSaidaRepository().selectForNameStudentAndClass( nome, turma );
    }else if( !nome.isEmpty() && turma.trimmed().isEmpty() ) {
        listDto = BilheteEntraDaOuSaidaRepository().selectForNameStudent( nome );
    }else{
        listDto = BilheteEntraDaOuSaidaRepository().selectForClass( turma );
    }

    qInfo() << "BilheteEntradaOuSaidaService::selectPeloNomeETurma";

    return listDto;
}

BilheteEntradaOuSaidaModel* BilheteEntradaOuSaidaService::selectBilheteById( const QString& id ) {
    return BilheteEntraDaOuSaidaRepository().buscarBilheteById( id );
}

BilheteEntradaOuSaidaModel* BilheteEntradaOuSaidaService::todoDtoToBilheteEntradaOuSaidaModel( const BilheteEntradaOuSaidaDto* dto ) const {

    //FIXME tentado utilizar o reinterpret_cast, porem causa erro , dadosBilheteSave = reinterpret_cast<BilheteEntradaOuSaidaModel*>( dto );

    BilheteEntradaOuSaidaModel* dadosBilheteSave = new BilheteEntradaOuSaidaModel();

    dadosBilheteSave->setNome( dto->nome() );
    dadosBilheteSave->setMotivo( dto->motivo() );
    dadosBilheteSave->setAssinatura( dto->assinatura() );
    dadosBilheteSave->setDescricao( dto->descricao() );
    dadosBilheteSave->setTurma( dto->turma() );
    dadosBilheteSave->setTurno( dto->turno() );
    dadosBilheteSave->setTipoBilhete( dto->tipoBilhete() );
    dadosBilheteSave->setIdBilhete( dto->idBilhete() );
    dadosBilheteSave->setData( dto->data() );
    dadosBilheteSave->setNomeEscola( dto->nomeEscola() );
    dadosBilheteSave->setHora( dto->hora() );

    return dadosBilheteSave;
}

BilheteEntradaOuSaidaDto* BilheteEntradaOuSaidaService::modelToDto( const BilheteEntradaOuSaidaModel* model ) const {

    BilheteEntradaOuSaidaDto* dadosBilheteSave = new BilheteEntradaOuSaidaDto();

    dadosBilheteSave->setNome( model->nome() );
    dadosBilheteSave->setMotivo( model->motivo() );
    dadosBilheteSave->setAssinatura( model->assinatura() );
    dadosBilheteSave->setDescricao( model->descricao() );
    dadosBilheteSave->setTurma( model->turma() );
    dadosBilheteSave->setTurno( model->turno() );
    dadosBilheteSave->setTipoBilhete( model->tipoBilhete() );
    dadosBilheteSave->setIdBilhete( model->idBilhete() );
    dadosBilheteSave->setData( model->data() );
    dadosBilheteSave->setNomeEscola( model->nomeEscola() );
    dadosBilheteSave->setHora( model->hora() );

    return dadosBilheteSave;
}

QList<BilheteEntradaOuSaidaDto*> BilheteEntradaOuSaidaService::listModelToListDto( const QList<BilheteEntradaOuSaidaModel*> model ) const {
    QList<BilheteEntradaOuSaidaDto*> listDto;
    for( BilheteEntradaOuSaidaModel* modelCurrent : model ) {
        listDto.append( modelToDto( modelCurrent ) );
    }
    return listDto;
}

TipoStatusBilhete BilheteEntradaOuSaidaService::validarBilheteEnviado( int idBilhete, int statusCode ) {

    TipoStatusBilhete tpStatusEnvioBilhete = TipoStatusBilhete::SUCCESS;

    NotificacoesModel* notificacao = new NotificacoesModel();

    if( ConfigCaixaGeralService().selectDadosIntegracao() ) {

        notificacao->setIdBilhete( idBilhete );

        if( statusCode == 200 ) {

            notificacao->setTituloMensagem( "Success" );
            notificacao->setDsMensagem( "Sucesso ao Criar Bilhete!" );
            notificacao->setTipoEnum( TipoNotificacaoEnum::SUCCESS );

            NotificacoesService().insertNotificacao( notificacao );

            tpStatusEnvioBilhete = TipoStatusBilhete::SUCCESS;

        }else{
            notificacao->setTituloMensagem( "Warning" );
            notificacao->setDsMensagem( "Bilhete nao Enviado!" );
            notificacao->setTipoEnum( TipoNotificacaoEnum::VERIFICAR );

            NotificacoesService().insertNotificacao( notificacao );

            tpStatusEnvioBilhete = TipoStatusBilhete::VERIFICAR;
        }
    }

    delete( notificacao );

    return tpStatusEnvioBilhete;

}

bool BilheteEntradaOuSaidaService::hasBilheteWithId( const QString& id ) {
    qInfo() << "BilheteEntradaOuSaidaService::hasBilheteWithId";

    bool success = false;

    BilheteEntradaOuSaidaModel* bilhete = BilheteEntraDaOuSaidaRepository().buscarBilheteById( id );

    if( bilhete && bilhete->idBilhete() > 0 ) {
        success = bilhete->idBilhete() == id.toInt();
    }

    delete( bilhete );
    qInfo() << "BilheteEntradaOuSaidaService::hasBilheteWithId [RETURN]" << success;
    return success;
}

QList<BilheteEntradaOuSaidaModel*> BilheteEntradaOuSaidaService::selectTodosOsBilhetes() const {
    return BilheteEntraDaOuSaidaRepository().selectTodosBilhetes();
}

int BilheteEntradaOuSaidaService::selectUltimoIdBilhete() {
    return BilheteEntraDaOuSaidaRepository().selectUltimoIdBanco();
}

void BilheteEntradaOuSaidaService::updateBilhete( BilheteEntradaOuSaidaModel* dadosBilhete ) {
    return BilheteEntraDaOuSaidaRepository().updateBilhete( dadosBilhete );
}

int BilheteEntradaOuSaidaService::sendTicket( BilheteEntradaOuSaidaModel* dadosBilhete ) {
    if( ConfigCaixaGeralService().selectDadosIntegracao() ) {
        return BilheteEntradaOuSaidaEndPoint().post( dadosBilhete );
    }
    return 404;
}

bool BilheteEntradaOuSaidaService::reenviarTicket( QString idBilhete ) {
//    std::unique_ptr<Tododto> dto = std::unique_ptr<Tododto>( BilheteEntradaOuSaidaService().selectBilheteById( idBilhete ) );
    return sendTicket( BilheteEntradaOuSaidaService().selectBilheteById( idBilhete ) ) != 200 ? false : true;;
}

void BilheteEntradaOuSaidaService::updateBilhete( QObject* dadosBilhete ) {
    BilheteEntradaOuSaidaDto* dto = static_cast<BilheteEntradaOuSaidaDto*>( dadosBilhete );
    BilheteEntradaOuSaidaService().updateBilhete( todoDtoToBilheteEntradaOuSaidaModel( dto ) );
    delete dto;
}

QList<BilheteEntradaOuSaidaModel*> BilheteEntradaOuSaidaService::buscarInformacoesBilhetes( const QString& nome, const QString& turma ) {

    if( nome.trimmed().isEmpty() && turma.trimmed().isEmpty() ) {
        return selectTodosOsBilhetes();
    }

    return selectPeloNomeETurma( nome, turma );
}


