#include "bilheteentradaousaidacontroller.h"

#include <QTimer>
#include <QEventLoop>
#include <QMap>

#include "bilheteentradaousaida/bilheteentradaousaidarepository.h"
#include "bilheteentradaousaida/bilheteentradaousaidaendpoint.h"
#include "bilheteentradaousaida/bilheteentradaousaidaservice.h"

#include <configcaixageral/configcaixageralservice.h>
#include <notificacao/notificacoesservice.h>
#include <processo/processocontroller.h>

#include "bilheteentradaousaidadto.h"
#include "bilheteformatar.h"
#include "salvartxt.h"
#include "salvartextstring.h"
#include "cabecalhobilhete.h"
#include "centrobilhete.h"
#include "gerararquivopdf.h"

BilheteEntradaOuSaidaController::BilheteEntradaOuSaidaController( QObject* parent ) :
    QObject( parent ),
    _bilheteDados( new BilheteEntradaOuSaidaModel() )
{}

BilheteEntradaOuSaidaController::~BilheteEntradaOuSaidaController() {
    qInfo() << "BilheteEntradaOuSaidaController::~BilheteEntradaOuSaidaController";
    delete( _bilheteDados );
}

void BilheteEntradaOuSaidaController::centroBilhete( const QString& description, const QString& nome, const QString& assinatura, const QString& data, const QString& turma, const QString& turno ) {

    _bilheteDados->setNome( nome );
    _bilheteDados->setTurma( turma );
    _bilheteDados->setTurno( turno );
    _bilheteDados->setDescricao( description );
    _bilheteDados->setNomePdf( nome );
    _bilheteDados->setAssinatura( assinatura );
    _bilheteDados->setData( data );
    _bilheteDados->setHora( BilheteFormatar::pegarHora() );

    _centro = CentroBilhete().centroBilheteFormatar( description, nome, assinatura, data, turma, turno );
}

void BilheteEntradaOuSaidaController::cabesalhoBilhete( const QString& tipoBilhete, const QString& motivo ) {

    _bilheteDados->setMotivo( motivo );
    _bilheteDados->setTipoBilhete( tipoBilhete );

    _cabesalho = CabecalhoBilhete().cabesalhoBilhete( tipoBilhete, ConfigCaixaGeralService().selectNomeEscola(), motivo );

    footer();
}

void BilheteEntradaOuSaidaController::runAnimation( bool successOrFail ) {
    QTimer timer;
    QEventLoop loop;

    connect( &timer, &QTimer::timeout, this, [&]() {
        pararAnimation();
        setSelectAnimationFailOrSucessPrintar( successOrFail );
        emit loop.exit();
    } );

    // tempo fake para rodar animacao
    timer.start( 3000 );
    loop.exec();
}

void BilheteEntradaOuSaidaController::saveTicket() {

    _bilheteDados->setNomeEscola( ConfigCaixaGeralService().selectNomeEscola() );

    BilheteEntradaOuSaidaService bilheteEntradaOuSaidaService;

    runAnimation( bilheteEntradaOuSaidaService.insertBilheteEntradaOuSaida( _bilheteDados ) );

    int idBilhete = selectUltimoIdBilhete();

    int statusCode = sendTicket( _bilheteDados );

    _bilheteDados->setTpStatusEnvio( validarBilheteEnviado( idBilhete, statusCode ) );

    _bilheteDados->setIdBilhete( idBilhete );

    bilheteEntradaOuSaidaService.updateBilhete( _bilheteDados );
}

bool BilheteEntradaOuSaidaController::hasDtos() {
    return !_dtos.isEmpty();
}

void BilheteEntradaOuSaidaController::pararAnimation() {
    setAnimationRunnigPrintar( false );
    emit animationRunnigChanged();
}

bool BilheteEntradaOuSaidaController::excluirBilhetePorId( const QString& id ) const {
    return BilheteEntradaOuSaidaService().excluirBilhetePorId( id );
}

void BilheteEntradaOuSaidaController::footer() {
    QString formata;
    QStringList formataRetorno;
    formataRetorno.append( "AVISO ESSE BILHETE DEVE SER REGISTRADO" );
    formataRetorno.append( "NAO SEGUIR AVISO RESULTARA EM MULTA" );
    formataRetorno = BilheteFormatar().formatarBilhete( formataRetorno );
    formata = BilheteFormatar().formatarTamnhoBilhete( formataRetorno );
    this->_footer = formata;
}

void BilheteEntradaOuSaidaController::salvarArquivoFormatado() {

    if( !_bilheteDados->nome().isEmpty() && !_bilheteDados->motivo().isEmpty() && !_bilheteDados->assinatura().isEmpty() ) {

        saveTicket();

        QStringList bilheteCompleto;
        bilheteCompleto.append( _centro );
        bilheteCompleto.prepend( _cabesalho );
        bilheteCompleto.append( _footer );
        bilheteCompleto.append( QString( "Hora Da Emissão : " + BilheteFormatar().pegarHora().leftJustified( 45, ' ' ) ) );

        QString retorno = bilheteCompleto.join( "" );
        setArquivoPrintar( retorno );
        emit arquivoPrintarChanged();

        if( _opcaoGerarPdf == 1 ) {
            BilheteEntradaOuSaidaService().gerarArquivoPdf( retorno, _bilheteDados->nomePdf() );
        }

        SalvarTextString().salvarTextoQFile( retorno );

    }

}

void BilheteEntradaOuSaidaController::telaDireita( QList<BilheteEntradaOuSaidaDto*> list ) {

    TodoDtoInformacoesTelaDebusca* todoTelaBusca = new TodoDtoInformacoesTelaDebusca();
    int contEntrada = 0, contSaida = 0, totalBilhetes = 0;
    _dtosTelaBusca.clear();

    for ( BilheteEntradaOuSaidaDto* dto : list ) {
        if( dto->tipoBilhete() == "ENTRADA" ) {
            contEntrada++;
        }else if ( dto->tipoBilhete() == "SAIDA" ) {
            contSaida++;
        }
        todoTelaBusca->setNome( dto->nome() );
        todoTelaBusca->setDataUltimoBilhete( dto->data() );
        todoTelaBusca->setTurmaDoAluno( dto->turma() );
        todoTelaBusca->setTurnoDoAluno( dto->turno() );
        // Implementar metodo e atributo  // todoTelaBusca->setBilhetesSuspenssao();
    }

    totalBilhetes = contEntrada + contSaida;
    todoTelaBusca->setNumeroTotalBilhetes( QString::number( totalBilhetes ) );
    todoTelaBusca->setNumeroBilhetesSaida( QString::number( contSaida ) );
    todoTelaBusca->setNumeroBilhetesEntrada( QString::number( contEntrada ) );
    _dtosTelaBusca.append( todoTelaBusca );
    emit dtosTelaBuscaChanged();

}

int BilheteEntradaOuSaidaController::listarBilhetes( QString nome, QString turma, int numPage ) {
    _dtos.clear();
    _listCurrenteDtos = buscarPorNomeDto( nome, turma );
    return controlePaginacao( numPage );
}

int BilheteEntradaOuSaidaController::controlePaginacao( int numPageSearch ) {

    telaDireita( _listCurrenteDtos );

    int numPageCurrent( 0 );
    bool pageFound( false );

    // TODO implementar algo para deletar as listas de ponteiro
    QList<BilheteEntradaOuSaidaDto*> dto;
    QListIterator<BilheteEntradaOuSaidaDto*> dtoListFromSearch( _listCurrenteDtos );
    QMap<int, QList<BilheteEntradaOuSaidaDto*> > listPages;

    if( numPageSearch == 0 ) {
        _dtos.append( _listCurrenteDtos );
        return numPageCurrent;
    }

    while( dtoListFromSearch.hasNext() ) {

        BilheteEntradaOuSaidaDto* dtoCurrent = dtoListFromSearch.next();

        if( dto.size() < 2 ) {
            dto.append( dtoCurrent );
        }

        if( dto.size() >= 2 || ( !dtoListFromSearch.hasNext() && !dto.isEmpty() ) ) {
            numPageCurrent++;
            listPages.insert( numPageCurrent, dto );
            dto.clear();
        }
    }

    pageFound = listPages.size() > numPageSearch;

    _dtos = pageFound ? listPages[numPageSearch] : listPages[numPageCurrent];

    return numPageCurrent;
}

QList<QObject*> BilheteEntradaOuSaidaController::dtosToObject() const {
    QList<QObject*> dtos = {};

    if( !_dtos.isEmpty() ) {
        for( int i = _dtos.count() - 1; i >=0; i-- ) {
            dtos.append( _dtos.at( i ) );
        }
        return dtos;
    }else {
        for( int i = _dtosTelaBusca.count() - 1; i >=0; i-- ) {
            dtos.append( _dtosTelaBusca.at( i ) );
        }
        return dtos;
    }
}

QObject* BilheteEntradaOuSaidaController::selectBilheteByid( const QString& id ) {
    return BilheteEntradaOuSaidaService().modelToDto( BilheteEntradaOuSaidaService().selectBilheteById( id ) );
}

bool BilheteEntradaOuSaidaController::hasBilheteWithId( const QString& id ) {
    return BilheteEntradaOuSaidaService().hasBilheteWithId( id );
}

void BilheteEntradaOuSaidaController::updateBilhete( QObject* dadosBilhete ) {
    BilheteEntradaOuSaidaService().updateBilhete( dadosBilhete );
}

bool BilheteEntradaOuSaidaController::reenviarTicket( QString idBilhete ) {
    return BilheteEntradaOuSaidaService().reenviarTicket( idBilhete );
}

QString BilheteEntradaOuSaidaController::arquivoPrintar() const { return _arquivoPrintar; }

QList<BilheteEntradaOuSaidaDto*> BilheteEntradaOuSaidaController::buscarPorNomeDto( QString nome, QString turma ) {
    return BilheteEntradaOuSaidaService().listModelToListDto( BilheteEntradaOuSaidaService().buscarInformacoesBilhetes( nome, turma ) );
}

int BilheteEntradaOuSaidaController::sendTicket( BilheteEntradaOuSaidaModel* dadosBilhete ) {
    return BilheteEntradaOuSaidaService().sendTicket( dadosBilhete );
}

bool BilheteEntradaOuSaidaController::selectAnimationFailOrSucessPrintar() {
    return _selectAnimationFailOrSucess;
}

void BilheteEntradaOuSaidaController::setSelectAnimationFailOrSucessPrintar( bool var ) {
    _selectAnimationFailOrSucess = var;
    selectAnimationFailOrSucessChanged();
}

int BilheteEntradaOuSaidaController::selectUltimoIdBilhete() {
    return BilheteEntradaOuSaidaService().selectUltimoIdBilhete();
}

BilheteEntradaOuSaidaModel::TipoNotificacaoEnum BilheteEntradaOuSaidaController::validarBilheteEnviado( int idBilhete, int statusCode ) {
    return BilheteEntradaOuSaidaService().validarBilheteEnviado( idBilhete, statusCode );
}

bool BilheteEntradaOuSaidaController::animationRunnigPrintar() { return _runnigAnimation; }
void BilheteEntradaOuSaidaController::setAnimationRunnigPrintar( bool var ) { _runnigAnimation = var; }

int BilheteEntradaOuSaidaController::getOpcaoGerarPdf() const { return _opcaoGerarPdf; }
void BilheteEntradaOuSaidaController::setOpcaoGerarPdf( int opcaoGerarPdf ) { _opcaoGerarPdf  = opcaoGerarPdf; }

void BilheteEntradaOuSaidaController::setArquivoPrintar( const QString& arquivoPrintar ) { _arquivoPrintar = arquivoPrintar; }

QList<TodoDtoInformacoesTelaDebusca*> BilheteEntradaOuSaidaController::getDtosTelaBusca() const { return _dtosTelaBusca; }
void BilheteEntradaOuSaidaController::setDtosTelaBusca( const QList<TodoDtoInformacoesTelaDebusca*>& dtosTelaBusca ) { _dtosTelaBusca = dtosTelaBusca; }



