#ifndef BILHETEENTRADAOUSAIDAENDPOINT_H
#define BILHETEENTRADAOUSAIDAENDPOINT_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QJsonDocument>

#include <bilheteentradaousaida/bilheteentradaousaidamodel.h>

class BilheteEntradaOuSaidaEndPoint : public QObject {
    Q_OBJECT
public:
    explicit BilheteEntradaOuSaidaEndPoint( QObject* parent = nullptr );
    int post( BilheteEntradaOuSaidaModel* dadosBilhete, const int& timeout = 5000 );
    void postSavarCadastroUsuario( /*QByteArray qbyteArray*/ );
    QByteArray fromJson( BilheteEntradaOuSaidaModel* dadosBilhete );
    void replyFinished( QNetworkReply* resp );
    int requestStatus( int tempo );

private:
    QNetworkAccessManager* mManager;
    QNetworkReply* reply;
};

#endif // BILHETEENTRADAOUSAIDAENDPOINT_H
