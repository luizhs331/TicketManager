#include "bilheteentradaousaidaendpoint.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QCoreApplication>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonObject>

#include <QTimer>

#define ENDPOINT_POST "http://localhost:3333/bilhetes"


BilheteEntradaOuSaidaEndPoint::BilheteEntradaOuSaidaEndPoint( QObject* parent ) :
    QObject( parent ) {
    mManager = new QNetworkAccessManager();
}

int BilheteEntradaOuSaidaEndPoint::post( BilheteEntradaOuSaidaModel* dadosBilhete, const int& timeout ) {

    QNetworkRequest request = QNetworkRequest( QUrl( ENDPOINT_POST ) );
    request.setHeader( QNetworkRequest::ContentTypeHeader, "application/json" );
    reply = mManager->post( request, fromJson( dadosBilhete ) );
    return requestStatus( timeout );

}

void BilheteEntradaOuSaidaEndPoint::postSavarCadastroUsuario( /*QByteArray qbyteArray*/ ) {

    QNetworkRequest request = QNetworkRequest( QUrl( ENDPOINT_POST ) );
    request.setHeader( QNetworkRequest::ContentTypeHeader, "application/json" );
    QUrl url =  QUrl( "http://localhost:3334" );
    request.setUrl( url );
}


QByteArray BilheteEntradaOuSaidaEndPoint::fromJson( BilheteEntradaOuSaidaModel* dadosBilhete ) {

    QJsonObject response;
    response[ BilheteEntradaOuSaidaModel::NOME ] = dadosBilhete->nome();
    response[ BilheteEntradaOuSaidaModel::DATA ] = dadosBilhete->data();
    response[ BilheteEntradaOuSaidaModel::HORA ] = dadosBilhete->hora();
    response[ BilheteEntradaOuSaidaModel::TIPO_BILHETE ] = dadosBilhete->tipoBilhete();
    response[ BilheteEntradaOuSaidaModel::MOTIVO ] = dadosBilhete->motivo();

    QJsonDocument doc( response );

    return doc.toJson();
}

void BilheteEntradaOuSaidaEndPoint::replyFinished( QNetworkReply* resp ) {
    QVariant status_code = resp->attribute( QNetworkRequest::HttpStatusCodeAttribute );
    QString status = status_code.toString();
    qDebug() << status;
}

int BilheteEntradaOuSaidaEndPoint::requestStatus( int tempo ) {

    QTimer timer;
    timer.setSingleShot( true );

    QEventLoop loop;
    QObject::connect( &timer, &QTimer::timeout, &loop, &QEventLoop::quit );
    QObject::connect( reply, &QNetworkReply::finished, &loop, &QEventLoop::quit );
    timer.start( tempo );
    loop.exec();

    if( timer.isActive() ) {
        timer.stop();
        if( reply->error() > 0 ) {
            qDebug() << "Erro Request";
            return 404;
        }else {
            int v = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();

            if ( v >= 200 && v < 300 ) { // Success
                qDebug() << "Sucess";
                return 200;
            }
        }
    } else {
        // timeout
        QObject::disconnect( reply, &QNetworkReply::finished, &loop, &QEventLoop::quit );
        reply->abort();
        return 599;
    }
    return 404;
}
