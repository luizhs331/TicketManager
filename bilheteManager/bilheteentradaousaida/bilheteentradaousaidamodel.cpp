#include "bilheteentradaousaidamodel.h"

QString BilheteEntradaOuSaidaModel::nomePdf() const {
    return _nomePdf;
}

void BilheteEntradaOuSaidaModel::setNomePdf( const QString& nomePdf ) {
    _nomePdf = nomePdf;
}

QString BilheteEntradaOuSaidaModel::descricao() const {
    return _descricao;
}

void BilheteEntradaOuSaidaModel::setDescricao( const QString& descricao ) {
    _descricao = descricao;
}

QString BilheteEntradaOuSaidaModel::motivo() const {
    return _motivo;
}

void BilheteEntradaOuSaidaModel::setMotivo( const QString& motivo ) {
    _motivo = motivo;
}

QString BilheteEntradaOuSaidaModel::assinatura() const {
    return _assinatura;
}

void BilheteEntradaOuSaidaModel::setAssinatura( const QString& assinatura ) {
    _assinatura = assinatura;
}

QString BilheteEntradaOuSaidaModel::nome() const {
    return _nome;
}

void BilheteEntradaOuSaidaModel::setNome( const QString& nome ) {
    _nome = nome;
}

QString BilheteEntradaOuSaidaModel::data() const {
    return _data;
}

void BilheteEntradaOuSaidaModel::setData( const QString& data ) {
    _data = data;
}

QString BilheteEntradaOuSaidaModel::hora() const {
    return _hora;
}

void BilheteEntradaOuSaidaModel::setHora( const QString& hora ) {
    _hora = hora;
}

QString BilheteEntradaOuSaidaModel::tipoBilhete() const {
    return _tipoBilhete;
}

void BilheteEntradaOuSaidaModel::setTipoBilhete( const QString& tipoBilhete ) {
    _tipoBilhete = tipoBilhete;
}

QString BilheteEntradaOuSaidaModel::turma() const {
    return _turma;
}

void BilheteEntradaOuSaidaModel::setTurma( const QString& turma ) {
    _turma = turma;
}

QString BilheteEntradaOuSaidaModel::turno() const {
    return _turno;
}

void BilheteEntradaOuSaidaModel::setTurno( const QString& turno ) {
    _turno = turno;
}

QString BilheteEntradaOuSaidaModel::nomeEscola() const {
    return _nomeEscola;
}

void BilheteEntradaOuSaidaModel::setNomeEscola( const QString& nomeEscola ) {
    _nomeEscola = nomeEscola;
}

BilheteEntradaOuSaidaModel::TipoNotificacaoEnum BilheteEntradaOuSaidaModel::tpStatusEnvio() const {
    return _tpStatusEnvio;
}

void BilheteEntradaOuSaidaModel::setTpStatusEnvio( const BilheteEntradaOuSaidaModel::TipoNotificacaoEnum& tpStatusEnvio ) {
    _tpStatusEnvio = tpStatusEnvio;
}

int BilheteEntradaOuSaidaModel::idBilhete() const {
    return _idBilhete;
}

void BilheteEntradaOuSaidaModel::setIdBilhete( int idBilhete ) {
    _idBilhete = idBilhete;
}



