#ifndef BILHETEENTRADAOUSAIDA_H
#define BILHETEENTRADAOUSAIDA_H

#include <QObject>
#include "bilheteentradaousaidadto.h"
#include "tododtoinformacoesteladebusca.h"

#include <bilheteentradaousaida/bilheteentradaousaidamodel.h>
#include <notificacao/notificacoesmodel.h>

class QNetworkAccessManager;

class BilheteEntradaOuSaidaController : public QObject {
    Q_OBJECT
    Q_PROPERTY( QList<QObject*> dtosTelaBusca READ dtosToObject NOTIFY dtosTelaBuscaChanged )
    Q_PROPERTY( QList<QObject*> dtos READ dtosToObject NOTIFY dtosChanged )
    Q_PROPERTY( QString printar READ arquivoPrintar NOTIFY arquivoPrintarChanged )
    Q_PROPERTY( bool selectAnimationFailOrSucess READ selectAnimationFailOrSucessPrintar WRITE setSelectAnimationFailOrSucessPrintar NOTIFY selectAnimationFailOrSucessChanged )
    Q_PROPERTY( bool animationRunnig READ animationRunnigPrintar WRITE setAnimationRunnigPrintar NOTIFY animationRunnigChanged )
    Q_PROPERTY( int selectUltimoIdBilhete READ selectUltimoIdBilhete )

public:

    explicit BilheteEntradaOuSaidaController( QObject* parent = nullptr );
    ~BilheteEntradaOuSaidaController();

    int selectUltimoIdBilhete();

    BilheteEntradaOuSaidaModel::TipoNotificacaoEnum validarBilheteEnviado( int idBilhete, int statusCode );

    int sendTicket( BilheteEntradaOuSaidaModel* dadosBilhete );

    bool selectAnimationFailOrSucessPrintar();
    void setSelectAnimationFailOrSucessPrintar( bool var );

    bool animationRunnigPrintar();
    void setAnimationRunnigPrintar( bool var );

    void telaDireita( QList<BilheteEntradaOuSaidaDto*> list );

    QList <QObject*> dtosToObject() const;

    void setArquivoPrintar( const QString& arquivoPrintar );

    QStringList getListDados() const;
    void setListDados( const QStringList& listDados );

    int getOpcaoGerarPdf() const;

    QList<TodoDtoInformacoesTelaDebusca*> getDtosTelaBusca() const;
    void setDtosTelaBusca( const QList<TodoDtoInformacoesTelaDebusca*>& dtosTelaBusca );

    void runAnimation( bool success );

signals:
    void dtosChanged();
    void arquivoPrintarChanged();
    void mudarDeTela();
    void dtosTelaBuscaChanged();
    void selectAnimationFailOrSucessChanged();
    void animationRunnigChanged();
    void signalLiberarProcesso();

public slots:

    QObject* selectBilheteByid( const QString& id );

    void pararAnimation();
    void footer();
    void salvarArquivoFormatado();
    void saveTicket();
    void setOpcaoGerarPdf( int opcaoGerarPdf );
    void updateBilhete( QObject* dadosBilhete );
    void cabesalhoBilhete( const QString& tipoBilhete, const QString& motivo );
    void centroBilhete( const QString& description, const QString& nome, const QString& assinatura, const QString& data, const QString& turma, const QString& turno );

    int listarBilhetes( QString nome, QString turma, int numPage = 0 );
    int controlePaginacao( int numPageSearch = 0 );

    bool hasDtos();
    bool reenviarTicket( QString idBilhete );
    bool hasBilheteWithId( const QString& id );
    bool excluirBilhetePorId( const QString& id ) const;

    QString arquivoPrintar() const;

    QList<BilheteEntradaOuSaidaDto*> buscarPorNomeDto( QString nome, QString turma );

private:

    BilheteEntradaOuSaidaModel* _bilheteDados;

    int _opcaoGerarPdf;

    bool _selectAnimationFailOrSucess = true;
    bool _runnigAnimation = true;

    QString _centro;
    QString _cabesalho;
    QString _footer;
    QString _arquivoPrintar;

    QList<TodoDtoInformacoesTelaDebusca*> _dtosTelaBusca = {};
    QList<BilheteEntradaOuSaidaDto*> _dtos = {};
    QList<BilheteEntradaOuSaidaDto*> _listCurrenteDtos = {};
};

#endif // BILHETEENTRADAOUSAIDA_H
