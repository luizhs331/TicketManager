#ifndef BILHETEENTRADAOUSAIDAMODEL_H
#define BILHETEENTRADAOUSAIDAMODEL_H

#include <QObject>

class BilheteEntradaOuSaidaModel {

public:

    enum TipoNotificacaoEnum : char {
        VERIFICAR = 'V',
        FAIL = 'F',
        SUCCESS = 'S'
    };

    QString arquivoPrintar() const;
    void setArquivoPrintar( const QString& arquivoPrintar );

    QString nomePdf() const;
    void setNomePdf( const QString& nomePdf );

    QString descricao() const;
    void setDescricao( const QString& descricao );

    QString motivo() const;
    void setMotivo( const QString& motivo );

    QString assinatura() const;
    void setAssinatura( const QString& assinatura );

    QString nome() const;
    void setNome( const QString& nome );

    QString data() const;
    void setData( const QString& data );

    QString hora() const;
    void setHora( const QString& hora );

    QString tipoBilhete() const;
    void setTipoBilhete( const QString& tipoBilhete );

    QString turma() const;
    void setTurma( const QString& turma );

    QString turno() const;
    void setTurno( const QString& turno );

    QString nomeEscola() const;
    void setNomeEscola( const QString& nomeEscola );

    TipoNotificacaoEnum tpStatusEnvio() const;
    void setTpStatusEnvio( const TipoNotificacaoEnum& tpStatusEnvio );

    int idBilhete() const;
    void setIdBilhete( int idBilhete );

    static constexpr const char* NOME = "nome";
    static constexpr const char* TIPO_BILHETE = "tipoBilhete";
    static constexpr const char* DATA = "data";
    static constexpr const char* HORA = "hora";
    static constexpr const char* MOTIVO = "motivo";

private:

    int _idBilhete = 0;

    QString _nomePdf = "";
    QString _descricao = "";
    QString _motivo = "";
    QString _assinatura = "";
    QString _nome = "";
    QString _data = "";
    QString _hora = "";
    QString _tipoBilhete = "";
    QString _turma = "";
    QString _turno = "";
    QString _nomeEscola = "";
    BilheteEntradaOuSaidaModel::TipoNotificacaoEnum _tpStatusEnvio = BilheteEntradaOuSaidaModel::TipoNotificacaoEnum::VERIFICAR;
};


#endif // BILHETEENTRADAOUSAIDAMODEL_H
