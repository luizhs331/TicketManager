#include "bilheteentradaousaidarepository.h"

#include <QtSql>
#include <QFileInfo>

#include <bilheteentradaousaidadto.h>
#include <sqls.h>

#define CAMINHO_BANCO "/BancoDedados/databasebilhete.db"
#define SELECT_NOME_TURMA "SELECT ds_nome_turma FROM turmas_cadastradas WHERE ds_nome_turma /*IS NOT NULL;*/"
#define SELECT_TODOS_OS_BILHETES "select ds_nome_aluno, ds_nome_escola , tp_bilhete , ds_descricao_bilhete , ds_assinatura_pedagogo , dt_data_emissao , dh_hora_emissao  , ds_motivo , ds_turma , ds_turno_aluno, id_bilhete from bilhetes_entrada_saida"

bool BilheteEntraDaOuSaidaRepository::excluirBilhetePorId( const QString& id ) const {
    QSqlQuery query;
    query.prepare( "delete from bilhetes_entrada_saida where id_bilhete = '" + id + "' " );
    return query.exec();
}

BilheteEntradaOuSaidaModel* BilheteEntraDaOuSaidaRepository::buscarBilheteById( const QString& id ) const {

    QSqlQuery query;

    query.prepare( "select ds_nome_aluno, ds_nome_escola , tp_bilhete , ds_descricao_bilhete , ds_assinatura_pedagogo , dt_data_emissao , dh_hora_emissao  , ds_motivo , ds_turma , ds_turno_aluno, id_bilhete from bilhetes_entrada_saida where id_bilhete ='" + id + "'" );

    QList<BilheteEntradaOuSaidaModel*> listBilhetes = selectRowGeneric( query );

    return !listBilhetes.isEmpty() ? listBilhetes.first() : new BilheteEntradaOuSaidaModel();

}

bool BilheteEntraDaOuSaidaRepository::insertBilheteEntradaOuSaida( BilheteEntradaOuSaidaModel* dadosBilhete ) const {

    QSqlQuery query;

    query.prepare( Sqls::createQuery( INSERT, "selectsqls/bilheteentradaousaida/" ) );

    query.bindValue( ":NOMEALUNO", dadosBilhete->nome() );
    query.bindValue( ":NOMEESCOLA", dadosBilhete->nomeEscola() );
    query.bindValue( ":TPBILHETE", dadosBilhete->tipoBilhete() );
    query.bindValue( ":DESCRICAOBILHETE", dadosBilhete->descricao() );
    query.bindValue( ":ASSINATURAPEDAGOGO", dadosBilhete->assinatura() );
    query.bindValue( ":DATAEMISSAO", dadosBilhete->data() );
    query.bindValue( ":HORAEMISSAO", dadosBilhete->hora() );
    query.bindValue( ":MOTIVO", dadosBilhete->motivo() );
    query.bindValue( ":TURMA", dadosBilhete->turma() );
    query.bindValue( ":TURNOALUNO", dadosBilhete->turno() );

    return query.exec();
}

void BilheteEntraDaOuSaidaRepository::updateBilhete( BilheteEntradaOuSaidaModel* dadosBilhete ) const {

    QSqlQuery query;

    query.prepare( "UPDATE bilhetes_entrada_saida SET ds_nome_aluno = :ds_nome_aluno, ds_nome_escola = :ds_nome_escola, tp_bilhete = :tp_bilhete, ds_descricao_bilhete = :ds_descricao_bilhete, ds_assinatura_pedagogo = :ds_assinatura_pedagogo, dt_data_emissao = :dt_data_emissao, dh_hora_emissao = :dh_hora_emissao, ds_motivo = :ds_motivo, ds_turma = :ds_turma, ds_turno_aluno = :ds_turno_aluno, tp_status = :tp_status WHERE id_bilhete = :id_bilhete;" );

    char castEnum = static_cast<char>( dadosBilhete->tpStatusEnvio() );

    query.bindValue( ":ds_nome_aluno", dadosBilhete->nome() );
    query.bindValue( ":ds_nome_escola", dadosBilhete->nomeEscola() );
    query.bindValue( ":tp_bilhete", dadosBilhete->tipoBilhete() );
    query.bindValue( ":ds_descricao_bilhete", dadosBilhete->descricao() );
    query.bindValue( ":ds_assinatura_pedagogo", dadosBilhete->assinatura() );
    query.bindValue( ":dt_data_emissao", dadosBilhete->data() );
    query.bindValue( ":dh_hora_emissao", dadosBilhete->hora() );
    query.bindValue( ":ds_motivo", dadosBilhete->motivo() );
    query.bindValue( ":ds_turma", dadosBilhete->turma() );
    query.bindValue( ":ds_turno_aluno", dadosBilhete->turno() );
    query.bindValue( ":tp_status", QString().append( castEnum ) );

    query.bindValue( ":id_bilhete", dadosBilhete->idBilhete() );

    if( query.exec() ) {
        qDebug() << " BilheteEntraDaOuSaidaRepository::updateBilhete Sucess ";
    }

}

int BilheteEntraDaOuSaidaRepository::selectUltimoIdBanco() {

    QSqlQuery query;
    query.prepare( Sqls::createQuery( Sqls::V1_SELECT_ULTIMO_ID, Sqls::CONST_DIRECTORY_SELECTS ) );

    if( query.exec() ) {
        query.value( 1 );
    }

    qDebug() << query.value( query.next() );
    return query.value( "id_bilhete" ).toInt();
}

QList<BilheteEntradaOuSaidaModel*> BilheteEntraDaOuSaidaRepository::selectForNameStudentAndClass( const QString& nome, const QString& turma ) const {
    QSqlQuery query;
    query.prepare( "select ds_nome_aluno, ds_nome_escola , tp_bilhete , ds_descricao_bilhete , ds_assinatura_pedagogo , dt_data_emissao , dh_hora_emissao  , ds_motivo , ds_turma , ds_turno_aluno, id_bilhete from bilhetes_entrada_saida where ds_nome_aluno like '" + nome + "' and ds_turma like '" + turma + "' " );
    return selectRowGeneric( query );
}

QList<BilheteEntradaOuSaidaModel*> BilheteEntraDaOuSaidaRepository::selectForNameStudent( const QString& nome ) const {
    QSqlQuery query;
    query.prepare( "select ds_nome_aluno, ds_nome_escola , tp_bilhete , ds_descricao_bilhete , ds_assinatura_pedagogo , dt_data_emissao , dh_hora_emissao  , ds_motivo , ds_turma , ds_turno_aluno, id_bilhete from bilhetes_entrada_saida where ds_nome_aluno like '" + nome + "'" );
    return selectRowGeneric( query );
}

QList<BilheteEntradaOuSaidaModel*> BilheteEntraDaOuSaidaRepository::selectForClass( const QString& turma ) const {
    QSqlQuery query;
    query.prepare( "select ds_nome_aluno, ds_nome_escola , tp_bilhete , ds_descricao_bilhete , ds_assinatura_pedagogo , dt_data_emissao , dh_hora_emissao  , ds_motivo , ds_turma , ds_turno_aluno, id_bilhete from bilhetes_entrada_saida where ds_turma like '" + turma + "'" );
    return selectRowGeneric( query );
}

QList<BilheteEntradaOuSaidaModel*> BilheteEntraDaOuSaidaRepository::selectRowGeneric( QSqlQuery query ) const {

    QList<BilheteEntradaOuSaidaModel*> listDto = {};

    if( query.exec() ) {
        qDebug() << "Select Sucess";
        while( query.next() ) {
            BilheteEntradaOuSaidaModel* dto = new BilheteEntradaOuSaidaModel();
            dto->setTipoBilhete( query.value( BilheteEntraDaOuSaidaRepository::TP_BILHETE ).toString() );
            dto->setTurma( query.value( BilheteEntraDaOuSaidaRepository::DS_TURMA ).toString() );
            dto->setNome( query.value( BilheteEntraDaOuSaidaRepository::DS_NOME_ALUNO ).toString() );
            dto->setTurno( query.value( BilheteEntraDaOuSaidaRepository::DS_TURNO ).toString() );
            dto->setMotivo( query.value( BilheteEntraDaOuSaidaRepository::DS_MOTIVO ).toString() );
            dto->setDescricao( query.value( BilheteEntraDaOuSaidaRepository::DS_DESCRICAO_BILHETE ).toString() );
            dto->setAssinatura( query.value( BilheteEntraDaOuSaidaRepository::DS_ASSINATURA_PEDAGOGO ).toString() );
            dto->setHora( query.value( BilheteEntraDaOuSaidaRepository::DH_HORA_EMISSAO ).toString() );
            dto->setData( query.value( BilheteEntraDaOuSaidaRepository::DT_EMISSAO ).toString() );
            dto->setIdBilhete( query.value( BilheteEntraDaOuSaidaRepository::ID_BILHETE ).toInt() );
            dto->setNomeEscola( query.value( BilheteEntraDaOuSaidaRepository::DS_NOME_ESCOLA ).toString() );
            listDto.append( dto );
        }
    }else {
        qCritical() << "BilheteEntraDaOuSaidaRepository::selectRowGeneric Error";
    }

    return listDto;
}

QList<BilheteEntradaOuSaidaModel*> BilheteEntraDaOuSaidaRepository::selectTodosBilhetes() const {

    QSqlQuery query;

    query.prepare( SELECT_TODOS_OS_BILHETES );

    return selectRowGeneric( query );

}


