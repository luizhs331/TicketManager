#ifndef BILHETEENTRADAOUSAIDAREPOSITORY_H
#define BILHETEENTRADAOUSAIDAREPOSITORY_H

#include <QObject>
#include <QSqlQuery>

#include <bilheteentradaousaida/bilheteentradaousaidamodel.h>
#include "bilheteentradaousaidadto.h"

class BilheteEntraDaOuSaidaRepository : public QObject {
    Q_OBJECT
public:
    BilheteEntradaOuSaidaModel* buscarBilheteById( const QString& id ) const;

    bool excluirBilhetePorId( const QString& id ) const;
    bool insertBilheteEntradaOuSaida( BilheteEntradaOuSaidaModel* dadosBilhete ) const;

    void updateBilhete( BilheteEntradaOuSaidaModel* dadosBilhete ) const;

    static int selectUltimoIdBanco();

    QList<BilheteEntradaOuSaidaModel*> selectForNameStudentAndClass( const QString& nome, const QString& turma ) const;
    QList<BilheteEntradaOuSaidaModel*> selectForNameStudent( const QString& nome ) const;
    QList<BilheteEntradaOuSaidaModel*> selectForClass( const QString& turma ) const;
    QList<BilheteEntradaOuSaidaModel*> selectRowGeneric( QSqlQuery query ) const;

    QList<BilheteEntradaOuSaidaModel*> selectTodosBilhetes() const;

    static constexpr const char* DS_NOME_ALUNO = "ds_nome_aluno";
    static constexpr const char* DS_NOME_ESCOLA = "ds_nome_escola";
    static constexpr const char* TP_BILHETE = "tp_bilhete";
    static constexpr const char* DS_DESCRICAO_BILHETE = "ds_descricao_bilhete";
    static constexpr const char* DS_ASSINATURA_PEDAGOGO = "ds_assinatura_pedagogo";
    static constexpr const char* DT_EMISSAO = "dt_data_emissao";
    static constexpr const char* DH_HORA_EMISSAO = "dh_hora_emissao";
    static constexpr const char* DS_MOTIVO = "ds_motivo";
    static constexpr const char* DS_TURMA = "ds_turma";
    static constexpr const char* DS_TURNO = "ds_turno_aluno";
    static constexpr const char* DS_TURMA_ALUNO = "ds_nome_turma";
    static constexpr const char* ID_BILHETE = "id_bilhete";
    static constexpr const char* TP_STATUS_ENVIO = "tp_status";

    static constexpr const char* INSERT = "INSERT_INTO_BILHETE_ENTRADA_OU_SAIDA.txt";


};

#endif // BILHETEENTRADAOUSAIDAREPOSITORY_H
