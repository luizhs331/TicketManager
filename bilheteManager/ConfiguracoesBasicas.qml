import QtQuick 2.6
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import br.com.configgeral 1.0

import "qml/components"

Item {
    id: primeraTab

    ConfigGeral {
        id: configGeral
    }

    property string retornoNumMaximoCarcter: configGeral.retornoMaximoCaracterLinha()

    signal buttonSavePressed
    signal processoConcluidoStatus(var result)

    onButtonSavePressed: {
        var result = configGeral.salvarDadosConfigGeral(
                    mySwitch.checked, txtMaximoTamanhoLinha.currentText)

        primeraTab.processoConcluidoStatus(result)
    }

    function mudarResulucaoTela(status) {
        mainWindow.validarFullScreenWindow = status
    }

    Rectangle {
        id: segundoRetangulo
        width: primeraTab.width
        height: parent.height
        anchors.centerIn: parent
        color: colorBackground

        Column {
            anchors.centerIn: parent
            width: parent.width - (primeraTab.width / 10)
            height: parent.height - (primeraTab.width / 10)
            spacing: 15

            Label {
                font.pointSize: 18
                color: "#000"
                text: "Configurações basicas"
            }

            TextFieldWithText {
                id: txtMaximoTamanhoLinha
                width: parent.width / 2
                height: primeraTab.height / 11
                titleField: "Quantidade de caracteres por linhas"
                isMandatoryField: false
                textSize: 13
                currentText: retornoNumMaximoCarcter
            }

            Column {

                spacing: 5

                Label {
                    text: "Gerar pdf"
                    font.pixelSize: 16
                    horizontalAlignment: Label.AlignHCenter
                    verticalAlignment: Label.AlignVCenter
                    color: "#6c6c80"
                }

                Switch {
                    id: mySwitch
                    text: mySwitch.position == 1 ? "Sim" : "Não"
                    font.pointSize: 13
                }
            }

            ColumnLayout {
                Column {
                    spacing: 5
                    Label {
                        text: "Modo tela"
                        font.pixelSize: 16
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        color: "#6c6c80"
                    }
                    Row {
                        spacing: 10
                        RadioButton {
                            text: qsTr("FullScreen")
                            onCheckedChanged: primeraTab.mudarResulucaoTela(
                                                  true)
                        }
                        RadioButton {
                            checked: true
                            text: qsTr("Modo Janela")
                            onCheckedChanged: primeraTab.mudarResulucaoTela(
                                                  false)
                        }
                    }
                }
            }
        }
    }
}
