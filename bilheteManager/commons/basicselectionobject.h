#ifndef NOTOTEXTFIELDWITHSEARCHDTO_H
#define NOTOTEXTFIELDWITHSEARCHDTO_H

#include <QObject>

class BasicSelectionObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY( int idOption READ idOption WRITE setIdOption )
    Q_PROPERTY( QString description READ description WRITE setDescription )
public:
    BasicSelectionObject( int idSala, const QString& descricao );

    int idOption() const;
    void setIdOption(int idOption);

    QString description() const;
    void setDescription(const QString &description);

private:
    int _idOption;
    QString _description;
};

#endif // NOTOTEXTFIELDWITHSEARCHDTO_H
