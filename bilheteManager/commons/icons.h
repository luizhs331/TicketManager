#ifndef ICONS_H
#define ICONS_H

class Icons{

  public:
    static constexpr const char* USUARIOS = "img/usuarios.png";
    static constexpr const char* DADOS = "img/caderno.png";
    static constexpr const char* CHECK = "img/check.svg";

};


#endif // ICONS_H
