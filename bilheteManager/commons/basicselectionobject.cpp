#include "basicselectionobject.h"

BasicSelectionObject::BasicSelectionObject( int idSala, const QString& descricao ) :
    _idOption( idSala ),
    _description( descricao ){}


int BasicSelectionObject::idOption() const{
    return _idOption;
}

void BasicSelectionObject::setIdOption(int idOption){
    _idOption = idOption;
}

QString BasicSelectionObject::description() const{
    return _description;
}

void BasicSelectionObject::setDescription(const QString &description){
    _description = description;
}



