#ifndef SALAMODEL_H
#define SALAMODEL_H

#include <QString>

class SalaModel
{

public:
    int idSala() const;
    void setIdSala(int idSala);

    QString nomeSala() const;
    void setNomeSala(const QString &nomeSala);

    QString nrCapacidade() const;
    void setNrCapacidade(const QString &nrCapacidade);

    QString nrAndar() const;
    void setNrAndar(const QString &nrAndar);

    QString nrSala() const;
    void setNrSala(const QString &nrSala);

    QString complemento() const;
    void setComplemento(const QString &complemento);

private:

    int _idSala;
    QString _nomeSala;
    QString _nrCapacidade;
    QString _nrAndar;
    QString _nrSala;
    QString _complemento;

};

#endif // SALAMODEL_H
