#ifndef SALASERVICE_H
#define SALASERVICE_H

#include <QList>

#include "sala/saladto.h"
#include "sala/salamodel.h"

class SalaService
{
public:
    void insertSala( const SalaModel* salaModel ) const;
    QList<SalaModel*> findAllSalas() const;
    QList<SalaModel*> findSalasByName( const QString& name ) const;
    SalaModel* findSalaById( int idSala ) const;

    QList<SalaDto*> salaModelToDto( QList<SalaModel*> listSalaModel ) const;

};

#endif // SALASERVICE_H
