#ifndef SALADELEGATE_H
#define SALADELEGATE_H

#include <QList>

#include "sala/saladto.h"

class SalaModel;

class SalaDelegate
{
public:
    void insertSala( const SalaModel* salaModel ) const;
    QList<SalaModel*> findAllSalas() const;
    SalaModel* findSalaById( int idSala ) const;
    QList<SalaDto*> salaModelToDto( QList<SalaModel*> listSalaModel ) const;
    QList<SalaModel*> findSalasByName( const QString& name ) const;
};

#endif // SALADELEGATE_H
