#include "salacontroller.h"

#include "sala/salaservice.h"
#include "sala/saladelegate.h"

void SalaController::insertSala( SalaDto* salaModel ) const{
    SalaDelegate().insertSala( salaModel );
}

SalaModel *SalaController::findSalaById(int idSala) const{
    return SalaDelegate().findSalaById( idSala );
}

QList<QObject*> SalaController::findBasicSalasByName( const QString& name ) const {

    QList<QObject*> basicSalas = {};
    QList<SalaModel*> salas = SalaDelegate().findSalasByName( name );

    for( SalaModel* sala : salas ){
        if( sala ){
            basicSalas.append( new BasicSelectionObject( sala->idSala(), sala->nomeSala() ) );
            delete sala;
        }
    }

    return basicSalas;
}

QList<SalaDto*> SalaController::findAllSalas() const {
    return salaModelToDto( SalaDelegate().findAllSalas() );
}

QList<SalaDto*> SalaController::salaModelToDto(QList<SalaModel *> listSalaModel) const{
    return SalaDelegate().salaModelToDto( listSalaModel );
}
