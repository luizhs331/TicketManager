#include "salamodel.h"

int SalaModel::idSala() const {
    return _idSala;
}

void SalaModel::setIdSala(int idSala) {
    _idSala = idSala;
}

QString SalaModel::nomeSala() const {
    return _nomeSala;
}

void SalaModel::setNomeSala(const QString &nomeSala) {
    _nomeSala = nomeSala;
}

QString SalaModel::nrCapacidade() const {
    return _nrCapacidade;
}

void SalaModel::setNrCapacidade(const QString &nrCapacidade) {
    _nrCapacidade = nrCapacidade;
}

QString SalaModel::nrAndar() const {
    return _nrAndar;
}

void SalaModel::setNrAndar(const QString &nrAndar) {
    _nrAndar = nrAndar;
}

QString SalaModel::nrSala() const {
    return _nrSala;
}

void SalaModel::setNrSala(const QString &nrSala) {
    _nrSala = nrSala;
}

QString SalaModel::complemento() const {
    return _complemento;
}

void SalaModel::setComplemento(const QString &complemento) {
    _complemento = complemento;
}
