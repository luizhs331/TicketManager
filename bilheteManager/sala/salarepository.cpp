#include "salarepository.h"

#include <QtSql>

#include "sqls.h"

#include <sql/notosqlquery.h>

#define DIRECTORY "selectsqls/sala/"

void SalaRepository::insertSala( const SalaModel* salaModel ) const{

    NotoSqlQuery query;

    query.prepare( Sqls::createQuery( INSERT_SALA, DIRECTORY ) );

    query.bindParam( NOME_SALA, salaModel->nomeSala() );
    query.bindParam( CAPACIDADE, salaModel->nrCapacidade() );
    query.bindParam( NR_ANDAR, salaModel->nrAndar() );
    query.bindParam( NR_SALA, salaModel->nrSala() );
    query.bindParam( COMPLEMENTO, salaModel->complemento() );

    query.exec();
}

QList<SalaModel*> SalaRepository::findAllSalas() const {

    NotoSqlQuery query;
    QList<SalaModel*> salaList;

    query.prepare( Sqls::createQuery( SELECT_ALL, DIRECTORY ) );

    query.exec();

    while( query.next() ){

    SalaModel* salaModel = new SalaModel();

    salaModel->setIdSala( query.value( ID_SALA ).toInt() );
    salaModel->setNomeSala( query.value( NOME_SALA ).toString() );
    salaModel->setNrAndar( query.value( CAPACIDADE ).toString() );
    salaModel->setComplemento( query.value( NR_ANDAR ).toString() );
    salaModel->setNrCapacidade( query.value( NR_SALA ).toString() );
    salaModel->setNrSala( query.value( COMPLEMENTO ).toString() );

    salaList.append( salaModel );

    }

    return salaList;
}

QList<SalaModel*> SalaRepository::findSalasByName(const QString &name) const{

    NotoSqlQuery query;
    QList<SalaModel*> salaList;

    query.prepare( Sqls::createQuery( SELECT_BY_NOME_SALA, DIRECTORY ) );

    query.bindParam( NOME_SALA, name );

    query.exec();

    while( query.next() ){

    SalaModel* salaModel = new SalaModel();

    salaModel->setIdSala( query.value( ID_SALA ).toInt() );
    salaModel->setNomeSala( query.value( NOME_SALA ).toString() );
    salaModel->setNrAndar( query.value( CAPACIDADE ).toString() );
    salaModel->setComplemento( query.value( NR_ANDAR ).toString() );
    salaModel->setNrCapacidade( query.value( NR_SALA ).toString() );
    salaModel->setNrSala( query.value( COMPLEMENTO ).toString() );

    salaList.append( salaModel );

    }

    return salaList;

}

void SalaRepository::updateSala( const SalaModel* /*salaModel*/) const {}

void SalaRepository::deleteSala( int /*idSala*/) const {}

SalaModel *SalaRepository::findSalaById( int /*idSala*/) const {}
