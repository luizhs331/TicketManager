#ifndef SALACONTROLLER_H
#define SALACONTROLLER_H

#include <QObject>

#include <commons/basicselectionobject.h>

#include "sala/salamodel.h"
#include "sala/saladto.h"

class SalaController : public QObject {
    Q_OBJECT
public slots:
    void insertSala( SalaDto* salaModel ) const;
    SalaModel* findSalaById( int idSala ) const;
    QList<SalaDto*> findAllSalas() const;

    QList<QObject*> findBasicSalasByName( const QString& name ) const;

    QList<SalaDto*> salaModelToDto( QList<SalaModel*> listSalaModel ) const;

};

#endif // SALACONTROLLER_H
