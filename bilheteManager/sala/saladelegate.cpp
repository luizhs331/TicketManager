#include "saladelegate.h"

#include "sala/salaservice.h"

void SalaDelegate::insertSala(const SalaModel* salaModel) const{
    SalaService().insertSala( salaModel );
}

QList<SalaModel*> SalaDelegate::findAllSalas() const{
    return SalaService().findAllSalas();
}

SalaModel* SalaDelegate::findSalaById(int idSala) const{
    return SalaService().findSalaById( idSala );
}

QList<SalaDto *> SalaDelegate::salaModelToDto(QList<SalaModel *> listSalaModel) const{
    return SalaService().salaModelToDto( listSalaModel );
}

QList<SalaModel *> SalaDelegate::findSalasByName(const QString &name) const{
    return SalaService().findSalasByName( name );
}
