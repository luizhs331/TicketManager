#include "salaservice.h"

#include <sala/salarepository.h>

void SalaService::insertSala(const SalaModel* salaModel) const{
    SalaRepository().insertSala( salaModel );
}

QList<SalaModel*> SalaService::findAllSalas() const {
    return SalaRepository().findAllSalas();
}

QList<SalaModel*> SalaService::findSalasByName(const QString &name) const{
    return SalaRepository().findSalasByName( name );
}

SalaModel *SalaService::findSalaById(int idSala) const{
    return SalaRepository().findSalaById( idSala );
}

QList<SalaDto*> SalaService::salaModelToDto( QList<SalaModel *> listSalaModel) const {

    QList<SalaDto*> listSalaDto = {};
    SalaDto* salaDto;

    for( SalaModel* salaModel : listSalaModel){
        salaDto = static_cast<SalaDto*>(salaModel);
        listSalaDto.append( salaDto );
    }

    return listSalaDto;
}

