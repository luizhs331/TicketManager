#ifndef SALADTO_H
#define SALADTO_H

#include <QObject>

#include "sala/salamodel.h"

class SalaDto : public QObject, public SalaModel
{
    Q_OBJECT
    Q_PROPERTY(int idSala READ idSala WRITE setIdSala )
    Q_PROPERTY(QString nomeSala READ nomeSala WRITE setNomeSala )
    Q_PROPERTY(QString nrCapacidade READ nrCapacidade WRITE setNrCapacidade )
    Q_PROPERTY(QString nrAndar READ nrAndar WRITE setNrAndar )
    Q_PROPERTY(QString nrSala READ nrSala WRITE setNrSala )
    Q_PROPERTY(QString complemento READ complemento WRITE setComplemento )

};

#endif // SALADTO_H
