#ifndef SALAREPOSITORY_H
#define SALAREPOSITORY_H

#include <QList>

#include "sala/salamodel.h"

class SalaRepository
{
public:
   void insertSala( const SalaModel* salaModel ) const;
   void updateSala( const SalaModel* salaModel ) const;
   void deleteSala( int idSala ) const;

   QList<SalaModel*> findAllSalas() const;
   QList<SalaModel*> findSalasByName( const QString& name ) const;
   SalaModel* findSalaById( int idSala ) const;

   //
   static constexpr const char* ID_SALA = "ID_SALA";
   static constexpr const char* NOME_SALA = "NOME_SALA";
   static constexpr const char* CAPACIDADE = "CAPACIDADE";
   static constexpr const char* NR_ANDAR = "NR_ANDAR";
   static constexpr const char* NR_SALA = "NR_SALA";
   static constexpr const char* COMPLEMENTO = "COMPLEMENTO";

   // SQL
   static constexpr const char* INSERT_SALA = "INSERT_SALA.txt";
   static constexpr const char* SELECT_ALL = "SELECT_ALL.txt";
   static constexpr const char* SELECT_BY_NOME_SALA = "SELECT_BY_NOME_SALA.txt";

};

#endif // SALAREPOSITORY_H
