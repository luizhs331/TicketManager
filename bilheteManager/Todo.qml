import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0

Rectangle {
    id: root
    visible: true
    width: parent.width
    height: (text.height + (container.anchors.margins * 3))
    border.color: "#d3d3d3"
    radius: 3

    property string nome: ''
    property string assinaturaPedagogo: ''
    property string tipoBilhete: ''
    property string turma: ''
    property string turno: ''
    property string motivoEmissao: ''
    property string descricaoBilhete: ''
    property string horaEmissao: ''
    property string dataEmissao: ''
    property int idBilhete: 0

    Rectangle {
        id: recTop
        width: root.width / 1.8
        height: 25
        color: Qt.rgba(1, 0.550, 0.240, 0.72)
        z: 1

        Label {
            text: "ID Bilhete:" + root.idBilhete
            font.pointSize: 15
            visible: true
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            color: "#fff"
        }
    }

    Rectangle {
        id: container
        anchors.margins: 8
        color: root.color
        anchors.top: recTop.bottom

        Rectangle {
            anchors.centerIn: container
            anchors.margins: 12
            anchors.fill: parent

            Column {
                spacing: 2

                NotoRowLayout {
                    title: "Tipo do Bilhete: "
                    texto: root.tipoBilhete
                    colorTitle: "#414143"
                    pointSize: 14
                }

                NotoRowLayout {
                    title: "Turma do Aluno: "
                    texto: root.turma
                    colorTitle: "#414143"
                    pointSize: 14
                }

                NotoRowLayout {
                    title: "Nome do Aluno: "
                    texto: root.nome
                    colorTitle: "#414143"
                    pointSize: 14
                }

                NotoRowLayout {
                    title: "Turno Frequentado: "
                    texto: root.turno
                    colorTitle: "#414143"
                    pointSize: 14
                }

                NotoRowLayout {
                    title: "Motivo Emissao: "
                    texto: root.motivoEmissao
                    colorTitle: "#414143"
                    pointSize: 14
                }

                NotoRowLayout {
                    title: "Descricao: "
                    texto: root.descricaoBilhete
                    colorTitle: "#414143"
                    pointSize: 14
                }

                NotoRowLayout {
                    title: "Assinatura Pedagogo: "
                    texto: root.assinaturaPedagogo
                    colorTitle: "#414143"
                    pointSize: 14
                }

                NotoRowLayout {
                    title: "Hora Emissao: "
                    texto: root.horaEmissao
                    colorTitle: "#414143"
                    pointSize: 14
                }

                NotoRowLayout {
                    title: "Data Emissao: "
                    texto: root.dataEmissao
                    colorTitle: "#414143"
                    pointSize: 14
                }
            }
        }
    }
}
