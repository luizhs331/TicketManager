import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.4

import "qml/components"

Rectangle {
    id: rectangleMain

    property alias titleField: field.titleField
    property alias model: listView.model
    property alias isMandatoryField: field.isMandatoryField
    property alias showTextErro: field.showTextErro
    property alias currentText: field.currentText

    property int idOptionSelected

    signal textChanged

    function isValid() {
        showTextErro = !idOptionSelected > 0
        return !showTextErro
    }

    Column {
        TextFieldWithText {
            id: field
            width: rectangleMain.width
            height: rectangleMain.height
            isMandatoryField: false
            currentText: ""
            textSize: 10
            placeholderText: ""
            onFieldTextChange: textChanged()
            onFieldFocusChanged: {
                rectangleListView.visible = hasFocusEnable
            }

            Keys.onReturnPressed: {
                if (model.count > 0) {
                    rectangleListView.visible = true
                } else {
                    borderColorValidate = false
                }
            }
        }

        Rectangle {
            id: rectangleListView
            color: "#d3d3d3"
            radius: 3
            border.color: "#fff"
            width: rectangleMain.width
            height: model.count < 4 ? (((rectangleMain.height * 3.35) / 4)
                                       * model.count) : (rectangleMain.height * 3.25)
            visible: false

            ListView {
                id: listView
                width: rectangleMain.width
                height: rectangleListView.height
                spacing: 2

                clip: true
                anchors.horizontalCenter: parent.horizontalCenter

                delegate: Rectangle {
                    id: rectangleListViewButton
                    width: rectangleMain.width
                    height: rectangleMain.height / 1.3
                    color: "transparent"

                    Column {

                        Rectangle {
                            color: "transparent"
                            width: rectangleMain.width
                            height: 3
                        }

                        Button {
                            id: buttonListView
                            width: rectangleListViewButton.width - 8
                            height: rectangleListViewButton.height / 1.0
                            anchors.horizontalCenter: parent.horizontalCenter

                            Rectangle {
                                width: buttonListView.width / 1.05
                                height: buttonListView.height
                                anchors.centerIn: parent
                                color: "transparent"

                                Label {
                                    font.pointSize: 16
                                    color: mouseAreaButtonListView.containsMouse ? "#fff" : "#a3a3a3"
                                    anchors.verticalCenter: parent.verticalCenter
                                    font.family: "helvetica"
                                    text: modelData.description
                                }
                            }

                            background: Rectangle {
                                color: mouseAreaButtonListView.containsMouse ? "#87cefa" : "#fff"
                                radius: 3
                            }
                            MouseArea {
                                id: mouseAreaButtonListView
                                anchors.fill: buttonListView
                                hoverEnabled: true
                                onClicked: {
                                    field.currentText = modelData.description
                                    idOptionSelected = modelData.idOption
                                    rectangleListView.visible = false
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
