#ifndef TURMADTO_H
#define TURMADTO_H

#include <QObject>

#include <QDateTime>

class TurmaDto : public QObject {
    Q_OBJECT

    Q_PROPERTY( int idSala READ idSala WRITE setIdSala NOTIFY idSalaChanged )
    Q_PROPERTY( QString nomeTurma READ nomeTurma WRITE setNomeTurma NOTIFY nomeTurmaChanged )
    Q_PROPERTY( QString dtInicio READ dtInicio WRITE setDtInicio NOTIFY dtInicioChanged )
    Q_PROPERTY( QString dtFinal READ dtFinal WRITE setDtFinal NOTIFY dtFinalChanged )

public:

    int idSala() const;
    void setIdSala( int idSala );

    QString nomeTurma() const;
    void setNomeTurma( const QString& nomeTurma );

    QString dtInicio() const;
    void setDtInicio( const QString& dtInicio );

    QString dtFinal() const;
    void setDtFinal( const QString& dtFinal );

signals:
    void idSalaChanged();
    void nomeTurmaChanged();
    void dtInicioChanged();
    void dtFinalChanged();

private:
    int _idSala;
    QString _nomeTurma;
    QString _dtInicio;
    QString _dtFinal;

};

#endif // TURMADTO_H
