#include "turmacontroller.h"

#include "turmaservice.h"

// Todo em breve sera criada a camada do delegate em toda a aplicacao

void TurmaController::insertTurma( TurmaDto* turma ) const {
    TurmaService().insertNomeTurma( TurmaService().turmaDtoToTurmaModel( turma ) );
    delete turma;
}

void TurmaController::deleteTurma( const QString& nomeTurma ) const {
    TurmaService().deleteTurma( nomeTurma );
}

QStringList TurmaController::selectTurmas() const {
    return TurmaService().selectTurmas();
}
