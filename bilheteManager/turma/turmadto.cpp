#include "turmadto.h"

int TurmaDto::idSala() const {
    return _idSala;
}

void TurmaDto::setIdSala( int idSala ) {
    _idSala = idSala;
}

QString TurmaDto::nomeTurma() const {
    return _nomeTurma;
}

void TurmaDto::setNomeTurma( const QString& nomeTurma ) {
    _nomeTurma = nomeTurma;
}

QString TurmaDto::dtInicio() const {
    return _dtInicio;
}

void TurmaDto::setDtInicio( const QString& dtInicio ) {
    _dtInicio = dtInicio;
}

QString TurmaDto::dtFinal() const {
    return _dtFinal;
}

void TurmaDto::setDtFinal( const QString& dtFinal ) {
    _dtFinal = dtFinal;
}
