#ifndef TURMASERVICE_H
#define TURMASERVICE_H

#include <QString>

#include <turma/turmamodel.h>
#include <turma/turmadto.h>

class TurmaService {
public:
    void insertNomeTurma( const TurmaModel* nomeTurma ) const;
    void deleteTurma( const QString& nomeTurma ) const;

    TurmaModel* turmaDtoToTurmaModel( TurmaDto* turmaDto ) const;

    QStringList selectTurmas() const;

};

#endif // TURMASERVICE_H
