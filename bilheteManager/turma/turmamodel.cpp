#include "turmamodel.h"

int TurmaModel::idSala() const {
    return _idSala;
}

void TurmaModel::setIdSala( int idSala ) {
    _idSala = idSala;
}

QString TurmaModel::nomeTurma() const {
    return _nomeTurma;
}

void TurmaModel::setNomeTurma( const QString& nomeTurma ) {
    _nomeTurma = nomeTurma;
}

QDate TurmaModel::dtInicio() const {
    return _dtInicio;
}

void TurmaModel::setDtInicio( const QDate& dtInicio ) {
    _dtInicio = dtInicio;
}

QDate TurmaModel::dtFinal() const {
    return _dtFinal;
}

void TurmaModel::setDtFinal( const QDate& dtFinal ) {
    _dtFinal = dtFinal;
}
