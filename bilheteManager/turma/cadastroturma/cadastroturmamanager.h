#ifndef CADASTROTURMAMANAGER_H
#define CADASTROTURMAMANAGER_H

#include <QObject>

#include <components/stepcontroller.h>

class CadastroTurmaManager : public QObject
{
    Q_OBJECT
public:
    explicit CadastroTurmaManager();

    enum CadastroTurmaStep {
        DADOS_BASICOS,
        ALUNOS,
        CONFIRMACAO
    };

    Q_ENUM( CadastroTurmaStep )

public slots:
    QList<QObject*> steps();
    void changeStep( CadastroTurmaStep index );

private:
    StepController* _stepController;
};

#endif // CADASTROTURMAMANAGER_H
