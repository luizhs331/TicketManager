#include "cadastroturmamanager.h"

#include <QDebug>
#include <commons/icons.h>

CadastroTurmaManager::CadastroTurmaManager():
    _stepController( new StepController ){
    _stepController->initSteps( { {"Dados Básicos",Icons::DADOS},{"Alunos",Icons::USUARIOS },{"Confirmação",Icons::CHECK } } );
    _stepController->stepByIndex( int( CadastroTurmaStep::DADOS_BASICOS ) )->setSelected( true );
}

QList<QObject*> CadastroTurmaManager::steps(){
    return _stepController->stepsToQObject();
}

void CadastroTurmaManager::changeStep(CadastroTurmaStep index){
    qInfo() << "CadastroTurmaManager::changeStep [INDEX]" << index;
    _stepController->changeStep( int(index) );
}

