#ifndef TURMACONTROLLER_H
#define TURMACONTROLLER_H

#include <QObject>

#include <turma/turmadto.h>

class TurmaController : public QObject {
    Q_OBJECT
public:
    void deleteTurma( const QString& turma ) const;
    QStringList selectTurmas() const;

public slots:
    void insertTurma( TurmaDto* turma ) const;

};

#endif // TURMACONTROLLER_H
