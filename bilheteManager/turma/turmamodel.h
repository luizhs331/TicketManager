#ifndef TURMAMODEL_H
#define TURMAMODEL_H

#include <QDateTime>

class TurmaModel {
public:

    int idSala() const;
    void setIdSala( int idSala );

    QString nomeTurma() const;
    void setNomeTurma( const QString& nomeTurma );

    QDate dtInicio() const;
    void setDtInicio( const QDate& dtInicio );

    QDate dtFinal() const;
    void setDtFinal( const QDate& dtFinal );

private:

    int _idSala;
    QString _nomeTurma;
    QDate _dtInicio;
    QDate _dtFinal;
};

#endif // TURMAMODEL_H
