#ifndef TURMAREPOSITORY_H
#define TURMAREPOSITORY_H

#include <QStringList>

#include <turma/turmamodel.h>

class TurmaRepository {
public:
    void insertNomeTurma( const TurmaModel* turma ) const;
    void deleteTurma( const QString& nomeTurma );
    QStringList selectTurmas();

    static constexpr const char* DS_TURMA = "ds_nome_turma";

    // sqls

    static constexpr const char* INSERT = "INSERT_TURMA.txt";

};

#endif // TURMAREPOSITORY_H
