#include "turmarepository.h"

#include <QSqlQuery>
#include <QDebug>

#include <sqls.h>

#define SELECT_NOME_TURMA "SELECT ds_nome_turma FROM turmas WHERE ds_nome_turma IS NOT NULL"

void TurmaRepository::insertNomeTurma( const TurmaModel* turma ) const {

    QSqlQuery query;

    query.prepare( Sqls::createQuery( INSERT, "selectsqls/turma/" ) );

    query.bindValue( ":DS_NOME_TURMA", turma->nomeTurma() );
    query.bindValue( ":DT_INCIO", turma->dtInicio() );
    query.bindValue( ":DT_FINAL", turma->dtFinal() );
    query.bindValue( ":ID_SALA", turma->idSala() );

    if( !query.exec() ) {
        qCritical() << "insertFail ConfigBancoGeral::insertNomeTurma";
    }
}

void TurmaRepository::deleteTurma( const QString& nomeTurma ) {
    QSqlQuery query;
    query.prepare( "delete from turmas where ds_nome_turma ='" + nomeTurma + "'" );
    query.exec();
}

QStringList TurmaRepository::selectTurmas() {
    QSqlQuery query;
    QStringList retorno;

    query.prepare( SELECT_NOME_TURMA );
    query.exec();
    while ( query.next() ) {
        retorno.append( query.value( DS_TURMA ).toString() );
    }
    return retorno;
}
