#include "turmaservice.h"

#include "turmarepository.h"

void TurmaService::insertNomeTurma( const TurmaModel* turma ) const {
    if( selectTurmas().contains( turma->nomeTurma() ) ) {
        TurmaRepository().insertNomeTurma( turma );
    }
}

void TurmaService::deleteTurma( const QString& nomeTurma ) const {
    TurmaRepository().deleteTurma( nomeTurma );
}

QStringList TurmaService::selectTurmas() const {
    return TurmaRepository().selectTurmas();
}

TurmaModel* TurmaService::turmaDtoToTurmaModel( TurmaDto* turmaDto ) const {

    TurmaModel* turmaModel = new TurmaModel();

    QString formatDate = "dd/MM/yyyy";

    turmaModel->setIdSala( turmaDto->idSala() );
    turmaModel->setNomeTurma( turmaDto->nomeTurma() );
    turmaModel->setDtFinal( QDate::fromString( turmaDto->dtFinal(), formatDate ) );
    turmaModel->setDtInicio( QDate::fromString( turmaDto->dtInicio(), formatDate ) );

    return turmaModel;
}
