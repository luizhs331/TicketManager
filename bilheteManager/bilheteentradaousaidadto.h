#ifndef TODODTO_H
#define TODODTO_H

#include <QObject>

class BilheteEntradaOuSaidaDto : public QObject {
    Q_OBJECT
    Q_PROPERTY( QString description READ descricao WRITE setDescricao NOTIFY descriptionChanged )
    Q_PROPERTY( QString nome READ nome WRITE setNome NOTIFY nomeChanged )
    Q_PROPERTY( QString assinatura READ assinatura WRITE setAssinatura NOTIFY assinaturaChanged )
    Q_PROPERTY( QString motivo READ motivo WRITE setMotivo NOTIFY motivoChanged )
    Q_PROPERTY( QString turno READ turno WRITE setTurno NOTIFY turnoChanged )
    Q_PROPERTY( QString turma READ turma WRITE setTurma NOTIFY turmaChanged )
    Q_PROPERTY( QString tipoBilhete READ tipoBilhete WRITE setTipoBilhete NOTIFY tipoBilheteChanged )
    Q_PROPERTY( QString nomeEscola READ nomeEscola WRITE setNomeEscola NOTIFY nomeEscolaChanged )
    Q_PROPERTY( QString hora READ hora WRITE setHora NOTIFY horaChanged )
    Q_PROPERTY( QString data READ data WRITE setData NOTIFY dataChanged )
    Q_PROPERTY( int idBilhete READ idBilhete WRITE setIdBilhete NOTIFY idBilheteChanged )

public:
    explicit BilheteEntradaOuSaidaDto( );
    ~BilheteEntradaOuSaidaDto();

    QString turno() const;
    void setTurno( const QString& turno );

    QString turma() const;
    void setTurma( const QString& turma );

    QString tipoBilhete() const;
    void setTipoBilhete( const QString& tipoBilhete );

    QString nomeEscola() const;
    void setNomeEscola( const QString& nomeEscola );

    QString hora() const;
    void setHora( const QString& hora );

    QString data() const;
    void setData( const QString& data );

    QString nome() const;
    void setNome( const QString& nome );

    QString assinatura() const;
    void setAssinatura( const QString& assinatura );

    QString motivo() const;
    void setMotivo( const QString& motivo );

    QString descricao() const;
    void setDescricao( const QString& descriprion );

    int idBilhete() const;
    void setIdBilhete( int idBilhete );

signals:
    void textChanged();
    void descriptionChanged();
    void nomeChanged();
    void assinaturaChanged();
    void motivoChanged();
    void dataChanged();
    void turnoChanged();
    void turmaChanged();
    void tipoBilheteChanged();
    void nomeEscolaChanged();
    void horaChanged();
    void idBilheteChanged();

private:
    int _idBilhete;

    QString _descriprion;
    QString _motivo;
    QString _assinatura;
    QString _nome;
    QString _data;
    QString _hora;
    QString _nomeEscola;
    QString _tipoBilhete;
    QString _turma;
    QString _turno;

};
#endif // TODODTO_H
