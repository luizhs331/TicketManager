import QtQuick 2.4
import QtQuick.Layouts 1.6
import QtQuick.Controls 2.0

import "qml/components"

PoPupGenerico {
    id: mainPopup
    width: parent.width / 2
    height: parent.height / 1.5
    colorTop: "#11cdef"
    descricaoPoPup: "Configurar Teclas"

    //        Item {
    //           anchors.fill: parent
    //           focus: true
    //           Keys.onPressed: {
    //               if (event.key == ( 16777220 || Qt.Key_Enter )) {
    //                   carlos.texto = "Enter"
    //                   event.accepted = true;
    //               }
    //           }
    //         }

    // Se a Tecla Nao estiver Funcionando Precione Tab , ai vai comessar a funcionar
    ListModel {
        id: nameModel
        ListElement {
            name: "Criar Bilhete"
            tecla: "E"
        }
        ListElement {
            name: "Atualizar Bilhete"
            tecla: "F8"
        }
        ListElement {
            name: "Tela De Busca"
            tecla: "L"
        }
        ListElement {
            name: "Excluir Bilhete"
            tecla: "F1"
        }
        ListElement {
            name: "Tela Configuracoes"
            tecla: "Q"
        }
        ListElement {
            name: "Fechar Aplicacao"
            tecla: "ESC"
        }
        ListElement {
            name: "Fullscreen"
            tecla: "F11"
        }
    }
    Rectangle {
        width: parent.width
        height: parent.height / 1.13
        anchors.bottom: parent.bottom
        color: "transparent"

        Column {
            id: column
            width: parent.width
            height: parent.height / 1.2
            anchors.top: parent.top
            spacing: 30

            Flickable {
                id: flickable
                width: parent.width
                height: parent.height
                contentHeight: todoList.height
                anchors.centerIn: parent
                clip: true

                Column {
                    id: todoList
                    width: parent.width
                    spacing: 10

                    Repeater {
                        model: nameModel
                        NotoRowEdit {
                            title: name
                            texto: tecla
                            width: parent.width
                            height: mainPopup.height / 13
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                    }
                }
            }

            Rectangle {
                id: rectangle1Px
                width: parent.width
                height: 1
                color: "#d3d3d3"
                anchors.bottom: column.bottom
            }
            Rectangle {
                id: rectangleButtons
                width: parent.width
                height: parent.height / 7
                color: "transparent"
                anchors.top: rectangle1Px.bottom
                anchors.topMargin: 15

                Row {
                    id: rowHelperTecals
                    spacing: 10
                    width: btnAvancar.width + btnVoltar.width
                    anchors.centerIn: parent

                    ButtonCriado {
                        id: btnVoltar
                        widthButton: mainPopup.width / 4
                        heightButton: mainPopup.height / 14
                        buttonName: "<"
                        aumentarTamanhoWidth: 1
                        aumentarTamanhoHeight: 1
                        onClicked: {

                        }
                    }
                    ButtonCriado {
                        id: btnAvancar
                        widthButton: mainPopup.width / 4
                        heightButton: mainPopup.height / 14
                        buttonName: ">"
                        aumentarTamanhoWidth: 1
                        aumentarTamanhoHeight: 1
                        onClicked: {

                        }
                    }
                }
            }
        }
    }
}
