import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12

import "qml/components"

PoPupGenerico {
    id: fecharAplicacao
    visible: false
    width: parent.width / 4
    height: parent.height / 2
    anchors.centerIn: parent

    colorTop: "#f06363"
    heightRectangleTop: 100
    sizePointSize: 15
    imgLink: "img/desligar.png"

    Column {
        id: columnPopupClose
        width: parent.width
        height: parent.height / 1.1
        anchors.top: fecharAplicacao.top
        spacing: 20

        ButtonCriado {
            id: btnClosseAplicacao
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: columnPopupClose.bottom
            widthButton: parent.width / 2
            heightButton: parent.height / 3
            buttonName: "Fechar"
            onClicked: {
                mainWindow.close()
            }
        }
    }
}
