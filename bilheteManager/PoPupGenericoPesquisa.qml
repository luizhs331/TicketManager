import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12

import br.com.bilheteentradaousaida 1.0

import "qml/components"

Popup {
    id: popup
    anchors.centerIn: parent
    modal: true
    focus: true
    z: 0

    signal hasBilheteWithId
    signal buttonPressed
    signal clearCurrentTextField
    signal borderColorValidateSignal(var validate)

    property RegExpValidator customRegexPoPup
    property string currentTextTextFild: ""
    property string titlePoPup: ""
    property string borderColor: ""
    property string buttonName: ""
    property var borderColorValidate: true

    background: Rectangle {
        width: parent.width
        height: parent.height
        radius: 10
    }

    closePolicy: function () {
        return Popup.CloseOnEscape || Popup.CloseOnPressOutsideParent
    }

    onClearCurrentTextField: {
        idBilhete.currentText = ""
    }

    onBorderColorValidateSignal: function (validate) {
        idBilhete.borderColorValidate = validate
    }

    BilheteEntradaOuSaida {
        id: bilheteEntradaOuSaida
    }

    Rectangle {
        id: rectangleBuscarBilhete
        visible: true
        width: popup.width / 1.1
        height: popup.height / 1.2
        color: "transparent"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Column {
            spacing: popup.height / 8.2

            Label {
                text: titlePoPup
                font.family: "Helvetica"
                font.pointSize: 30
                color: "#474a51"
                anchors.horizontalCenter: popup.horizontalCenter
                opacity: 0.8
            }

            TextFieldWithText {
                id: idBilhete
                width: rectangleBuscarBilhete.width / 1.1
                height: rectangleBuscarBilhete.height / 3.2
                titleField: "ID Bilhete"
                customRegex: customRegexPoPup
                borderColorValidate: popup.borderColorValidate
                borderColor: popup.borderColor
                hasFocusEnable: true
                onCurrentTextChanged: currentTextTextFild = idBilhete.currentText
                currentText: ""
            }

            ButtonCriado {
                buttonName: popup.buttonName
                colorButtonPressed: "#afc0ff"
                colorBorderPressed: "#afc0ff"
                width: rectangleBuscarBilhete.width / 2.5
                height: rectangleBuscarBilhete.height / 4
                onClicked: {
                    popup.buttonPressed()
                }
            }
        }
    }
}
