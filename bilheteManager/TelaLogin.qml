import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12
import br.com.logincontroller 1.0
import QtQuick.Controls.Material 2.1

import "qml/components"

Rectangle {

    id: login
    width: parent.width
    height: parent.height
    anchors.centerIn: parent
    color: "#161616"
    anchors.leftMargin: 30
    border.color: "#d3d3d3"
    border.width: 0.3
    visible: false

    Material.theme: Material.Dark
    Material.accent: Material.Red

    onVisibleChanged: {
        control.recuperarUsuarioLogado()
        login.validarFocusLogin()
    }

    Rectangle {
        width: parent.width / 3.5
        height: parent.height / 1.5
        anchors.centerIn: parent
        color: "#fff"
        opacity: 1
        border.color: "#161616"
        radius: 10
    }

    CadastroEscola {
        id: cadastroEscola
        width: login.width / 2.2
        height: login.height / 1.1
        visible: false
        anchors.centerIn: parent
    }

    function validarFocusLogin() {

        if (username.text == "") {
            username.focus = true
        } else {
            password.focus = true
        }
    }

    function validarCorBordarStatusLogin() {
        if (usuarioValido == false && username.focus) {
            usuarioValido = undefined
            return "#d41b1b"
        } else if (username.focus) {
            return "#0fdbff"
        }
        return "#dfe0e1"
    }
    NotoFailOrSucess {
        id: animationFailOrSucess
        width: 330
        height: 150
        anchors.right: parent.right
        color: "transparent"
        visible: false
        mensage: "Usuario ou senha invalido"
        fromYinit: 45
        fromYdone: 25
    }

    property var usuarioValido: undefined

    LoginController {
        id: control
    }

    onVerificarUsuarioValido: function (dsNomeUsuario, dsSenhaUsuario) {

        usuarioValido = control.loginApproved(dsNomeUsuario, dsSenhaUsuario)

        if (usuarioValido) {
            login.loginAprovado()
        } else {
            username.focus = true
            animationFailOrSucess.visible = true
            animationFailOrSucess.runAnimation()
        }
    }

    signal loginAprovado
    signal verificarUsuarioValido(var dsNomeUsuario, var dsSenhaUsuario)

    Label {
        text: "Ticket Manager versao 0.33v"
        font.family: "Helvetica"
        font.pointSize: 10
        color: "#fff"
        opacity: 0.7
    }

    Image {
        id: img
        width: 125
        height: 125
        source: "img/logo.png"
        anchors.bottom: columnId.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: 40
        opacity: 0.8
    }

    Rectangle {
        width: parent.width / 5
        opacity: 1
        height: 2
        color: "#11cdef"
        anchors.bottom: columnId.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: 30
    }

    Column {
        id: columnId
        width: parent.width
        height: parent.height / 2
        anchors.bottom: parent.bottom
        spacing: 10

        TextField {
            id: username
            placeholderText: "Usuario"
            text: control.nomeUsuarioLogado
            width: parent.width / 5
            height: 45
            anchors.horizontalCenter: parent.horizontalCenter
            onTextChanged: text = text.toUpperCase()
            background: Rectangle {
                radius: 5
                border.color: login.validarCorBordarStatusLogin()
            }
        }

        TextField {
            id: password
            placeholderText: "Password"
            width: parent.width / 5
            height: 45
            echoMode: TextInput.Password
            anchors.horizontalCenter: parent.horizontalCenter
            background: Rectangle {
                radius: 5
                border.color: password.focus ? /*"#87cefa"*/ "#11cdef" : "#d3d3d3"
            }
        }

        Keys.onPressed: {
            if ((event.key === 16777220) || (event.key = Qt.Key_Enter)) {
                login.verificarUsuarioValido(username.text, password.text)
            }
        }

        ButtonCriado {
            id: buttonLogin
            widthButton: parent.width / 5
            heightButton: parent.height / 8
            anchors.horizontalCenter: parent.horizontalCenter
            buttonName: "Login"
            tpButtonTransparent: "N"
            onClicked: {
                login.verificarUsuarioValido(username.text, password.text)
            }
        }

        Button {
            id: buttonCadastro
            width: textCadastro.width
            height: textCadastro.height
            anchors.horizontalCenter: parent.horizontalCenter
            background: Rectangle {
                color: "transparent"
            }
            Label {
                id: textCadastro
                text: "Cadastre-se"
                font.family: "Helvetica"
                font.pointSize: 13
                color: "#000"
                opacity: mouseAreaButtonCadastro.containsMouse ? 1 : 0.6
            }
            MouseArea {
                id: mouseAreaButtonCadastro
                hoverEnabled: true
                anchors.fill: parent
                onClicked: {
                    cadastroEscola.visible = true
                }
            }
        }

        //    Text {
        //        id: link_Text
        //        text: '<html><style type="text/css"></style><a href="http://google.com">Cadastre-se</a></html>'
        //        onLinkActivated: Qt.openUrlExternally('https://stackoverflow.com/questions/12536416/qml-text-element-hyperlink')
        //        anchors.horizontalCenter: parent.horizontalCenter
        //    }
    }
}
