#include "configcaixageralservice.h"

#include "configcaixageralrepository.h"

bool ConfigCaixaGeralService::insertConfigGeral( const QString& dsNomeEscola, const QString& numMaximoCaracter ) {
    return ConfigCaixaGeralRepository().insertConfigGeral( dsNomeEscola, numMaximoCaracter );
}

bool ConfigCaixaGeralService::updateConfigGeral( bool fgGerarPdf, const QString& numMaximoCaracter ) {
    return ConfigCaixaGeralRepository().updateConfigGeral( fgGerarPdf, numMaximoCaracter );
}

QString ConfigCaixaGeralService::selectNomeEscola() {
    return ConfigCaixaGeralRepository().selectNomeEscola();
}

int ConfigCaixaGeralService::selectMaximoCarcterLinha() {
    return ConfigCaixaGeralRepository().selectMaximoCarcterLinha();
}

void ConfigCaixaGeralService::updateDadosIntegracao( const int& valor ) {
    ConfigCaixaGeralRepository().updateDadosIntegracao( valor );
}

bool ConfigCaixaGeralService::selectDadosIntegracao() {
    return ConfigCaixaGeralRepository().selectDadosIntegracao();
}
