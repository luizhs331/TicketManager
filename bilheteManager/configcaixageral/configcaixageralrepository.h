#ifndef CONFIGCAIXAGERALREPOSITORY_H
#define CONFIGCAIXAGERALREPOSITORY_H

#include <QString>

class ConfigCaixaGeralRepository {
public:

    bool insertConfigGeral( const QString& dsNomeEscola, const QString& numMaximoCaracter );
    bool updateConfigGeral( bool fgGerarPdg, const QString& numMaximoCaracter );
    QString selectNomeEscola();
    int selectMaximoCarcterLinha();
    void updateDadosIntegracao( const int& valor );
    bool selectDadosIntegracao();

    static constexpr const char* DS_NOME_ESCOLA = "ds_nome_escola";
    static constexpr const char* DS_TURMA_ALUNO = "ds_nome_turma";
    static constexpr const char* MAXIMO_CARACTER_UMA_LINHA = "maximo_caracter_linha";
    static constexpr const char* VL_INTEGRACAO_ATIVA = "flag_enviar_dados";

};

#endif // CONFIGCAIXAGERALREPOSITORY_H
