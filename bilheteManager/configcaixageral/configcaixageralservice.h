#ifndef CONFIGCAIXAGERALSERVICE_H
#define CONFIGCAIXAGERALSERVICE_H

#include <QString>

class ConfigCaixaGeralService {
public:
    bool insertConfigGeral( const QString& dsNomeEscola, const QString& numMaximoCaracter );
    bool updateConfigGeral( bool fgGerarPdf, const QString& numMaximoCaracter );
    QString selectNomeEscola();
    int selectMaximoCarcterLinha();
    void updateDadosIntegracao( const int& valor );
    bool selectDadosIntegracao();

};

#endif // CONFIGCAIXAGERALSERVICE_H
