#ifndef CONFIGCAIXAGERALCONTROLLER_H
#define CONFIGCAIXAGERALCONTROLLER_H

#include <QObject>

class ConfigCaixaGeralController : public QObject {
    Q_OBJECT
    Q_PROPERTY( QStringList comboBox READ comboBoxPrintar NOTIFY comboBoxChanged )
public:
    QStringList comboBoxPrintar() const;
signals:

    void comboBoxChanged();

public slots:

    void updateDadosIntegracao( const int& valor );
    void deletarTurma( const QString& nomeTurma );

    int convertOptionForIndex( QStringList options, QString optionSelected );
    int retornoMaximoCaracterLinha();

    bool selectDadosIntegracao();
    bool salvarDadosConfigGeral( bool fgGerarPdf, const QString& maximoCaracterLinha );

    QString retornoNomeEscola();

private:

    bool _animationFailOrSucess = false;
};
#endif // CONFIGCAIXAGERALCONTROLLER_H
