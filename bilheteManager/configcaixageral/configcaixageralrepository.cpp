#include "configcaixageralrepository.h"

#include <QtSql>
#include <QFileInfo>

#include "bilheteentradaousaidadto.h"
#include "bilheteformatar.h"

#define CAMINHO_BANCO "/BancoDedados/databasebilhete.db"

#define SELECT_NOME_TURMA "SELECT ds_nome_turma FROM turmas_cadastradas WHERE ds_nome_turma IS NOT NULL"
#define SELECT_NOME_ESCOLA "SELECT ds_nome_escola FROM config_geral WHERE id_config_geral = '1' "
#define SELECT_MAXIMO_CARACTER_UMA_LINHA "select maximo_caracter_linha from config_geral where id_config_geral = '1' "
#define SELECT_DADOS_INTEGRACAO "select flag_enviar_dados from config_geral where id_config_geral = 1"

void ConfigCaixaGeralRepository::updateDadosIntegracao( const int& valor ) {

    QSqlQuery query;

    query.prepare( "update config_geral set flag_enviar_dados = " + QString::number( valor ) + " where id_config_geral = 1" );

    if( !query.exec() ) {
        qCritical( "Error ConfigBancoGeral::insertDadosIntegracao" );
    }
}

bool ConfigCaixaGeralRepository::selectDadosIntegracao() {

    QSqlQuery query;

    query.prepare( SELECT_DADOS_INTEGRACAO );
    query.exec();

    query.value( query.next() ).toInt();

    return query.value( VL_INTEGRACAO_ATIVA ).toBool();
}

bool ConfigCaixaGeralRepository::insertConfigGeral( const QString& dsNomeEscola, const QString& numMaximoCaracter ) {
    QSqlQuery query;
    query.prepare( "insert into config_geral (ds_nome_escola , maximo_caracter_linha , id_config_geral ) values ('" + dsNomeEscola + "' , '" + numMaximoCaracter + "' , 1 )" );
    return query.exec();
}

bool ConfigCaixaGeralRepository::updateConfigGeral( bool fgGerarPdf, const QString& numMaximoCaracter ) {
    QSqlQuery query;
    query.prepare( "update config_geral set flag_criarpdf = :FG_CRIAR_PDF , maximo_caracter_linha = :NR_MAXIMO_CARACTER where id_config_geral='1' " );

    query.bindValue( ":FG_CRIAR_PDF", fgGerarPdf );
    query.bindValue( ":NR_MAXIMO_CARACTER", numMaximoCaracter );

    return query.exec();
}

QString ConfigCaixaGeralRepository::selectNomeEscola() {

    QSqlQuery query;
    QString retorno;

    query.prepare( SELECT_NOME_ESCOLA );
    query.exec();

    query.value( query.next() ).toString();
    retorno = query.value( DS_NOME_ESCOLA ).toString();
    return retorno;
}

int ConfigCaixaGeralRepository::selectMaximoCarcterLinha() {

    QSqlQuery query;
    int retorno;

    query.prepare( SELECT_MAXIMO_CARACTER_UMA_LINHA );
    query.exec();

    query.value( query.next() ).toInt();
    retorno = query.value( MAXIMO_CARACTER_UMA_LINHA ).toInt();

    return retorno;

}

