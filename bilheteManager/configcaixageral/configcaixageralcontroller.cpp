#include "configcaixageralcontroller.h"

#include "configcaixageralservice.h"

#include <turma/turmacontroller.h>

QString ConfigCaixaGeralController::retornoNomeEscola() {
    return ConfigCaixaGeralService().selectNomeEscola().isEmpty() ? "Nome Escola" : ConfigCaixaGeralService().selectNomeEscola();
}

bool ConfigCaixaGeralController::selectDadosIntegracao() {
    return ConfigCaixaGeralService().selectDadosIntegracao();
}

int ConfigCaixaGeralController::retornoMaximoCaracterLinha() {
    return ConfigCaixaGeralService().selectMaximoCarcterLinha();
}

void ConfigCaixaGeralController::deletarTurma( const QString& nomeTurma ) {
    if( !nomeTurma.isEmpty() ) {
        TurmaController().deleteTurma( nomeTurma );
    }
}

bool ConfigCaixaGeralController::salvarDadosConfigGeral( bool fgGerarPdf, const QString& maximoCaracterLinha ) {
    return ConfigCaixaGeralService().updateConfigGeral( fgGerarPdf, maximoCaracterLinha );
}

void ConfigCaixaGeralController::updateDadosIntegracao( const int& valor ) {
    ConfigCaixaGeralService().updateDadosIntegracao( valor );
}


QStringList ConfigCaixaGeralController::comboBoxPrintar() const {
    QStringList listTurmas;
    listTurmas.append( TurmaController().selectTurmas() );
    listTurmas.prepend( " " );
    return listTurmas;
}

int ConfigCaixaGeralController::convertOptionForIndex( QStringList options, QString optionSelected ) {
    int i = 0;
    for( QString optionTime : options ) {
        if( optionTime == optionSelected ) {
            return i;
        }
        i++;
    }
    return i;
}
